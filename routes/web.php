<?php

use App\Http\Controllers\Admin\SuperAdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/parent-home', 'UserController@parent')->name('parent-home');

Route::get('provider/home', function() {
    return view('provider-index');
})->name('provider/home');

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile/{id}', 'HomeController@profile')->name('profile');

Route::put('profile/{id}', 'HomeController@profileUpdate')->name('profile.update');

Route::put('password/{id}', 'HomeController@passwordUpdate')->name('password.update');

Route::get('/about', 'UserController@about')->name('about');

Route::get('post/{post}', 'UserController@daycares')->name('post');

Route::get('search', 'UserController@search')->name('search');

Route::get('terms', 'UserController@terms')->name('terms');

Route::get('do-not-sell', 'UserController@sale')->name('do-not-sell');

Route::get('privacy', 'UserController@privacy')->name('privacy');

Route::get('help', 'UserController@help')->name('help');

Route::get('daycare-search', 'UserController@faq')->name('daycare-search');

Route::get('daycare-connect', 'UserController@connect')->name('daycare-connect');

Route::post('reserve-spot','UserController@reserve')->name('reserve-spot');

Route::post('report', 'UserController@Report')->name('report');

Route::post('tour', 'UserController@tour')->name('tour');

Route::post('review', 'UserController@review')->name('review');

Route::get('resources', 'UserController@resources')->name('resources');

Route::get('filter', 'UserController@filter')->name('filter');

Route::get('daycares', 'UserController@allDaycares')->name('daycares');

Route::get('contact', 'UserController@getContact')->name('contact');

/* Message */
Route::post('message', 'HomeController@sendMessage');

Route::post('sendChat/{user}', 'HomeController@sendChat');

Route::get('messages', 'HomeController@getMessages')->name('messages');

Route::get('message/{id}', 'HomeController@getChat')->name('chat');

Route::get('chat/{id}', 'HomeController@getMessage')->name('message');
/*End of message*/

Route::get('all-waitlists', 'UserController@daycareWaitlist')->name('all-waitlists');

Route::get('waitlist/{post}', 'UserController@Waitlist')->name('waitlist');

Route::get('remind/{id}', 'HomeController@getReminder')->name('parent-reminder');

Route::get('card', 'Transaction\TransactionController@getCard')->name('card.form');

Route::post('save', 'Transaction\CustomerController@save')->name('save.customer');

Route::get('express', 'Transaction\SellerController@create')->name('create.express');

Route::get('stripe', 'Transaction\SellerController@save')->name('save.express');

Route::group(['middleware' => ['stripe']], function() {   
    Route::post('purchase', 'Transaction\TransactionController@renew')->name('purchase')->middleware('customer');
});

Route::prefix('daycare')->group(function() {

    //Login Route
    Route::get('/login', 'Daycare\LoginController@showLoginForm')->name('daycare/login');
    Route::post('/login', 'Daycare\LoginController@login')->name('daycare.login');

    //Logout Route
    Route::post('/logout','Daycare\LoginController@logout')->name('daycare.logout');

    //Register Route
    Route::get('/register', 'Daycare\RegisterController@showRegistrationForm')->name('daycare/register');
    Route::post('/register', 'Daycare\RegisterController@create')->name('daycare.register');

    //Password Reset
    Route::get('/password/reset','Daycare\ForgotPasswordController@showLinkResetForm')->name('daycare.password.request');
    Route::post('/password/email','Daycare\ForgotPasswordController@sendResetLinkEmail')->name('daycare.password.email');
    Route::get('/password/reset/{token}', 'Daycare\ResetPasswordController@showResetForm')->name('daycare.password.reset');
    Route::post('/password/reset','Daycare\ResetPasswordController@reset')->name('daycare.password.update');

    //Email Verification
    Route::get('email/verify', 'Daycare\VerificationController@show')->name('daycare.verification.notice');
    Route::get('/email/resend', 'Daycare\VerificationController@resend')->name('daycare.verification.resend');
    Route::get('/email/verify/{id}/{hash}', 'Daycare\VerificationController@verify')->name('daycare.verification.verify');

    //Generic pages
    Route::get('about', 'AdminController@daycareAbout')->name('daycare/about');
    Route::get('resources', 'AdminController@daycareResources')->name('daycare/resources');
    Route::get('get-started', 'AdminController@getStarted')->name('daycare/get-started');
    Route::get('trust-and-safety', 'AdminController@trust')->name('daycare/trust-and-safety');
    Route::get('referrals', 'AdminController@referrals')->name('daycare/referrals');
    Route::get('cancel', 'AdminController@cancel')->name('daycare/cancel');

    //Dashboard
    // Route::group(['middleware' => ['subscribed']], function() {
        Route::get('home', 'AdminController@index')->name('daycare/home');
        Route::get('dashboard','AdminController@dashboard')->name('daycare/dashboard');
        Route::put('user/update/{id}'   , 'AdminController@userUpdate')->name('user.update');
        Route::put('info/update/{id}'   , 'AdminController@infoUpdate')->name('info.update');
        Route::post('rates', 'AdminController@rates')->name('rates');
        Route::put('rates/update/{id}'   , 'AdminController@ratesUpdate')->name('rates.update');
        Route::post('services', 'AdminController@service')->name('services');
        Route::put('services/update/{id}'   , 'AdminController@serviceUpdate')->name('services.update');
        Route::post('features', 'AdminController@feature')->name('features');
        Route::put('features/update/{id}'   , 'AdminController@featureUpdate')->name('features.update');
        Route::post('drops', 'AdminController@drop')->name('drops');
        Route::put('drops/update/{id}'   , 'AdminController@dropUpdate')->name('drops.update');
        Route::post('timeslot', 'AdminController@timeslot')->name('timeslot');
        Route::put('timeslot/update/{id}'   , 'AdminController@timeslotUpdate')->name('timeslot.update');
        Route::put('uploadImage/{id}','AdminController@uploadImage')->name('image-upload');
        Route::put('uploadPhoto/{id}','AdminController@uploadPhoto')->name('photo_upload');
        Route::post('description', 'AdminController@storeDescription')->name('dashboard.descriptions');
        Route::put('description/update/{id}', 'AdminController@updateDescription')->name('description.update');

        //Schedule
        Route::get('schedule','AdminController@schedule')->name('daycare/schedule');
        Route::post('schedules', 'AdminController@schedules')->name('schedules');
        Route::put('schedule/update/{id}', 'AdminController@scheduleUpdate')->name('schedule.update');

        //Tour
        Route::get('tour','AdminController@tour')->name('daycare/tour');
        Route::post('tour/approve/{id}', 'AdminController@tourApprove')->name('tour.approve');
        Route::post('tour/reject/{id}', 'AdminController@tourReject')->name('tour.reject');
        Route::get('tour-approved', 'AdminController@tourApproved')->name('daycare/tour-approved');
        Route::get('tour-rejected', 'AdminController@tourRejected')->name('daycare/tour-rejected');

        //Waitlist
        Route::get('waitlist', 'AdminController@waitlist')->name('daycare/waitlist');
        Route::post('waitlist/approve/{id}', 'AdminController@waitlistApprove')->name('waitlist.approve');
        Route::post('waitlist/reject/{id}', 'AdminController@waitlistReject')->name('waitlist.reject');

        //reservation
        Route::get('reservation', 'AdminController@reservation')->name('daycare/reservations');
        Route::post('reservation/approve/{id}', 'AdminController@spotApprove')->name('reservation.approve');
        Route::post('reservation/reject/{id}', 'AdminController@spotReject')->name('reservation.reject');

        //Reviews
        Route::get('reviews', 'AdminController@review')->name('daycare/reviews');

        //Analytics
        Route::get('analytics', 'AdminController@analytics')->name('analytics');
        Route::get('get-analytics', 'AnalyticsController@getMonthlyPostData')->name('get-analytics');

        Route::group(['middleware' => ['stripeAccount']], function() {
            //Reminder
            Route::get('reminder', 'AdminController@getReminder')->name('daycare/reminder');
            Route::post('reminder/{id}', 'Transaction\ReminderController@send')->name('reminder');
            Route::get('Balance', 'Transaction\SellerController@login')->name('stripe.login');
        });

        //messages
        Route::get('messages', 'AdminController@messages')->name('daycare/messages');
    // });

    Route::get('subscribe', 'AdminController@subscribe')->name('daycare/subscribe');
    Route::post('subscribe', 'AdminController@pay')->name('daycare/subscribe');
    Route::post('cancelSub', 'AdminController@cancelSub')->name('cancelSub');
    Route::get('resumeSub', 'AdminController@resumeSub')->name('resumeSub');
    Route::post('daycare/subscribe-yearly', 'AdminController@payYearly')->name('daycare/subscribe-yearly');
    Route::get('do-not-sell', 'UserController@sell')->name('daycare/do-not-sell');
    Route::get('terms-of-use', 'UserController@termsUse')->name('daycare/terms-of-use');
});

Route::namespace('Admin')->group(function() {
    Route::get('admin-register', 'RegisterController@showRegistrationForm')->name('admin-register');
    Route::post('admin.register', 'RegisterController@register')->name('admin.register');
    Route::get('admin-login', 'LoginController@showLoginForm')->name('admin-login');
    Route::post('admin.login', 'LoginController@login')->name('admin.login');
    Route::post('admin.logout', 'LoginController@logout')->name('admin.logout');

    //Dashboard
    Route::get('admin/dashboard', 'SuperAdminController@dashboard')->name('admin/dashboard');
    Route::get('admin-report', 'SuperAdminController@report')->name('admin-report');
    Route::get('admin-providers', 'SuperAdminController@providers')->name('admin-providers');
});