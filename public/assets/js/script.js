const date = new Date();

const renderCalendar = () => {
  date.setDate(1);

  const monthDays = document.querySelector(".days");

  const lastDay = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDate();

  const prevLastDay = new Date(
    date.getFullYear(),
    date.getMonth(),
    0
  ).getDate();

  const firstDayIndex = date.getDay();

  const lastDayIndex = new Date(
    date.getFullYear(),
    date.getMonth() + 1,
    0
  ).getDay();

  const nextDays = 7 - lastDayIndex - 1;

  const months = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];

  document.querySelector(".date h1").innerHTML = months[date.getMonth()];

  document.querySelector(".date p").innerHTML = new Date().toDateString();

  let days = "";

  for (let x = firstDayIndex; x > 0; x--) {
    days += `<div class="prev-date">${prevLastDay - x + 1}</div>`;
  }

  for (let i = 1; i <= lastDay; i++) {
    if (
      i === new Date().getDate() &&
      date.getMonth() === new Date().getMonth()
    ) {
      days += `<div class="today">${i}</div>`;
    } else {
      days += `<div>${i}</div>`;
    }
  }

  for (let j = 1; j <= nextDays; j++) {
    days += `<div class="next-date">${j}</div>`;
    monthDays.innerHTML = days;
  }
};

document.querySelector(".prev").addEventListener("click", () => {
  date.setMonth(date.getMonth() - 1);
  renderCalendar();
});

document.querySelector(".next").addEventListener("click", () => {
  date.setMonth(date.getMonth() + 1);
  renderCalendar();
});

renderCalendar();


/*

const select = document.querySelector(".myselect");
const options = document.querySelectorAll(".myselect option");
 
if (select) {
  // 1st method
  select.addEventListener("change", function() {
    const url = this.options[this.selectedIndex].dataset.url;
    if (url) {
      location.href = url;
    }
  });
 
  for (const option of options) {
    const url = option.dataset.url;
    if (url === location.href) {
      option.setAttribute("selected", "");
      break;
    }
  }
 
  // 2nd method
  /*select.addEventListener("change", function() {
    const url = this.options[this.selectedIndex].dataset.url;
    if (url) {
      localStorage.setItem("url", url);
      location.href = url;
    }
  });
 
  if (localStorage.getItem("url")) {
    for (const option of options) {
      const url = option.dataset.url;
      if (url === localStorage.getItem("url")) {
        option.setAttribute("selected", "");
        break;
      }
    }
  }
}

*/