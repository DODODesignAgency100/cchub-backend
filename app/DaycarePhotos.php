<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DaycarePhotos extends Model
{
    //
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    public function daycare() {
        return $this->belongsTo('App\Daycare');
    }
}
