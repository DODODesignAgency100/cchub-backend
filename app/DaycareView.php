<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DaycareView extends Model
{
    use SoftDeletes;

    protected $table = 'daycare_views';

    public static function createViewLog(Daycare $post) {
        $daycareView = new DaycareView();
        $daycareView->daycare_id = $post->id;
        $daycareView->business_name = $post->business_name;
        $daycareView->session_id = Request::getSession()->getId();
        $daycareView->user_id = Auth::user()->id;
        $daycareView->ip = Request::getClientIp();
        $daycareView->agent = Request::header('User-Agent');
        $daycareView->save();
    }

    public function daycare() {
        return $this->hasOne('App\Daycare','daycare_id');
    }
}
