<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    public function daycare() {
        return $this->belongsTo('App\Daycare','daycare_id');
    } 

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
