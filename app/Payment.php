<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $fillable = [
        'user_id',
        'daycare_id',
        'stripe_charge_id',
        'paid_out',
        'fees_collected',
        'description',
        'refunded',
    ];

    protected $cast = ['refunded' => 'boolean'];

    public function user() 
    {
        return $this->hasOne('App\User', 'user_id');
    }

    public function daycare()
    {
        return $this->hasOne('App\Daycare', 'daycare_id');
    }
}
