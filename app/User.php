<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Message;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    const VERIFIED_USER = '1';
    const UNVERIFIED_USER = '0';

    const ADMIN_USER = 'true';
    const REGULAR_USER = 'false';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email', 
        'password',
        'verified',
        'verification_token',
        'admin',
        'address',
        'address1',
        'city',
        'province',
        'zip',
        'phone_number',
        'terms',
        'place_id',
        'longitude',
        'latitude',
        'image',
        'stripe_customer_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verification_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isVerified() {
        return $this->verified == User::VERIFIED_USER;
    }

    public function isAdmin() {
        return $this->admin == User::ADMIN_USER;
    }


    public static function generateVerificationCode() {
        $timeNow = Carbon::now();
        return sha1($timeNow);
    }

    public function comment() {
        return $this->hasMany('App\Review','user_id');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment', 'user_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function reminder() 
    {
        return $this->hasMany(reminder::class);
    }

    public function message()
    {
        $message = Message::where('user_id', $this->id)->orWhere('receiver_id', $this->id)->get();
        return $message;
    }

}