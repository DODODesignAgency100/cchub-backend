<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Admin extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $guard = 'daycare';
    protected $dates = ['deleted_at'];

    const IS_SUPERAMNIN = 'true';
    const REGULAR_USER = 'false';

    protected $fillable = [
        'first_name',
        'last_name',
        'email', 
        'password',
        'verified',
        'verification_token',
        'admin',
        'address',
        'phone_number',
        'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verification_token',
    ];

    public function isSuperAdmin() {
        return $this->admin == Admin::IS_SUPERAMNIN;
    }

    public static function generateVerificationCode() {
        $timeNow = Carbon::now();
        return sha1($timeNow);
    }

    // public function sendPasswordResetNotification($token)
    // {
    //     $this->notify(new DaycarePasswordResetNotification($token));
    // }
}
