<?php 

namespace App\Services\Stripe;

use App\Daycare;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Stripe\Charge;
use Stripe\Stripe;
use Stripe\Transfer;

class Transaction 
{
    public static function create(User $user, Daycare $daycare, Request $request)
    {
        //initiate data for transaction
        $amount = $request->amount;
        $payout = $amount * 0.7;
        Stripe::setApiKey(config('services.stripe.secret'));

        //Create stripe charge from customer renewal
        $charge = Charge::create([
            'amount' => self::toStripeFormat($amount),
            'currency' => 'CAD',
            'customer' => $user->stripe_customer_id,
            'description' => 'Dyacare renewal to'. $daycare->business_name
        ]);

        //Transfer funds to daycare provicers, after fees have been extracted
        Transfer::create([
            'amount' => self::toStripeFormat($payout),
            'currency' => 'CAD',
            'source_transaction' => $charge->id,
            'destination' => $daycare->stripe_connect_id
        ]);

        //Save Transaction in DB
        $payment = new Payment();
        $payment->user_id = $user->id;
        $payment->daycare_id = $daycare->id;
        $payment->stripe_charge_id = $charge->id;
        $payment->paid_out = $payout;
        $payment->fees_collected = $amount - $payout;
        $payment->description = 'Payment made to' .$daycare->business_name;
        $payment->save();
    }

    public static function toStripeFormat(float $amount)
    {
        return $amount * 100;
    }
}