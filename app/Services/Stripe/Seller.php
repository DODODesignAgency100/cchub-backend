<?php

namespace App\Services\Stripe;

use App\User;
use GuzzleHttp\Client;

class Seller
{
    public static function create($code) 
    {
        try {
            $client = new Client(['base_uri' => 'https://connect.stripe.com/express/oauth/']);
            $request = $client->request("POST", "token", [
                "form_params" => [
                    'client_secret' => config('services.stripe.secret'),
                    'code' => $code,
                    'grant_type' => 'authorization_code'
                ]
            ]);

            return json_decode($request->getBody()->getContents());
        } catch(\Exception $e) {
            return $e->getMessage();
        }
    }
}