<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tour extends Model
{
    use SoftDeletes;
    //
    protected $dates = ['deleted_at'];

    const APPROVED_STATUS = 'approved';
    const REJECTED_STATUS = 'rejected';
    const PENDING_STATUS = 'pending';

    public function daycare() {
        return $this->belongsTo('App\Daycare','daycare_id');
    }

    public function user() {
        return $this->belongsTo('App\User','user_id');
    }

    public function isApproved() {
        return $this->approved == Tour::APPROVED_STATUS;
    }
}
