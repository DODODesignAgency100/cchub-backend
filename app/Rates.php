<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rates extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    public function daycares() {
        return $this->belongsTo('App\Daycare','daycare_id');
    }
}
