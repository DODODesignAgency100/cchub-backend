<?php

namespace App\Http\Controllers;

use App\Daycare;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AnalyticsController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:daycare');
    }
    
    function getAllMonths(){

		$month_array = array();

        $posts_dates = Daycare::join("daycare_views", "daycare_views.daycare_id", "=", "daycares.id")
                    ->where("daycares.id", '=', Auth::user()->id)
                    ->orderBy(DB::raw('(daycare_views.created_at)', 'asc'))
                    ->pluck("daycare_views.created_at");

        $posts_dates = json_decode( $posts_dates );

        // dd($posts_dates);

		if ( ! empty( $posts_dates ) ) {
			foreach ( $posts_dates as $unformatted_date ) {
				$date = new \DateTime( $unformatted_date );
				$month_no = $date->format( 'm' );
				$month_name = $date->format( 'M' );
				$month_array[ $month_no ] = $month_name;
			}
		}
		return $month_array;
	}

	function getMonthlyPostCount( $month ) {
        // $monthly_post_count = Post::whereMonth( 'created_at', $month )->get()->count();
        
        $monthly_post_count = Daycare::join("daycare_views", "daycare_views.daycare_id", "=", "daycares.id")
                                ->where("daycares.id", '=', Auth::user()->id)
                                ->whereMonth( 'daycare_views.created_at', $month )
                                ->get()->count();

        return $monthly_post_count;
        
	}

	function getMonthlyPostData() {

		$monthly_post_count_array = array();
		$month_array = $this->getAllMonths();
		$month_name_array = array();
		if ( ! empty( $month_array ) ) {
			foreach ( $month_array as $month_no => $month_name ){
				$monthly_post_count = $this->getMonthlyPostCount( $month_no );
				array_push( $monthly_post_count_array, $monthly_post_count );
				array_push( $month_name_array, $month_name );
			}
		}

		$max_no = max( $monthly_post_count_array );
		$max = round(( $max_no + 10/2 ) / 10 ) * 10;
		$monthly_post_data_array = array(
			'months' => $month_name_array,
			'post_count_data' => $monthly_post_count_array,
			'max' => $max,
		);

		return $monthly_post_data_array;

    }
}
