<?php

namespace App\Http\Controllers;

use App\Daycare;
use App\DaycareDescription;
use App\DaycarePhotos;
use App\Drop;
use App\Features;
use App\Mail\SpotApproved;
use App\Mail\SpotRejected;
use App\Mail\TourApproved;
use App\Mail\TourRejected;
use App\Rates;
use App\Review;
use App\Schedule;
use App\Services;
use App\Spot;
use App\Timeslot;
use App\Tour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Image;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Stripe\Stripe;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:daycare')->except(
            'daycareResources',
            'daycareAbout',
            'trust',
            'referrals',
            'getStarted'
        );
    }

    public function index()
    {
        $daycares = Daycare::all();
        return view('daycare/home', compact('daycares'));
    }

    public function getStarted() {
        return view('daycare/getStarted');
    }

    public function daycareAbout() {
        $daycares = Daycare::all();
        return view('daycare/about');
    }

    public function daycareResources() {
        return view('daycare/resources');
    }

    public function trust() {
        return view('daycare/trust-and-privacy');
    }

    public function referrals() {
        return view('daycare/referral');
    }

    public function dashboard() {
        $user = Auth::user();
        $rates = Rates::where("daycare_id", Auth::user()->id)->first();
        $service = Services::where("daycare_id", Auth::user()->id)->first();
        $feature = Features::where("daycare_id", Auth::user()->id)->first();;
        $drop = Drop::where("daycare_id", Auth::user()->id)->first();;
        $timeslot = Timeslot::where("daycare_id", Auth::user()->id)->first();
        $items = DaycareDescription::where("daycare_id", Auth::user()->id)->first();
        $pictures = DaycarePhotos::where("daycare_id", Auth::id())->first();

        // dd($rates);

        return view('daycare/dashboard', compact('user','rates','service','feature','drop','timeslot','items', 'pictures'));
    }

    public function userUpdate(Request $request, $id) {

        $user = Daycare::findOrFail($id);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->phone_number = $request->phone_number;

        $user->save();

        return redirect(route('daycare/dashboard'))->with('message','Profile successfully updated');

    }

    public function infoUpdate(Request $request, $id) {
        
        $info = Daycare::findOrFail($id);

        $info->business_name = $request->business_name;
        $info->business_address = $request->business_address;
        $info->license_number = $request->license_number;
        $info->b_phone = $request->b_phone;
        $info->show_address = $request->show_address;

        $info->slug = Str::slug($request->business_name, '-');

        $info->save();

        return redirect(route('daycare/dashboard'))->with('message','Profile successfully updated');

    }

    public function rates(Request $request) {

        $rate = new Rates();

        $rate->infant_age = $request->infant_age;
        $rate->infant_hours = $request->infant_hours;
        $rate->infant_daily_hours = $request->infant_daily_hours;
        $rate->infant_monthly_hours = $request->infant_monthly_hours;
        $rate->infant_weekly_hours = $request->infant_weekly_hours;
        $rate->infant_week_nights = $request->infant_week_nights;
        $rate->infant_weekend_overnights = $request->infant_weekend_overnights;
        $rate->infant_no_offers = $request->infant_no_offers;
        $rate->show_rates = $request->show_rates;
        $rate->daycare_id = Auth::user()->id;
        $rate->preschool_age = $request->preschool_age;
        $rate->preschool_hours = $request->preschool_hours;
        $rate->preschool_daily_hours = $request->preschool_daily_hours;
        $rate->preschool_monthly_hours = $request->preschool_monthly_hours;
        $rate->preschool_weekly_hours = $request->preschool_weekly_hours;
        $rate->preschool_week_nights = $request->preschool_week_nights;
        $rate->preschool_weekend_overnights = $request->preschool_weekend_overnights;
        $rate->preschool_no_offers = $request->preschool_no_offers;
        $rate->school_age = $request->school_age;
        $rate->school_hours = $request->school_hours;
        $rate->school_daily_hours = $request->school_daily_hours;
        $rate->school_monthly_hours = $request->school_monthly_hours;
        $rate->school_weekly_hours = $request->school_weekly_hours;
        $rate->school_week_nights = $request->school_week_nights;
        $rate->school_weekend_overnights = $request->school_weekend_overnights;
        $rate->school_no_offers = $request->school_no_offers;
        $rate->show_rates = $request->show_rates;

        $rate->save();

        return redirect()->back()->with('message','Rates successfully created');
    }

    public function ratesUpdate(Request $request, $id) {

        $rate = Rates::findOrFail($id);
        
        $rate->infant_age = $request->infant_age;
        $rate->infant_hours = $request->infant_hours;
        $rate->infant_daily_hours = $request->infant_daily_hours;
        $rate->infant_monthly_hours = $request->infant_monthly_hours;
        $rate->infant_weekly_hours = $request->infant_weekly_hours;
        $rate->infant_week_nights = $request->infant_week_nights;
        $rate->infant_weekend_overnights = $request->infant_weekend_overnights;
        $rate->infant_no_offers = $request->infant_no_offers;
        $rate->show_rates = $request->show_rates;
        $rate->daycare_id = Auth::user()->id;
        $rate->preschool_age = $request->preschool_age;
        $rate->preschool_hours = $request->preschool_hours;
        $rate->preschool_daily_hours = $request->preschool_daily_hours;
        $rate->preschool_monthly_hours = $request->preschool_monthly_hours;
        $rate->preschool_weekly_hours = $request->preschool_weekly_hours;
        $rate->preschool_week_nights = $request->preschool_week_nights;
        $rate->preschool_weekend_overnights = $request->preschool_weekend_overnights;
        $rate->preschool_no_offers = $request->preschool_no_offers;
        $rate->school_age = $request->school_age;
        $rate->school_hours = $request->school_hours;
        $rate->school_daily_hours = $request->school_daily_hours;
        $rate->school_monthly_hours = $request->school_monthly_hours;
        $rate->school_weekly_hours = $request->school_weekly_hours;
        $rate->school_week_nights = $request->school_week_nights;
        $rate->school_weekend_overnights = $request->school_weekend_overnights;
        $rate->school_no_offers = $request->school_no_offers;
        $rate->show_rates = $request->show_rates;

        $rate->save();

        return redirect(route('daycare/dashboard'))->with('message','Rates successful updated');
    }

    public function service(Request $request) {

        $services = new Services();

        $services->total_kids = $request->total_kids;
        $services->total_teachers = $request->total_teachers;
        $services->meals_offered = json_encode($request->meals_offered);
        $services->daycare_languages = json_encode($request->daycare_languages);
        $services->teacher_language_level = $request->teacher_language_level;
        $services->daycare_id = Auth::user()->id;

        $services->save();

        return redirect()->back()->with('message','Services successfully created');
    }

    public function serviceUpdate(Request $request, $id) {

        $service = Services::find($id);
        
        $service->total_kids = $request->total_kids;
        $service->total_teachers = $request->total_teachers;
        // $service->meals_offered = implode(',', $request->meals_offered);
        $service->meals_offered = json_encode($request->meals_offered);
        $service->daycare_languages = json_encode($request->daycare_languages);
        $service->teacher_language_level = $request->teacher_language_level;
        $service->daycare_id = Auth::user()->id;

        $service->save();

        return redirect(route('daycare/dashboard'))->with('message','Services successfully updated');
    }

    public function feature(Request $request) {

        $features = new Features();

        $features->features = json_encode($request->features);
        // $feature->meals_offered = implode(',', $request->meals_offered);
        $features->daycare_id = Auth::user()->id;

        $features->save();

        return redirect()->back()->with('message','Daycare features successfully created');
    }

    public function featureUpdate(Request $request, $id) {

        $feature = Features::findOrFail($id);
        
        $feature->features = json_encode($request->features);
        // $feature->meals_offered = implode(',', $request->meals_offered);
        $feature->daycare_id = Auth::user()->id;

        $feature->save();

        return redirect(route('daycare/dashboard'))->with('message','Features successfully updated');
    }

    public function drop(Request $request) {

        $drops = new Drop();

        $drops->drops = json_encode($request->drops);
        // $feature->meals_offered = implode(',', $request->meals_offered);
        $drops->daycare_id = Auth::user()->id;

        $drops->save();

        return redirect()->back()->with('message','Drop-offs Successfully Created');
    }

    public function dropUpdate(Request $request, $id) {

        $drop = Drop::findOrFail($id);
        
        $drop->drops = json_encode($request->drops);
        // $feature->meals_offered = implode(',', $request->meals_offered);
        $drop->daycare_id = Auth::user()->id;

        $drop->save();

        return redirect(route('daycare/dashboard'))->with('message','Drop-offs Successfully Updated');
    }

    public function timeslot(Request $request) {

        $timeslots = new Timeslot();

        $timeslots->rOpenhour = $request->rOpenhour;
        $timeslots->rClosehour = $request->rClosehour;
        $timeslots->wknOpenHour = $request->wknOpenHour;
        $timeslots->wknCloseHour = $request->wknCloseHour;
        $timeslots->wkcOpenHour = $request->wkcOpenHour;
        $timeslots->wkcCloseHour = $request->wkcCloseHour;
        $timeslots->ovrOpenHour = $request->ovrOpenHour;
        $timeslots->ovrCloseHour = $request->ovrCloseHour;
        $timeslots->sociallinkfacebook = $request->sociallinkfacebook;
        $timeslots->sociallinktwitter = $request->sociallinktwitter;
        $timeslots->sociallinkinstagram = $request->sociallinkinstagram;
        $timeslots->website = $request->website;
        // $timeslots->show_Address = $request->show_address;

        $timeslots->daycare_id = Auth::user()->id;

        $timeslots->save();

        return redirect()->back()->with('message','Timeslot Successfully Created');
    }

    public function timeslotUpdate(Request $request, $id) {

        $timeslot = Timeslot::findOrFail($id);
        
        $timeslot->rOpenhour = $request->rOpenhour;
        $timeslot->rClosehour = $request->rClosehour;
        $timeslot->wknOpenHour = $request->wknOpenHour;
        $timeslot->wknCloseHour = $request->wknCloseHour;
        $timeslot->wkcOpenHour = $request->wkcOpenHour;
        $timeslot->wkcCloseHour = $request->wkcCloseHour;
        $timeslot->ovrOpenHour = $request->ovrOpenHour;
        $timeslot->ovrCloseHour = $request->ovrCloseHour;
        $timeslot->sociallinkfacebook = $request->sociallinkfacebook;
        $timeslot->sociallinktwitter = $request->sociallinktwitter;
        $timeslot->sociallinkinstagram = $request->sociallinkinstagram;
        $timeslot->website = $request->website;
        
        $timeslot->daycare_id = Auth::user()->id;

        $timeslot->save();

        return redirect(route('daycare/dashboard'))->with('message','Daycare Timeslot Successfully Updated');
    }

    public function schedule() {
        $schedule = Schedule::where("daycare_id", Auth::user()->id)->first();
        return view('daycare.schedule', compact('schedule'));
    }

    public function schedules(Request $request) {
        $schedules = new Schedule();

        $schedules->weekdayOff = $request->weekdayOff;
        $schedules->weekendOff = $request->weekendOff;
        $schedules->holidayOff = $request->holidayOff;
        $schedules->daycare_id = Auth::user()->id;

        $schedules->save();

        return redirect()->back()->with('message','Schedule Successfully Created');
    }

    public function scheduleUpdate(Request $request, $id) {

        $schedule = Schedule::findOrFail($id);
        
        $schedule->weekdayOff = $request->weekdayOff;
        $schedule->weekendOff = $request->weekendOff;
        $schedule->holidayOff = $request->holidayOff;
        $schedule->daycare_id = Auth::user()->id;

        $schedule->save();

        return redirect(route('daycare/schedule'))->with('message','Schedule Successfully Updated');
    }

    public function tour() {
        $tours = Tour::where('status', '=', 'pending')->orWhere('daycare_id',Auth::user()->id)->with('user')->get();
        return view('daycare.tour', compact('tours'));
    }

    public function tourApproved() {
        $tours = Tour::where('status', '=', 'approved')->orWhere('daycare_id', Auth::user()->id)->with('user')->get();
        return view('daycare.tour-approved', compact('tours'));
    }

    public function tourRejected() {
        $tours = Tour::where('status', '=', 'rejected')->orWhere('daycare_id', Auth::user()->id)->with('user')->get();
        return view('daycare.tour-rejected', compact('tours'));
    }

    public function tourApprove($id) {
        $tour = Tour::where('id', '=', e($id))->first();

        if($tour) {
            
            $tour->status = Tour::APPROVED_STATUS;;
            $tour->save();

            Mail::to($tour->user->email)->queue(new TourApproved($tour));

            return back()->with('message','You have succesfully approved a pending tour request');
        }
    }

    public function tourReject($id) {
        $tour = Tour::where('id', '=', e($id))->first();

        if($tour) {
            
            $tour->status = Tour::REJECTED_STATUS;;
            $tour->save();

            Mail::to($tour->user->email)->queue(new TourRejected($tour));

            return back()->with('message','You have rejected a pending tour request');
        }
    }

    public function waitlist() {

        $totalSpace = Services::where("daycare_id", Auth::user()->id)->get();
        $totalSpot = Spot::where('status','=','approved')->count();

        // dd($totalSpace);

        if (!is_null($totalSpace)) {
            $totalSpaceLeft = $totalSpace->total_kids - $totalSpot;
        
            if ($totalSpaceLeft <= 0) {

                $skip = $totalSpace->total_kids;
                $limit = $totalSpot - $skip;

                $waitlists = Spot::where('status','=','pending')
                                ->orderBy('created_at', 'DESC')
                                ->skip( $skip )->take($limit)
                                ->get();

                return view('daycare.waitlist', compact('waitlists'));
            }
        }
        
        return view('daycare.waitlist');
    }

    public function waitlistApprove($id) {
        $reservation = Spot::where('id', '=', e($id))->first();

        if($reservation) {
            
            $reservation->status = Spot::APPROVED_STATUS;;
            $reservation->save();

            return back()->with('message','You have succesfully approved a pending reservation');
        }
    }

    public function waitlistReject($id) {
        $reservation = Spot::where('id', '=', e($id))->first();

        if($reservation) {
            
            $reservation->status = Spot::REJECTED_STATUS;;
            $reservation->save();

            return back()->with('message','You have rejected a pending reservation');
        }
    }

    public function reservation() {
        $reservations = Spot::whereStatus('pending')
                            ->where('daycare_id', Auth::user()->id)->with('user')->get();

        return view('daycare.reservation', compact('reservations'));
    }

    public function spotApprove($id) {
        $reservation = Spot::where('id', '=', e($id))->with('user')->first();

        // dd($reservation);

        if($reservation) {
            
            $reservation->status = Spot::APPROVED_STATUS;;
            $reservation->save();

            Mail::to($reservation->user->email)->queue(new SpotApproved($reservation));

            return back()->with('message','You have succesfully approved a pending reservation');
        }
    }

    public function spotReject($id) {
        $reservation = Spot::where('id', '=', e($id))->first();

        if($reservation) {
            
            $reservation->status = Spot::REJECTED_STATUS;;
            $reservation->save();

            Mail::to($reservation->user->email)->queue(new SpotRejected($reservation));

            return back()->with('message','You have rejected a pending reservation');
        }
    }

    public function review() {
        
        $reviews = Review::where('daycare_id','=', Auth::user()->id)->with('user')->get();
        // $most_reviewed = Review::all()->select('daycare_id')->get()->count();

        // return dd($most_reviewed);

        return view('daycare.reviews', compact('reviews'));
    }

    public function uploadImage(Request $request, $id) {

        $user = Daycare::findOrFail($id);

        if ($request->hasFile('director_image')) {
            $image = $request->file('director_image');
            $filename = $image->getClientOriginalName();
            $location = 'daycare/'. $filename;
            $img = Image::make($image)->save($location);
            $user->director_image = $filename;
        }

        // $user->daycare_id = Auth::user()->id;

        $user->save();

        // dd($daycare_image);

        return back()->with('message', 'Image uploaded successfully');
    }

    public function uploadPhoto(Request $request, $id) {

        $user = Daycare::findOrFail($id);

        if ($request->hasFile('daycare_photo')) 
        {

            foreach ($request->file('daycare_photo') as $image) {
                $filename = $image->getClientOriginalName();
                $location = 'daycare/'. $filename;
                $img = Image::make($image)->save($location);
                $data[] = $filename;
            }
            // $image = $request->file('daycare_photo');
            
        }
        
        $user->daycare_photo = json_encode($data);
        // dd($data);
        $user->save();

        return back()->with('message', 'Image uploaded successfully');
    }

    public function analytics() {

        // $daycareviews_daily = Daycare::join("daycare_views", "daycare_views.daycare_id", "=", "daycares.id")
        //     ->where("daycare_views.created_at", ">=", date("Y-m-d H:i:s", strtotime('-24 hours', time())))
        //     ->groupBy("daycares.id")
        //     ->orderBy(DB::raw('COUNT(daycares.id)', 'desc'))
        //     ->get(array(DB::raw('COUNT(daycares.id) as total_views'), 'daycares.*'));

        // $daycareviews_day = Daycare::join("daycare_views", "daycare_views.daycare_id", "=", "daycares.id")
        //                             ->select([
        //                                     // This aggregates the data and makes available a 'count' attribute
        //                                     DB::raw('COUNT(daycares.id) as total_views'), 'daycares.*', 
        //                                     // This throws away the timestamp portion of the date
        //                                     DB::raw('DATE(daycares.created_at) as day'), 'daycares.*'
        //                                 // Group these records according to that day
        //                                 ])->groupBy('daycares.id')
        //                                 // And restrict these results to only those created in the last week
        //                                 ->where('daycare_views.created_at', '>=', date("Y-m-d H:i:s", strtotime(time())))
        //                                 ->first(array(DB::raw('COUNT(daycares.id) as total_views'), 'daycares.*'));

        // $daycareviews_weekly = Daycare::join("daycare_views", "daycare_views.daycare_id", "=", "daycares.id")
        //     ->where("daycare_views.created_at", ">=", date("Y-m-d H:i:s", strtotime('-168 hours', time())))
        //     ->groupBy("daycares.id")
        //     ->orderBy(DB::raw('COUNT(daycares.id)', 'desc'))
        //     ->get(array(DB::raw('COUNT(daycares.id) as total_views'), 'daycares.*'));

        $dateNow = Carbon::now();

        $reservations = Spot::whereStatus('approved')
                                ->where('daycare_id', Auth::user()->id)->with('user')->get();
                                
        $rejectedReservations = Spot::whereStatus('rejected')->where('daycare_id', Auth::user()->id)
                                        ->with('user')->get();
            
        return view('daycare.analytics', compact( 'dateNow', 'reservations', 'rejectedReservations'));
    }

    public function storeDescription(Request $request) {

        $description = new DaycareDescription();

        $this->validate($request, [
            'overview' => 'required',
            'director' => 'required',
            'tour_policy' => 'required',
        ]);

        $description->overview = $request->overview;
        $description->director = $request->director;
        $description->dailyScheduleTime1 = $request->dailyScheduleTime1;
        $description->dailyScheduleTime2 = $request->dailyScheduleTime2;
        $description->dailyScheduleTime3 = $request->dailyScheduleTime3;
        $description->dailyScheduleActivity1 = $request->dailyScheduleActivity1;
        $description->dailyScheduleActivity2 = $request->dailyScheduleActivity2;
        $description->dailyScheduleActivity3 = $request->dailyScheduleActivity3;
        $description->tour_policy = $request->tour_policy;
        $description->provide_tour = $request->provide_tour;
        $description->daycare_id = Auth::user()->id;

        $description->save();

        return back()->with('message', 'Description created successfully');
    }

    public function updateDescription(Request $request, $id) {

        $description = DaycareDescription::findOrFail($id);

        $description->overview = $request->overview;
        $description->director = $request->director;
        $description->dailyScheduleTime1 = $request->dailyScheduleTime1;
        $description->dailyScheduleTime2 = $request->dailyScheduleTime2;
        $description->dailyScheduleTime3 = $request->dailyScheduleTime3;
        $description->dailyScheduleActivity1 = $request->dailyScheduleActivity1;
        $description->dailyScheduleActivity2 = $request->dailyScheduleActivity2;
        $description->dailyScheduleActivity3 = $request->dailyScheduleActivity3;
        $description->tour_policy = $request->tour_policy;
        $description->provide_tour = $request->provide_tour;
        $description->daycare_id = Auth::user()->id;

        $description->save();

        return back()->with('message', 'Description updated successfully');
    }

    public function getReminder() 
    {
        $dateNow = Carbon::now();
        $reservations = Spot::where('status','=','approved')->with('user')->get();

        return view('daycare.reminder', compact('dateNow', 'reservations'));
    }

    public function subscribe()
    {
        $user = Auth::user();

        $monthly = 'price_1HFfRkAsaHW5w0ugJ5nGfAQu';
        $yearly = 'price_1HFfRkAsaHW5w0ugg3u7edru';

        $data = [
            'intent' => $user->createSetupIntent(),
            'monthly' => $monthly,
            'yearly' => $yearly
        ];

        // dd($data);
        return view('daycare.subscribe')->with($data);
    }

    public function pay(Request $request) {
        // $user = Auth::user();

        // $paymentMethod = $request->stripeToken;
        
        // $user->newSubscription('subscription' ,'price_1HFfRkAsaHW5w0ugJ5nGfAQu')->create($paymentMethod);

        // return redirect()->route('daycare/dashboard')->with('message', 'Welcome to your dashboard');
        
        // $paymentMethod = $request->paymentMethod;
        
            $user = auth()->user();
            $input = $request->all();
            $token =  $request->stripeToken;
            $paymentMethod = $request->paymentMethod;
            
            try {

                Stripe::setApiKey(env('STRIPE_SECRET'));
                
                if (is_null($user->stripe_id)) {
                    $stripeCustomer = $user->createAsStripeCustomer();
                }

                \Stripe\Customer::createSource(
                    $user->stripe_id,
                    ['source' => $token]
                );

                $user->newSubscription('Premium','price_1HFfRkAsaHW5w0ugJ5nGfAQu')
                    ->create($paymentMethod, [
                    'email' => $user->email,
                ]);

                return redirect()->route('daycare/dashboard')->with('message', 'Welcome to your dashboard');
                
            } catch (\Exception $e) {
                return back()->with('error',$e->getMessage());
            }

        
        // dd($paymentMethod);

        // $user->createOrGetStripeCustomer();
        // // $user->addPaymentMethod($paymentMethod);
        // $user->newSubscription('Premium', 'price_1HFfRkAsaHW5w0ugJ5nGfAQu')
        //     ->create($request->stripeToken, [
        //         'email' => $user->email,
        //     ]);
            
        // return redirect()->route('daycare/dashboard')->with('message', 'Welcome to your dashboard');
    }

    public function cancelSub() {
        // $user = Auth::user();
        
        // $user->subscription('Premium')->cancel();

        // return 'canceled';
        
            $user = auth()->user();
            // $input = $request->all();
            // $token =  $request->stripeToken;
            // $paymentMethod = $request->paymentMethod;
            
            try {

                Stripe::setApiKey(env('STRIPE_SECRET'));
                
                // if (is_null($user->stripe_id)) {
                //     $stripeCustomer = $user->createAsStripeCustomer();
                // }

                // \Stripe\Customer::createSource(
                //     $user->stripe_id,
                //     ['source' => $token]
                // );

                $user->subscription('Premium')->cancel();

                return redirect()->route('daycare/subscribe');
                
            } catch (Exception $e) {
                return back()->with('error',$e->getMessage());
            }
    }

    public function resumeSub() 
    {
        // $user = Auth::user();

        // $user->subscription('Premium')->resume();

        // return redirect()->route('daycare/dashboard')->with('message', 'Welcome to your dashboard');
        
        $user = auth()->user();
        $input = $request->all();
        $token =  $request->stripeToken;
        $paymentMethod = $request->paymentMethod;
            
        try {

            Stripe::setApiKey(env('STRIPE_SECRET'));
                
            if (is_null($user->stripe_id)) {
                $stripeCustomer = $user->createAsStripeCustomer();
            }

            // \Stripe\Customer::createSource(
            //     $user->stripe_id,
            //     ['source' => $token]
            // );

            $user->subscription('Premium')->resume();

            return redirect()->route('daycare/dashboard')->with('message', 'Welcome to your dashboard');
                
        } catch (Exception $e) {
            return back()->with('error',$e->getMessage());
        }
    }

    public function payYearly() {
        // $user = Auth::user();
        
        // dd($request->all());
        
        // $user->newSubscription('subscription' ,'price_1HFfRkAsaHW5w0ugg3u7edru')->create();

        // return redirect()->route('daycare/dashboard')->with('message', 'Welcome to your dashboard');
        
            $user = auth()->user();
            $input = $request->all();
            $token =  $request->stripeToken;
            $paymentMethod = $request->paymentMethod;
            
            try {

                Stripe::setApiKey(env('STRIPE_SECRET'));
                
                if (is_null($user->stripe_id)) {
                    $stripeCustomer = $user->createAsStripeCustomer();
                }

                \Stripe\Customer::createSource(
                    $user->stripe_id,
                    ['source' => $token]
                );

                $user->newSubscription('Premium','price_1HFfRkAsaHW5w0ugg3u7edru')
                    ->create($paymentMethod, [
                    'email' => $user->email,
                ]);

                return redirect()->route('daycare/dashboard')->with('message', 'Welcome to your dashboard');
                
            } catch (\Exception $e) {
                return back()->with('error',$e->getMessage());
            }
    }

    public function messages()
    {
        return view('daycare.messages');
    }

    public function cancel()
    {
        return view('daycare.cancel');
    }

}
