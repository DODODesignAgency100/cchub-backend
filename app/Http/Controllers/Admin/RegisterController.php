<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class RegisterController extends Controller
{
    //
    protected $redirectTo = 'admin/dashboard';

    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function showRegistrationForm() {
        return view('admin.register');
    }

    public function register(Request $request)
    {
        $data = $this->validate($request, [
            'email' => ['required','email','unique:admins'],
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => ['required','min:8','confirmed'],
            'phone_number' => 'required',
        ]);
        
        $data['admin'] = Admin::IS_SUPERAMNIN;
        $data['password'] = Hash::make($data['password']);
        $data['verification_token'] = Admin::generateVerificationCode();

        $admin = Admin::Create($data);
        Auth::guard('admin')->loginUsingId($admin->id);

        // return redirect()->route('daycare/dashboard');
        return redirect()->intended('admin/dashboard');
    }
}
