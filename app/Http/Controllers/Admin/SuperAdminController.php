<?php

namespace App\Http\Controllers\Admin;

use App\Daycare;
use App\Http\Controllers\Controller;
use App\Report;
use App\Review;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SuperAdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function dashboard() 
    {
        $users = DB::table('users')->get();
        $daycares = DB::table('daycares')->get();
        $newUser = DB::table('users')->where('created_at', Carbon::today())
                                    ->get();
        $newDaycare = DB::table('daycares')->where('created_at', Carbon::today())
                                    ->get();  
        $allDaycares = Daycare::selectRaw('province, COUNT(*) as count')
                                ->groupBy('province')
                                ->get();

        return view('admin.dashboard', compact('users', 'daycares', 'newUser', 'newDaycare', 'allDaycares'));
    }

    public function report()
    {
        $reports = Review::with('daycare')->get();
        // dd($reports);
        return view('admin.report', compact('reports'));
    }

    public function providers() 
    {
        return view('admin.provider');
    }
}
