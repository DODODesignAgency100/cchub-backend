<?php

namespace App\Http\Controllers;

use App\Daycare;
use App\DaycareView;
use App\Mail\spot\daycareSPotBooked;
use App\Mail\spot\userSpotBooked;
use App\Mail\tour\daycareTourBooked;
use App\Mail\tour\userTourBooked;
use App\Rates;
use App\Report;
use App\Review;
use App\Services;
use App\Spot;
use App\Tour;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
   
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function about()
    {
        return view('parents.about');
    }

    public function daycares(Daycare $post) {

        if (Auth::guest()) {
            return redirect()->guest(route('login'));
        }

        $reviews = $post->review()->with('user')->whereNotNull('comments')->get();
        $nearby_daycare = $post->where('b_province', 'like', '%' .Auth::user()->province)
                                ->take(5)->get()->except($post->id);
        $totalSpace = Services::all();
        $totalSpot = Spot::count();
        $id = $post->id;

        if(! ( Session::get('id') == $id)){

            // $views = Daycare::where('id', $id)->increment('view_count');

            $daycareView = new DaycareView();
            $daycareView->daycare_id = $post->id;
            $daycareView->business_name = $post->business_name;
            $daycareView->session_id = request()->getSession()->getId();
            // $daycareView->user_id = Auth::user()->id;
            $daycareView->ip = request()->getClientIp();
            $daycareView->agent = request()->header('User-Agent');
            $daycareView->save();

            Session::put('id', $id);
        }

        return view('parents.listings', compact('post', 'reviews','nearby_daycare','totalSpace','totalSpot'));
    }

    public function search(Request $request, Daycare $post) {
        $search = $request->get('search');
        $result = Daycare::where('b_zip','like','%'.$search.'%')->orWhere('b_province','like','%'.$search.'%')->orderBy('created_at','DESC')->get();
        $totalSpace = Services::all();
        $totalSpot = Spot::count();

        return view('parents.result', compact('result','search', 'totalSpace', 'totalSpot'));
    }

    public function terms() {
        return view('terms');
    }

    public function privacy() {
        return view('privacy');
    }

    public function sale() {
        return view('sale');
    }

    public function help() {
        return view('help');
    }

    public function reserve(Request $request) {

        if (Auth::guest()) {
            return redirect()->guest(route('login'));
        }

        else {
            $this->validate($request, [
                'parent_name' => 'required',
                'kid_name' => 'required',
                'kid_age' => 'required',
                'address' => 'required',
                'allergies' => 'required',
                'language' => 'required',
            ]);

            $spot = new Spot();

            $spot->parent_name = $request->parent_name;
            $spot->kid_name = $request->kid_name;
            $spot->kid_age = $request->kid_age;
            $spot->address = $request->address;
            $spot->allergies = $request->allergies;
            $spot->language = $request->language;
            $spot->user_id = Auth::user()->id;
            $spot->daycare_id = $request->daycare_id;

            $daycare = Daycare::where('id', '=', e($request->daycare_id))->first();
            $user = Auth::user();

            $spot->save();

            Mail::to(Auth::user()->email)->queue(new userSpotBooked($daycare));
            Mail::to($daycare->b_email)->queue(new daycareSPotBooked($user));

            return back()->with('message', 'Spot successfully booked');
        }
    }

    public function report(Request $request) {
        if (Auth::guest()) {
            return redirect()->guest(route('login'));
        }
        else {
            $this->validate($request, [
                'report' => 'required',
            ]);

            $report = new Report();

            $report->report = $request->report;
            $report->daycare_id = $request->daycare_id;
            $report->user_id = Auth::user()->id;

            $report->save();

            return back()->with('message', 'Your report has been sent');
        }
    }

    public function tour(Request $request) {
        if (Auth::guest()) {
            return redirect()->guest(route('login'));
        }
        else {
            $this->validate($request, [
                'parent_name' => 'required',
                'kid_age' => 'required',
                'date' => 'required',
                'time' => 'required',
            ]);

            $tour = new Tour();

            $tour->parent_name = $request->parent_name;
            $tour->kid_age = $request->kid_age;
            $tour->date = $request->date;
            $tour->time = $request->time;
            $tour->daycare_id = $request->daycare_id;
            $tour->user_id = Auth::user()->id;

            $daycare = Daycare::where('id', '=', e($request->daycare_id))->first();
            $user = Auth::user();

            $tour->save();

            Mail::to(Auth::user()->email)->queue(new userTourBooked($daycare));
            Mail::to($daycare->b_email)->queue(new daycareTourBooked($user));

            return back()->with('message', 'Your tour has been scheduled for ' . date_format(new DateTime( $tour->date ),'l F j, Y') . ' and time ' . date_format(new DateTime( $tour->time ), 'g:ia' ) . '');
        }
    }

    public function review(Request $request) {
        if (Auth::guest()) {
            return redirect()->guest(route('login'));
        }
        else {
            // $this->validate($request, [
            //     'comments' => 'required',
            // ]);

            $review = new Review();

            $review->comments = $request->comments;
            $review->daycare_id = $request->daycare_id;
            $review->user_id = Auth::user()->id;
            $review->rate = $request->rate;

            $review->save();

            return back()->with('message', 'Your review has been submitted');
        }
    }

    public function sell() {
        return view('daycare/sale');
    }

    public function termsUse() {
        return view('daycare.terms');
    }

    public function parent()
    {
        $daycares = Daycare::where('featured', 1)->limit(6)->get();
        
        return view('parent-index', compact('daycares'));
    }

    // public function daycareAbout() {
    //     return view('daycare.about');
    // }

    // public function daycareResources() {
    //     return view('daycare/resources');
    // }

    public function resources() 
    {
        return view('faq-getting-started');
    }

    public function faq() 
    {
        return view('faq-searching');
    }

    public function connect() 
    {
        return view('faq-connecting');
    }

    public function filter(Request $request, Daycare $post) 
    {
        $age = $request->get('age');
        $result = DB::table('rates', 'daycares')
                        ->where('infant_age','like','%'.$age.'%')
                        ->orWhere('preschool_age','like','%'.$age.'%')
                        ->orWhere('school_age','like','%'.$age.'%')
                        ->get();
        $totalSpace = Services::all();
        $totalSpot = Spot::count();

        dd($result);

        return view('parents.result', compact('result','age', 'totalSpace', 'totalSpot', 'post'));
    }

    public function allDaycares() {
        $daycares = Daycare::all();
        $totalSpace = Services::all();
        $totalSpot = Spot::count();

        // dd($daycares, $totalSpace, $totalSpot);

        return view('parents.daycares', compact('daycares','totalSpace','totalSpot'));
    }

    public function getMessages()
    {
        return view('parents.messages');
    }

    public function getChat() 
    {
        return view('parents.chat');
    }

    public function daycareWaitlist() 
    {
        if (Auth::guest()) {
            return redirect()->guest(route('login'));
        }
        else {
            
            $totalSpace = Services::all();
            $totalSpot = Spot::count();
            $daycares = Daycare::orderBy('business_name', 'asc')->get();

            return view('parents.daycare-list', compact('totalSpace', 'totalSpot', 'daycares'));
        }
    }

    public function Waitlist(Daycare $post)
    {
        if (Auth::guest()) {
            return redirect()->guest(route('login'));
        }
        else {
            $totalSpace = Services::where("daycare_id", $post->id)->first();
            $totalSpot = Spot::where('daycare_id','=',$post->id)->count();

            // dd($totalSpot);

            if (!is_null($totalSpace)) {
                $totalSpaceLeft = $totalSpace->total_kids - $totalSpot;
                // dd($totalSpaceLeft);
                if ($totalSpaceLeft <= 0) {

                    $skip = $totalSpace->total_kids;
                    $limit = $totalSpot - $skip;

                    $waitlists = $post->spots()->sortBy('parent_name')
                                        ->skip( $skip )->take( 3 );
                    
                    
                    return view('parents.waitlist', compact('waitlists', 'post'));
                }
            }
        }
    }

    public function getContact()
    {
        return view('parents.contact');
    }

}
