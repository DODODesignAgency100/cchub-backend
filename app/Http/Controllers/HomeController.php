<?php

namespace App\Http\Controllers;

use App\Daycare;
use App\Events\MessageSent;
use App\Message;
use App\reminder;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Events\MessageSent as EventsMessageSent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Image;
use Pusher\Pusher;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $daycares = Daycare::where('featured', 1)->limit(6)->get();
        return view('home', compact('daycares'));
    }

    public function profile(Request $request, $id) {
        $user = User::findOrFail($id);

        return view('parents.profile', compact('user'));
    }

    public function profileUpdate(Request $request, $id) {

        $user = User::findorFail($id);

        if ($request->hasFile('image')) {
            $uimage = $request->file('image');
            $filename = $uimage->getClientOriginalName();
            $location = '/user/avatars/'. $filename;
            $img = Image::make($uimage)->save($location);   
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->province = $request->province;
        $user->phone_number = $request->phone_number;
        $user->email = $request->email;
        $user->image = $filename;

        $user->save();

        return back()->with('message', 'Profile updated successfully');
    }

    public function passwordUpdate(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            return back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $user = User::findorFail($id);

        $user->password = Hash::make($request->password);

        $user->save();

        return back()->with('message', 'Password changed successfully');
    }

    public function getMessages(User $user)
    {
        $mid = Auth::user();
        // $users = DB::select("select users.id, users.first_name, users.last_name, users.image, users.email, messages.created_at, messages.message, count(is_read) as unread, GROUP_CONCAT(users.first_name) as names 
        // from users LEFT  JOIN  messages ON users.id = messages.user_id and is_read = 0 and messages.receiver_id = " . Auth::id() . "
        // where users.id != " . Auth::id() . " 
        // GROUP BY users.id, users.first_name, users.last_name, users.image, users.email, messages.created_at, messages.message
        // ORDER BY messages.created_at desc");

        // while ($userRow = $users->fetch_assoc()) {
        //     $user[] = $userRow;
        // }

        // dd($user);

        // $userss = array_map(function($object){
        //     return (array) $object;
        // }, $users);

        // // dd($userss);
        // $user_id_count = array();
        // foreach ($userss as $key => $user) {
        //     $user_id_count[$user['first_name']][$key] = $user;
        // }

        // foreach ($userss as $index => $user) {
        //     // $user_id_count[$user['first_name']][$key] = $user;
        //     foreach ($user as $key => $value) {
        //         $userss[$index][$key]['first_name'];
        //     }
        // }

        // dd($userss);

        // $allUserMessage = (object) $user_id_count;

        // foreach ($allUserMessage as $user) {
        //     dd($user[0]);
        // }

        // $newUser = ksort($user_id_count, SORT_NUMERIC);

        // dd($user_id_count);

        // $users = Message::where('from', '=', Auth::id())->with('user')->get();
        // $users = $user->messages()->orderBy('updated_at', 'Desc')->get();
        // $users = Message::with('user')
        //                 ->where(['user_id' => $mid, 'receiver_id' => $user->id])
        //                 ->orWhere(function($query) use($user, $mid){
        //                     $query->where(['user_id' => $user->id, 'receiver_id' => $mid]);
        //                 })
        //                 ->get();
        // dd($users);
        $users = Message::where('receiver_id', $mid->id)->with('user')->get();

        // dd ($result);
        // dd($users);
        return view('parents.messages', compact('users'));
    }

    public function getChat($id)
    {
        $mid = Auth::id();
        $messages = Message::with('user')
                            ->where(['user_id' => $mid, 'receiver_id' => $id])
                            ->orWhere(function($query) use($id, $mid){
                                $query->where(['user_id' => $id, 'receiver_id' => $mid]);
                            })
                            ->get();

        // $Allmessages = json_encode($messages);

        // return $messages;
        return view('parents.chat', compact('messages','id'));
    }

    public function getMessage($id)
    {
        //Select all users except logged in user
        $mid = Auth::id();

        //get all messages for a selected user
        // $messages = Message::where(function($query) use($id, $mid){
        //             $query->where('from', $id)->where('to', $mid);
        //         })->orWhere(function($query) use($id, $mid) {
        //             $query->where('from', $id)->where('to', $mid);
        //         })->get();

        $allMessages = Message::with(['receiver', 'owner'])
                            ->where(['user_id' => $mid, 'receiver_id' => $id])
                            ->orWhere(function($query) use($id, $mid){
                                $query->where(['user_id' => $id, 'receiver_id' => $mid]);
                            })
                            ->get();

        // dd($allMessages[0]);
        return $allMessages;
    }

    // public function sendMessage(Request $request)
    // {
        // receiver,owner
    //     $user = Auth::user();
    //     // $to = $request->receiver_id;
    //     $message = $request->message;

    //     $data = new Message();
    //     $data->user_id = $user->id;
    //     // $data->to = $to;
    //     $data->message = $message;
    //     $data->is_read = 0;
    //     $data->save();

    //     //pusher 
    //     broadcast(new MessageSent($user, $request->message))->toOthers();
    // }

    public function sendMessage(Request $request)
    {
        $loggedUser = Auth::user();
        $input = $request->all();
        $input['is_read'] = 0;
        $message = $loggedUser->messages()->create($input);

        //pusher 
        broadcast(new MessageSent($message->load(['receiver', 'owner'])))->toOthers();

        return response(['message'=>'Message Sent Successfully', 'message'=>$message]);
    }

    public function getReminder($id) 
    {
        $reminder = reminder::with(['user', 'daycare'])->where('id', $id)->first();

        if (isset($reminder)) {
            return view('parents.renew')->with(['reminder' => $reminder]);
        } else {
            return view('errors.404');
        }
        
    }
    
}
