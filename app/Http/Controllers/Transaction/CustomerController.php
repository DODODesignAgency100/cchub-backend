<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\Stripe\Customer;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    //
    public function save(Request $request) 
    {
        $this->validate($request, [
            'name' => 'required',
            'cc_number' => 'required',
            'month' => 'required',
            'year' => 'required',
            'cvv' => 'required',
        ]);

        $card = [
            'card' => [
                'number' => $request->cc_number,
                'exp_month' => $request->month,
                'exp_year' => $request->year,
                'cvc' => $request->cvv,
            ]
        ];

        $user = Auth::user();

        //Create Stripe Customer via Service
        Customer::save($user, $card);

        $urls = array();
        if(Session::has('links')){
            $urls[] = Session::get('links');
         }

        return redirect()->back()->with('message', 'Card has been saved');
    }
}
