<?php

namespace App\Http\Controllers\Transaction;

use App\Daycare;
use App\Http\Controllers\Controller;
use App\Services\Stripe\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //
    public function renew(Request $request) 
    {
        $user = Auth::user();
        $daycare = Daycare::where('id', '=', $request->daycare_id)->first();

        if(is_null($daycare))
        {
            return back()->with('error', 'Daycare not found');
        }

        //Create the transaction
        Transaction::create($user, $daycare, $request);
        return redirect(route('profile',$user->id))->with('message', sprintf('You have succesfully renewed daycare service with %s', $daycare->business_name));
    }

    public function getCard()
    {
        return view('parents.card');
    }
}
