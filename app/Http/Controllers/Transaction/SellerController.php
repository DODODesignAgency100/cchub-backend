<?php

namespace App\Http\Controllers\Transaction;

use App\Daycare;
use App\Http\Controllers\Controller;
use App\Services\Stripe\Seller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Stripe\Account;
use Stripe\AccountLink;
use Stripe\Stripe;

class SellerController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth:daycare');
    }

    //Create Stripe seller express account
    public function create()
    {
        $user = Auth::user();

        if(!is_null($user->stripe_connect_id))
        {
            //Redirect to stripe dashboard
            return redirect()->route('stripe.login');
        }
        $session = request()->session()->getId();
        $uri = config('services.stripe.connect') .$session;

        //return to seller dashboard
        return redirect($uri);
    }

    public function save(Request $request)
    {
        $this->validate($request, [
            'code' => 'required',
            'state' => 'required',
        ]);

        //Get session from database incase users are not logged in
        $session = DB::table('sessions')->where('id', '=', $request->state)->first();
        //if session doesn't exist or it's null/empty
        if (is_null($session)) {
            return redirect()->route('daycare/dashboard')->with('error', 'Can not find user');
        }

        $data = Seller::create($request->code);
        Daycare::findOrFail($session->user_id)->update(['stripe_connect_id' => $data->stripe_user_id]);
        return redirect()->back()->with('success', 'Payment account has been created');
    }

    public function login() 
    {
        $user = Auth::user();
        Stripe::setApiKey(config('services.stripe.secret'));

        $account_link = AccountLink::create([
            'account' => $user->stripe_connect_id,
            'type' => 'account_onboarding'
        ]);

        dd($account_link);

        return redirect($account_link->url);
    }
}
