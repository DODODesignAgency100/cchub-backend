<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use App\Mail\PaymentReminder;
use App\reminder;
use App\Spot;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ReminderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:daycare');
    }

    //
    public function send(Request $request, $id) 
    {
        $this->validate($request, [
            'parent_name' => 'required',
            'parent_email' => 'required',
            'message' => 'required',
        ]);

        $reminder = Spot::where('id', '=', e($id))->first();
        $user = Auth::user();    
        
        $remind = new reminder();
        $remind->message = $request->message;
        $remind->daycare_id = Auth::user()->id;
        $remind->user_id = $request->user_id;
        $remind->parent_name = $request->parent_name;
        $remind->parent_email = $request->parent_email;
        $remind->price = $request->price;
        $remind->save();

        if ($reminder) {

            Mail::to($request->parent_email)->queue(new PaymentReminder($reminder, $user, $remind));

            return redirect()->route('daycare/reminder')->with('message', sprintf('You succesfully sent a reminder to %s', $request->parent_email));
        }

        return redirect()->route('daycare/reminder')->with('error', 'Can not find user');
        

    }
}
