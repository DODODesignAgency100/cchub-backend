<?php

namespace App\Http\Controllers\Daycare;

use App\Daycare;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    //
    protected $redirectTo = 'daycare/home';

    public function __construct()
    {
        $this->middleware('guest:daycare')->except('logout');
    }

    public function showRegistrationForm() {
        return view('daycare.register');
    }

    public function create(Request $request) {
        //Validate Form Data

        // if ($request->has('businessAddressIsPersonalAdrress')) {
        //     $data['b_city'] = $request->city;
        //     $data['b_province'] = $request->province;
        //     $data['b_zip'] = $request->zip;
        // }

        $data = $request->validate([
            'email' => ['required','email','unique:daycares'],
            'first_name' => 'required',
            'last_name' => 'required',
            'password' => ['required','min:8','confirmed'],
            'address' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'province' => 'required',
            'phone_number' => 'required',
            'terms' => 'required',
            'business_name' => 'required',
            'bank' => 'required',
            'account_number' => ['required','unique:daycares'],
            'service_type' => 'required',
            // 'payment_type' => 'required',
            // 'businessAddressIsPersonalAdrress' => 'required',
            'business_address' => 'required',
            // 'b_state' => 'required',
            'b_city' => 'required',
            'b_province' => 'required',
            'b_zip' => 'required',
            'b_phone' => 'required',
            'b_email' => 'required',
            'license_number' => 'required',
        ]);

        try {
            $api_key = 'AIzaSyAAfE-uI-vq_hDh1RPXAru17R5dYSxNDVs';
            $address = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$request->business_address.'&key='.$api_key;

            $client = new Client();

            $address_response = $client->request("GET", $address);

            $statusCode = $address_response->getStatusCode();
            $add_response = $address_response->getBody();
            $postCode = json_decode($add_response);
            $location = $postCode->results;

            for($i=0; $i < count($location); $i++){
                $formattedAddress = $location[$i]->formatted_address;
                $place_id = $location[$i]->place_id;
                $latitude = $location[$i]->geometry->location->lat;
                $longitude = $location[$i]->geometry->location->lng;

                for($j=0;$j < count($location[$i]->address_components); $j++){
                    for($k=0; $k < count($location[$i]->address_components[$j]->types); $k++){
                        if($location[$i]->address_components[$j]->types[$k] == "postal_code"){
                            $zipcode = $location[$i]->address_components[$j]->short_name;
                        }
                        if($location[$i]->address_components[$j]->types[$k] == "country"){
                            $code = $location[$i]->address_components[$j]->short_name;
                        }
                        if($location[$i]->address_components[$j]->types[$k] == "administrative_area_level_1"){
                            $province = $location[$i]->address_components[$j]->long_name;
                        }
                        if($location[$i]->address_components[$j]->types[$k] == "administrative_area_level_2"){
                            $city = $location[$i]->address_components[$j]->long_name;
                        }
                    }
                }
            
            }

            // if ($statusCode == 200 && $request->zip != $zipcode) {
            //     return back()->with('message', 'Your zip code does not match our records');
            // }

            // if ($statusCode == 200 && $province != $request->province) {
            //     return back()->with('message', 'Your Province does not match our records');
            // }

            // if ($statusCode == 200 && $zipcode != $request->b_zip) {
            //     return back()->with('message', 'Your zip code does not match our records');
            // }

            // if ($statusCode == 200 && $zipcode != $request->b_province) {
            //     return back()->with('message', 'Your zip code does not match our records');
            // }

            if ($statusCode == 200 && $code != 'CA') {
                return back()->with('error', 'This service is not yet available in your country');
            }

            if ($statusCode == 200 && $code == 'CA' ) {

                $daycare =  Daycare::create([
                    'email' => $data['email'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'password' => Hash::make($data['password']),
                    'city' => $data['city'],
                    'zip' => $data['zip'],
                    'address' => $data['address'],
                    'province' => $data['province'],
                    'phone_number' => $data['phone_number'],
                    'terms' => $data['terms'],
                    'business_name' => $data['business_name'],
                    'bank' => $data['bank'],
                    'account_number' => $data['account_number'],
                    'service_type' => $data['service_type'],
                    // 'payment_type' => $data['payment_type'],
                    // 'businessAddressIsPersonalAdrress' => 'required',
                    'business_address' => $data['business_address'],
                    'b_city' => $city,
                    'b_province' => $province,
                    'b_zip' => $zipcode,
                    'b_phone' => $data['b_phone'],
                    'b_email' => $data['b_email'],
                    'license_number' => $data['license_number'],
                    'business_address' => $formattedAddress,
                    'place_id' => $place_id,
                    'longitude' => $longitude,
                    'latitude' => $latitude,
                    'slug' => Str::slug($data['business_name'], '-'),
                ]);

                Auth::guard('daycare')->loginUsingId($daycare->id);

                return redirect()->route('daycare/dashboard');

            } else {
                return back()->with('error', 'Your zip code does not match your location');
            }
        } catch(\Exception $e) {
            return back()->with('message', $e->getMessage());
        }

        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        $data['verified'] = Daycare::UNVERIFIED_USER;



        // $daycare = Daycare::Create($data);
        // Auth::guard('daycare')->loginUsingId($daycare->id);

        $daycare = Daycare::Create($data);
        Auth::guard('daycare')->loginUsingId($daycare->id);

        return redirect()->route('daycare/dashboard');

        //Attempt to login as daycare
        // try {
            
        //     $daycare = Daycare::Create($data);
        //     Auth::guard('daycare')->loginUsingId($daycare->id);

        //     return redirect()->route('daycare/home');

        // } catch (\Exception $e) {
        //     Session::flash()->$e->message;
        //     return redirect()->back()->withInput($request->only($data));
        // }
    }
}