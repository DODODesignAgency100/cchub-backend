<?php

namespace App\Http\Controllers\Daycare;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = 'daycare/home';

    protected function guard()
    {
        return Auth::guard('daycare');
    }

    protected function broker() {
        return Password::broker('daycares');
    }

    public function showResetForm(\Illuminate\Http\Request $request, $token = null)
    {
        return view('daycare.passwords.reset')->with([
            'token' => $token, 'email' => $request->email
        ]);
    }
}
