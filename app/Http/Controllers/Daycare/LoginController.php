<?php

namespace App\Http\Controllers\Daycare;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    protected $redirectTo = 'daycare/home';
    
    public function __construct()
    {
        $this->middleware('guest:daycare')->except('logout');
    }

    public function showLoginForm() {
        return view('daycare.login');
    }

    public function login(Request $request) {
        //Validate Form Data
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ]);

        //Attempt to login as daycare
        if (Auth::guard('daycare')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
            //If successful redirect to daycare dashboard
            return redirect()->intended(route('daycare/dashboard'));
        }

        //Else if unsuccessful redirect to login page
        return redirect()->back()->with($request->only('email', 'remember'));
    }

    public function logout() {
        Auth::guard('daycare')->logout();
        return redirect('provider/home'); 
    }
}
