<?php

namespace App\Http\Controllers\Daycare;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Auth\AuthorizationException;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = 'daycare/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:daycare');
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function show(Request $request)
    {
        return $request->user()->hasVerifiedEmail()
                        ? redirect($this->redirectPath())
                        : view('daycare.verify');
    }

    // /**
    //  * Mark the authenticated user's email address as verified.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  *
    //  * @throws \Illuminate\Auth\Access\AuthorizationException
    //  */
    // public function verify(Request $request)
    // {
    //     auth('daycare')->loginUsingId($request->route('id'));
        
    //     if (! hash_equals((string) $request->route('id'), (string) $request->user()->getKey())) {
    //         throw new AuthorizationException;
    //     }

    //     if (! hash_equals((string) $request->route('hash'), sha1($request->user()->getEmailForVerification()))) {
    //         throw new AuthorizationException;
    //     }

    //     if ($request->user()->hasVerifiedEmail()) {
    //         return $request->wantsJson()
    //                     ? new Response('', 204)
    //                     : redirect($this->redirectPath());
    //     }

    //     if ($request->user()->markEmailAsVerified()) {
    //         event(new Verified($request->user()));
    //     }

    //     if ($response = $this->verified($request)) {
    //         return $response;
    //     }

    //     return $request->wantsJson()
    //                 ? new Response('', 204)
    //                 : redirect($this->redirectPath())->with('verified', true);
    // }

    // /**
    //  * The user has been verified.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return mixed
    //  */
    // protected function verified(Request $request)
    // {
    //     //
    // }

    // /**
    //  * Resend the email verification notification.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    // public function resend(Request $request)
    // {
    //     if ($request->user()->hasVerifiedEmail()) {
    //         return $request->wantsJson()
    //                     ? new Response('', 204)
    //                     : redirect($this->redirectPath());
    //     }

    //     $request->user()->sendEmailVerificationNotification();

    //     return $request->wantsJson()
    //                 ? new Response('', 202)
    //                 : back()->with('resent', true);
    // }
}
