<?php

namespace Illuminate\Auth\Middleware;

use Closure;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class EnsureEmailIsVerified
{

    public function handle($request, Closure $next, $guard = null ,$redirectToRoute = null)
    {

        $guards = array_keys(config('auth.guards'));

        foreach($guards as $guard) {

            if ($guard == 'daycare') {
                if (! Auth::guard($guard)->user() ||
                    (Auth::guard($guard)->user() instanceof MustVerifyEmail &&
                    ! Auth::guard($guard)->user()->hasVerifiedEmail())) {
                    return $request->expectsJson()
                            ? abort(403, 'Your email address is not verified.')
                            : Redirect::route($redirectToRoute ?: 'daycare.verification.notice');
                }
            }

            elseif ($guard == 'admin') {
                if (! Auth::guard($guard)->user() ||
                    (Auth::guard($guard)->user() instanceof MustVerifyEmail &&
                    ! Auth::guard($guard)->user()->hasVerifiedEmail())) {
                    return $request->expectsJson()
                            ? abort(403, 'Your email address is not verified.')
                            : Redirect::route($redirectToRoute ?: 'verification.notice');
                }
            }

            elseif ($guard == 'web') {
                if (! Auth::guard($guard)->user() ||
                    (Auth::guard($guard)->user() instanceof MustVerifyEmail &&
                    ! Auth::guard($guard)->user()->hasVerifiedEmail())) {
                    return $request->expectsJson()
                            ? abort(403, 'Your email address is not verified.')
                            : Redirect::route($redirectToRoute ?: 'verification.notice');
                }
            }

        }

        return $next($request);
    }
}
