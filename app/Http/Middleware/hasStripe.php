<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class hasStripe
{
    
    public function handle($request, Closure $next, $guard = null)
    {
        $user = Auth::user();

        switch ($guard) {
            case 'daycare':
                if (Auth::guard($guard)->check()) {
                    if (is_null($user->stripe_connect_id)) {
                        return redirect()->route('create.express');
                    } 
                }
                break;
            default:
                if (Auth::guard($guard)->check() && is_null($user->stripe_customer_id)) {
                    return redirect()->route('card.form');
                }
        }

        return $next($request);
    }
}
