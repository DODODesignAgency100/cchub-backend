<?php

namespace App\Http\Middleware;

use App\Daycare;
use Closure;
use Illuminate\Support\Facades\Auth;

class isSubscribed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {

        $user = Auth::user();

        if ($user && ! $user->subscribed('Premium')) {
            return redirect()->route('daycare/subscribe');
        }

        return $next($request);
    }
}
