<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class hasStripeConnect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'daycare')
    {
        $user = Auth::user();
        
        if (Auth::guard($guard)->check()) {
            if (is_null($user->stripe_connect_id)) {
                return redirect()->route('create.express');
            } 
        }
        return $next($request);
    }
}
