<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class reminder extends Model
{
    //
    public function user() 
    {
        return $this->belongsTo(User::class);
    }

    public function daycare() 
    {
        return $this->belongsTo(Daycare::class);
    }
}
