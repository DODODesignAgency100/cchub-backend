<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\DaycarePasswordResetNotification;
use App\Notifications\MailEmailVerificationNotification;
use Illuminate\Auth\Notifications\VerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Cashier\Billable;

class Daycare extends Authenticatable
{
    use Notifiable, SoftDeletes, Billable;

    protected $guard = 'daycare';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const VERIFIED_USER = '1';
    const UNVERIFIED_USER = '0';
    const SUBSCRIBED = 'true';
    const UNSUBSCRIBED = 'false';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email', 
        'password',
        'verified',
        'verification_token',
        'admin',
        'address',
        'address1',
        'city','province','zip',
        'phone_number',
        'businessAddressIsPersonalAdrress',
        'business_name',
        'bank',
        'account_number',
        'service_type',
        'payment_type',
        'business_address','b_city','b_province','b_state','b_zip','b_phone','b_email',
        'password',
        'license_number',
        'testimonial',
        'terms',
        'provider',
        'place_id',
        'longitude',
        'latitude',
        'show_address',
        'director_image',
        'daycare_photo',
        'slug',
        'stripe_connect_id',
        'featured',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'verification_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function isVerified() {
        return $this->verified == Daycare::VERIFIED_USER;
    }

    public function isSubscribed() 
    {
        return $this->subscribed == Daycare::SUBSCRIBED;
    }

    public static function generateVerificationCode() {
        $timeNow = Carbon::now();
        return sha1($timeNow);
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new DaycarePasswordResetNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new MailEmailVerificationNotification());
    }

    public function rate() {
        return $this->hasOne('App\Rates','daycare_id');
    }

    public function timeslot() {
        return $this->hasOne('App\Timeslot','daycare_id');
    }

    public function review() {
       return $this->hasMany('App\Review');
    }

    public function payments() 
    {
        return $this->hasMany('App\Payment', 'daycare_id');
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
    
    public function infantHours() {
        // dd($this->id);
        $i_hour = Rates::where("daycare_id", $this->id)->first();
        return $i_hour;
    }

    public function languages() {
        $daycare_language = Services::where("daycare_id", $this->id)->first();
        return $daycare_language;
    }

    public function timeslots() {
        $daycare_time = Timeslot::where("daycare_id", $this->id)->first();
        return $daycare_time;
    }

    public function daycareImage() {
        $daycare_image = DaycarePhotos::where("daycare_id", $this->id)->first();
        return $daycare_image;
    }

    public function description() {
        $description = DaycareDescription::where("daycare_id", $this->id)->first();
        return $description;
    }

    public function service() 
    {
        $service = Services::where("daycare_id", $this->id)->first();
        return $service;
    }

    public function spot() 
    {
        $count = Spot::where("daycare_id", $this->id)->count();
        return $count;
    }

    public function spots() 
    {
        $spot = Spot::where("daycare_id", $this->id)->get();
        return $spot;
    }

    public function messages() 
    {
        return $this->hasMany(Message::class);
    }

    public function reminder() 
    {
        return $this->hasMany(reminder::class);
    }
    
    public function ratings() 
    {
        $rating = Review::where("daycare_id", $this->id)->avg('rate');
        return $rating;
    }

    public function totalRatings()
    {
        $totalRatings = Review::where("daycare_id", $this->id)->whereNotNull('rate')->count();
        return $totalRatings;
    }
}
