<?php

namespace App\Mail;

use App\Tour;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class TourRejected extends Mailable
{
    use Queueable, SerializesModels;

    protected $tour;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Tour $tour)
    {
        $this->tour = $tour;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.tour.rejected')
                    ->with([
                        'userName' => $this->tour->user->first_name,
                        'daycareName' => $this->tour->daycare->business_name,
                    ]);
    }
}
