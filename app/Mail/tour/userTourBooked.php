<?php

namespace App\Mail\tour;

use App\Daycare;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class userTourBooked extends Mailable
{
    use Queueable, SerializesModels;

    protected $daycare;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Daycare $daycare)
    {
        $this->daycare = $daycare;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.tour.userTourBooked')
                    ->with([
                        'daycareName' => $this->daycare->business_name,
                    ]);
    }
}
