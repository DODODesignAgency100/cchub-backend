<?php

namespace App\Mail;

use App\Spot;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SpotRejected extends Mailable
{
    use Queueable, SerializesModels;

    protected $reservation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Spot $reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.spot.rejected')
                    ->with([
                        'userName' => $this->reservation->user->first_name,
                        'daycareName' => $this->reservation->daycare->business_name,
                    ]);
    }
}
