<?php

namespace App\Mail\spot;

use App\Daycare;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class userSpotBooked extends Mailable
{
    use Queueable, SerializesModels;

    protected $daycare;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Daycare $daycare)
    {
        $this->daycare = $daycare;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.spot.userBooked')
                    ->with([
                        'daycareName' => $this->daycare->business_name,
                    ]);
    }
}
