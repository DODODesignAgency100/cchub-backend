<?php

namespace App\Mail;

use App\Daycare;
use App\reminder;
use App\Spot;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PaymentReminder extends Mailable
{
    use Queueable, SerializesModels;

    protected $reminder;
    protected $user;
    protected $remind;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Spot $reminder, Daycare $user, reminder $remind)
    {
        $this->reminder = $reminder;
        $this->user = $user;
        $this->remind = $remind;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.payment.reminder')
                    ->with([
                        'userName' => $this->remind->parent_name,
                        'daycareId' => $this->remind->daycare_id,
                        'daycareName' => $this->reminder->daycare->business_name,
                        'amountDue' =>  $this->remind->price,
                        'message' => $this->remind->message,
                        'remindId' => $this->remind->id,
                    ]);
    }
}
