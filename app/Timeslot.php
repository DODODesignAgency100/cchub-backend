<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Timeslot extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    public function daycare() {
        return $this->belongsTo('App\Daycare','daycare_id');
    }
}
