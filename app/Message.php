<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Message extends Model
{
    //
    protected $dates = [
        'created_at',
        'deleted_at',
        'updated_at',
    ];

    protected $fillable = [
        'user_id',
        'receiver_id',
        'message',
        'is_read'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
