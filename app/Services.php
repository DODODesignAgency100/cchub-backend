<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Services extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //
    protected $table = 'services';
    
    public function daycare() {
        return $this->belongsTo('App\Daycare');
    }
}
