<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaycareDescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daycare_descriptions', function (Blueprint $table) {
            $table->id();
            $table->longText('overview');
            $table->longText('director');
            $table->longText('tour_policy');
            $table->time('dailyScheduleTime1')->nullable();
            $table->string('dailyScheduleActivity1')->nullable();
            $table->time('dailyScheduleTime2')->nullable();
            $table->string('dailyScheduleActivity2')->nullable();
            $table->time('dailyScheduleTime3')->nullable();
            $table->string('dailyScheduleActivity3')->nullable();
            $table->binary('images')->nullable();
            $table->string('provide_tour')->nullable();
            $table->integer('daycare_id')->unsigned()->index();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('daycare_id')->references('id')->on('daycares')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daycare_descriptions');
    }
}
