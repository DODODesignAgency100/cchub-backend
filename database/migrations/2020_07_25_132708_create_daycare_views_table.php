<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaycareViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daycare_views', function (Blueprint $table) {
            $table->id();
            $table->integer('daycare_id')->unsigned()->index();
            $table->string('business_name');
            $table->string('session_id');
            $table->integer('user_id')->nullable();
            $table->string('ip');
            $table->string('agent');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('daycare_id')->references('id')->on('daycares')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daycare_views');
    }
}
