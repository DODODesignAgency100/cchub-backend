<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeslotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timeslots', function (Blueprint $table) {
            $table->id();
            $table->string('rOpenhour');
            $table->string('rClosehour');
            $table->string('wknOpenHour');
            $table->string('wknCloseHour');
            $table->string('wkcOpenHour');
            $table->string('wkcCloseHour');
            $table->string('ovrOpenHour');
            $table->string('ovrCloseHour');
            $table->string('sociallinkfacebook');
            $table->string('sociallinktwitter')->nullable();
            $table->string('sociallinkinstagram')->nullable();
            $table->string('website');
            $table->string('show_address')->default('no')->nullable();
            $table->integer('daycare_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('daycare_id')->references('id')->on('daycares');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timeslots');
    }
}
