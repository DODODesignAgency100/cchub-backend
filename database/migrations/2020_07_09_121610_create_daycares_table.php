<?php

use App\Daycare;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaycaresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daycares', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->string('verified')->default(Daycare::UNVERIFIED_USER);
            $table->string('verification_token')->nullable();
            $table->string('address');
            $table->string('address1')->nullable();
            $table->string('show_address')->nullable();
            $table->string('city');
            $table->string('zip');
            $table->string('province');
            $table->string('phone_number')->unique();
            $table->string('business_name')->unique();
            $table->string('bank');
            $table->string('account_number')->unique();
            $table->string('service_type');
            $table->string('stripe_connect_id')->nullable();
            $table->string('business_address');
            $table->string('b_city');
            // $table->string('b_state');
            $table->string('b_province');
            $table->string('b_zip');
            $table->string('b_phone');
            $table->string('b_email');
            $table->string('license_number');
            $table->string('testimonial')->nullable();
            $table->string('terms');
            $table->string('place_id');
            $table->double('longitude');
            $table->double('latitude');
            $table->string('slug')->unique();
            $table->boolean('featured')->default(0);
            $table->string('subscribed')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daycares');
    }
}
