<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('infant_age')->nullable();
            $table->double('infant_hours', 0, 2)->nullable();
            $table->double('infant_daily_hours', 0, 2)->nullable();
            $table->double('infant_monthly_hours', 0, 2)->nullable();
            $table->double('infant_weekly_hours', 0, 2)->nullable();
            $table->double('infant_weekend_overnights', 0, 2)->nullable();
            $table->double('infant_week_nights', 0, 2)->nullable();
            $table->string('infant_no_offers')->nullable();
            $table->string('show_rates')->nullable();
            $table->string('preschool_age')->nullable();
            $table->double('preschool_hours', 0, 2)->nullable();
            $table->double('preschool_daily_hours', 0, 2)->nullable();
            $table->double('preschool_monthly_hours', 0, 2)->nullable();
            $table->double('preschool_weekly_hours', 0, 2)->nullable();
            $table->double('preschool_weekend_overnights', 0, 2)->nullable();
            $table->double('preschool_week_nights', 0, 2)->nullable();
            $table->string('preschool_no_offers')->nullable();
            $table->string('school_age')->nullable();
            $table->double('school_hours', 0, 2)->nullable();
            $table->double('school_daily_hours', 0, 2)->nullable();
            $table->double('school_monthly_hours', 0, 2)->nullable();
            $table->double('school_weekly_hours', 0, 2)->nullable();
            $table->double('school_weekend_overnights', 0, 2)->nullable();
            $table->double('school_week_nights', 0, 2)->nullable();
            $table->string('school_no_offers')->nullable();
            $table->integer('daycare_id')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('daycare_id')->references('id')->on('daycares')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rates');
    }
}
