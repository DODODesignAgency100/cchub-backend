<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaycarePhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daycare_photos', function (Blueprint $table) {
            $table->id();
            $table->binary('director_image')->nullable();
            $table->binary('daycare_photos')->nullable();
            $table->integer('daycare_id')->unsigned()->index();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('daycare_id')->references('id')->on('daycares')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daycare_photos');
    }
}
