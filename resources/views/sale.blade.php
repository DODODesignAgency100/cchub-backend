<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title> {{ config('app.name', '| Child Care Hub') }} </title>

        <meta name="description" content="Find the right care for your child">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
    </head>

    <body>
        
        <nav class="nav-bar" aria-labelledby="header menu">

            <div class="main-nav">
                <div class="logo">
                    @if(Auth::guard('daycare'))
                        <a href="{{ url('home') }}">
                            <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                        </a>
                    @elseif (Auth::guard('user')) 
    
                        <a href="{{ url('home') }}">
                            <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                        </a>
                    @else 
                        <a href="{{ url('parent-home') }}">
                            <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                        </a>
                    @endif
                    
                </div>

                <div class="menu">
                    <ul class="menu-list-light">

                        <li>
                            @if (!Auth::guard('daycare'))
                                <a href="#">About Us</a>
                            @else
                                <a href="{{ url('daycare/about') }}">About Us</a>
                            @endif
                            
                        </li>

                        <li>
                            <a href="#">Activities</a>
                        </li>

                        <li>
                            @if (!Auth::guard('daycare'))
                                <a href="#">Resources</a>
                            @else
                                <a href="{{ url('daycare/resources') }}">Resources</a>
                            @endif
                        </li>

                        <li>
                            <a href="#">Community</a>
                        </li>

                        <li>
                            <a href="#">Daycare Providers</a>
                        </li>
                    </ul>
                </div>


                <div class="hambuger">
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
            </div>

            <div id="menu" class="mobile-menu d-none">

                <div class="close">
                    <span class="bar-close"></span>
                    <span class="bar-close"></span>
                </div>

                <ul class="menu-mobile">

                    <li>
                        <a href="#">About Us</a>
                    </li>
                    <li>
                        <a href="#">Activities</a>
                    </li>
                    <li>
                        <a href="#">Resources</a>
                    </li>
                    <li>
                        <a href="#">Community</a>
                    </li>
                    <li>
                        <a href="#">Daycare Providers</a>
                    </li>
                    
                </ul>
            </div>
        </nav>

        <main>
        
            <section class="hero-pr">
                <img src="{{ asset ('assets/img/DNS.jpg') }}" alt="Do not Sell Breadcrum">
            </section>

            <section class="privacy-main">

                <div class="nav-breadcrum">
                    <div>
                        <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li>Do not Sell my Information</li>
                        </ul>
                    </div>
                </div>

                <div class="privacy-main--hero">
                    <img src="{{ asset('assets/img/donotsell-bread.jpg') }}" alt="Do not sell Hero Image">
                </div>

                <div class="privacy-content">
                    <div class="privacy-content--nav">
                        <nav class="privacy-content_nav">
                            <ul>
                                <li><a href="privacy.html">Privacy Policy</a></li>
                                <li><a href="{{ route('terms') }}">Terms of Use</a></li>
                                <li><a href="{{ route('do-not-sell') }}" class="p-active">Do not Sell my Information</a></li>
                            </ul>
                        </nav>

                        <div class="privacy-content--main">

                            <div class="privacy-content--inner">
                                <h3>Do Not Sell My Personal Information</h3>
                                <p>At CCH, we honor the importance of personal information. That’s why:</p>
                
                                <ul>
                                <li>We’re not selling any personal information.</li>
                                <li>We don’t have plans to sell personal information.</li>
                                </ul>

                                <p class="gap-top">For more information, read our <a href="privacy.html">Privacy Policy</a> or <a href="terms-of-use.html">Term of use</a> about personal data. We respect and understand that you may wish to ensure your personal information isn’t sold in the future. If so, please update your status below.</p>
                            </div>

                        </div>
                    </div>                
                </div>
            </section>
        
        </main>

    </body>