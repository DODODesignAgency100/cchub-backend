@if (count($errors) > 0)
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            <p>{{ $error }}</p>
        </div>
    @endforeach
@endif

@if (session()->has('message'))
    <div class="alert alert-success">
        <p>{{session('message')}}</p>
    </div>
@endif

@if (session()->has('error'))
    <div class="alert alert-danger">
        <p>{{ session('error') }}</p>
    </div>
@endif

@if (session()->has('status'))
    <div>{{session('status')}}</div>
@endif