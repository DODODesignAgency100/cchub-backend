@extends('layouts.parents.app')

@section('content')
<section class="hero-pr">
        <img src="{{ asset('assets/img/providers-resources banner.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="pro-resource">

        <h2>Providers</h2>

        <div class="pro-resources">

            <nav class="pro-resources_nav">
                <ul>
                    <li>
                        <a href="{{ route('daycare/resources') }}" class="p-active">Getting Started</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/get-started') }}">Managing Your Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/trust-and-safety') }}">Trust and Safety</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/referrals') }}">Getting Referrals</a>
                    </li>
                </ul>
            </nav>

            <div class="pro-resources_main">

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/open-spaces.jpg')}}" alt="Open Spaces">
                    <figcaption>List Your Open Spaces</figcaption>
                </figure>

                <figure class="pro-res">
                <img src="{{ asset('assets/img/more-referrals.jpg') }}" alt="More Referrals">
                <figcaption>Get More Referrals</figcaption>
                </figure>

                <figure class="pro-res">
                <img src="{{ asset('assets/img/preschool.jpg')}}" alt="Preschool">
                <figcaption>Can I Advertise My Daycare or Preschool on Childcare Hub?</figcaption>
                </figure>

                <figure class="pro-res">
                <img src="{{ asset('assets/img/re-pro.jpg')}}" alt="Program">
                <figcaption>How Do I Get More Reviews of My Program?</figcaption>
                </figure>

                <figure class="pro-res">
                <img src="{{ asset('assets/img/enrol.jpg')}}" alt="Enrol">
                <figcaption>Mark Your Program as Fully Enrolled</figcaption>
                </figure>

                <figure class="pro-res">
                <img src="{{ asset('assets/img/from-chub.jpg')}}" alt="Childcare">
                <figcaption>Stop Getting Referrals From Childcare Hub</figcaption>
                </figure>

            </div>
        </div>

    </section>
@endsection