<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Admin') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('admin/asset/css/admin.css') }}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('asset/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('assets/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    @yield('head')
</head>

<body>

    <header class="admin">

        <nav class="admin-nav">
            <div class="logo">
                <a href="/">
                    <img src="{{ asset('admin/asset/img/cchublogo.png') }}" alt="Childcare Hub logo">
                </a>
            </div>
    
            <div class="profile">
                <div class="profile-setting">
                    <svg width="23" height="24" viewBox="0 0 23 24" fill="#333333" xmlns="http://www.w3.org/2000/svg">
                        <path d="M18.6563 12.98C18.6938 12.66 18.7219 12.34 18.7219 12C18.7219 11.66 18.6938 11.34 18.6563 11.02L20.6352 9.37C20.8134 9.22 20.8603 8.95 20.7477 8.73L18.872 5.27C18.7876 5.11 18.6281 5.02 18.4593 5.02C18.403 5.02 18.3468 5.03 18.2999 5.05L15.9646 6.05C15.4769 5.65 14.9517 5.32 14.3796 5.07L14.0232 2.42C13.995 2.18 13.7981 2 13.5636 2H9.81212C9.57765 2 9.3807 2.18 9.35256 2.42L8.99617 5.07C8.42407 5.32 7.89886 5.66 7.41116 6.05L5.07586 5.05C5.01959 5.03 4.96332 5.02 4.90704 5.02C4.7476 5.02 4.58817 5.11 4.50376 5.27L2.62801 8.73C2.50609 8.95 2.56236 9.22 2.74056 9.37L4.71947 11.02C4.68195 11.34 4.65382 11.67 4.65382 12C4.65382 12.33 4.68195 12.66 4.71947 12.98L2.74056 14.63C2.56236 14.78 2.51547 15.05 2.62801 15.27L4.50376 18.73C4.58817 18.89 4.74761 18.98 4.91642 18.98C4.97269 18.98 5.02897 18.97 5.07586 18.95L7.41116 17.95C7.89886 18.35 8.42407 18.68 8.99617 18.93L9.35256 21.58C9.3807 21.82 9.57765 22 9.81212 22H13.5636C13.7981 22 13.995 21.82 14.0232 21.58L14.3796 18.93C14.9517 18.68 15.4769 18.34 15.9646 17.95L18.2999 18.95C18.3561 18.97 18.4124 18.98 18.4687 18.98C18.6281 18.98 18.7876 18.89 18.872 18.73L20.7477 15.27C20.8603 15.05 20.8134 14.78 20.6352 14.63L18.6563 12.98ZM16.7993 11.27C16.8368 11.58 16.8462 11.79 16.8462 12C16.8462 12.21 16.8274 12.43 16.7993 12.73L16.668 13.86L17.5027 14.56L18.5156 15.4L17.8591 16.61L16.668 16.1L15.6926 15.68L14.8485 16.36C14.4452 16.68 14.0607 16.92 13.6762 17.09L12.682 17.52L12.5319 18.65L12.3444 20H11.0314L10.8532 18.65L10.7031 17.52L9.70895 17.09C9.30567 16.91 8.93052 16.68 8.55537 16.38L7.7019 15.68L6.70776 16.11L5.51666 16.62L4.86015 15.41L5.87305 14.57L6.70776 13.87L6.57646 12.74C6.54832 12.43 6.52956 12.2 6.52956 12C6.52956 11.8 6.54832 11.57 6.57646 11.27L6.70776 10.14L5.87305 9.44L4.86015 8.6L5.51666 7.39L6.70776 7.9L7.68315 8.32L8.52723 7.64C8.93052 7.32 9.31504 7.08 9.69957 6.91L10.6937 6.48L10.8438 5.35L11.0314 4H12.335L12.5132 5.35L12.6632 6.48L13.6574 6.91C14.0607 7.09 14.4358 7.32 14.811 7.62L15.6644 8.32L16.6586 7.89L17.8497 7.38L18.5062 8.59L17.5027 9.44L16.668 10.14L16.7993 11.27ZM11.6879 8C9.61516 8 7.93637 9.79 7.93637 12C7.93637 14.21 9.61516 16 11.6879 16C13.7606 16 15.4394 14.21 15.4394 12C15.4394 9.79 13.7606 8 11.6879 8ZM11.6879 14C10.6562 14 9.81212 13.1 9.81212 12C9.81212 10.9 10.6562 10 11.6879 10C12.7195 10 13.5636 10.9 13.5636 12C13.5636 13.1 12.7195 14 11.6879 14Z" />
                    </svg>            
                </div>
        
                <div class="profile-user">
                    <p>
                        {{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
                        <span>
                            @if ( Auth::user()->image == '' )
                                <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                            @else
                                <img src="/admin/avatar/{{ Auth::user()->image }}" alt="avatar">
                            @endif
                        </span>
                    </p>
                </div>
        
            </div>
        </nav>
      
    </header>