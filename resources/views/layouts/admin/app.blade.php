<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('layouts.admin.header')

    @include('layouts.admin.sidebar')

    <main class="admin-main">
         @yield('content')
    </main>

    @include('layouts.admin.footer')

</body>
</html>