<footer class="ft">

        <div class="ft-main">

            <nav class="ft-main_col" aria-labelledby="ft menu">

                <h5>Childcare hub</h5>

                <ul class="col-list">
                    <li>
                        <a href="#">Careers</a>
                    </li>
                    {{-- <li>
                        <a href="#">Do not sell my information</a>
                    </li> --}}
                    <li>
                        <a href="#">+1 (306) 850-9384</a>
                    </li>
                    <li>
                        <a href="#" id="email">hello@childcarehub.com</a>
                    </li>
                </ul>

            </nav>
    
            <nav class="ft-main_col" aria-labelledby="ft menu">

                <h5>PARENTS</h5>

                <ul class="col-list">
                    {{-- <li>
                        <a href="#">Daycares Near Me</a>
                    </li> --}}
                    <li>
                        <a href="{{ route('resources') }}">Parents Resource</a>
                    </li>
                    {{-- <li>
                        <a href="#">Places to visit</a>
                    </li> --}}
                </ul>

            </nav>
    
            <nav class="ft-main_col" aria-labelledby="ft menu">

                <h5>PROVIDERS</h5>

                <ul class="col-list">
                    <li>
                        <a href="{{ route('daycare/referrals') }}">Get referrals</a>
                    </li>
                    {{-- <li>
                        <a href="#">get reviews</a>
                    </li> --}}
                    <li>
                        <a href="{{ route('daycare/resources') }}">Providers resource</a>
                    </li>
                </ul>

            </nav>
    
            <nav class="ft-main_col" aria-labelledby="ft menu">

                <h5>LOCAL</h5>

                <ul class="col-list">
                    <li>
                        <a href="#">Saskatchewan</a>
                    </li>
                    <li>
                        <a href="#">Toronto</a>
                    </li>
                    <li>
                        <a href="#">Montreal</a>
                    </li>
                    <li>
                        <a href="#">Ottawa</a>
                    </li>
                </ul>

            </nav>
    
            <nav class="ft-main_col" aria-labelledby="ft menu">

                <h5>SUPPORT</h5>

                <ul class="col-list">
                    <li>
                        <a href="{{ route('help') }}">Help Center</a>
                    </li>
                </ul>

            </nav>
        </div>
    
        <div class="ft-copyright">
            <div class="ft-copyright-text">
                <p>
                    <small>&copy; {{ date('Y') }} Child Care Hub</small>
                </p>
                <p>
                    <small>All rights reserved</small>
                </p>
                <p>
                    <small><a href="{{ route('privacy') }}"> Privacy Policy </a></small>
                </p>
                <p>
                    <small><a href="{{ route('terms') }}"> Terms of Use </a></small>
                </p>
                <p>
                    <a href="#">
                        <img src="{{ asset('assets/img/Facebook.svg') }}" alt="Facebook">
                    </a>
                    <a href="#">
                        <img src="{{ asset('assets/img/Twitter.svg')}}" alt="Twitter">
                    </a>
                    <a href="#">
                        <img src="{{ asset('assets/img/Instagram.svg')}}" alt="Instagram">
                    </a>
                </p>
            </div>
        </div>

    </footer>

    <script src="{{ asset('assets/js/index.js') }}"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    @section('footer')
        
    @show