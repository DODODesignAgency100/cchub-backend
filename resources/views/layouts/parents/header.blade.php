<head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
    
        <title>{{ config('app.name', 'Child care hub') }}</title>
    
        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">
    
        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/favicon/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/favicon/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicon/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/favicon/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/favicon/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('asset/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{ asset('assets/favicon/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('assets/favicon/ms-icon-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">
        @yield('head')
    </head>
    <body>
    
        <nav class="nav-bar" aria-labelledby="header menu">
    
            <div class="main-nav">
                <div class="logo">
                    <a href="{{ route('parent-home') }}">
                        <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                    </a>
                </div>
    
                <div class="menu">
                    <ul class="menu-list-light">
    
                        <li>
                            <a href="{{ route('about') }}">About Us</a>
                        </li>
    
                        <li>
                            <a href="{{ route('resources') }}">Resources</a>
                        </li>
    
                        <li>
                            <a href="{{ route('resources') }}">Daycare Providers</a>
                        </li>
    
                        @guest
                        <a class="menu-cta-login" href="{{ route('login') }}"> {{ __('Login') }} </a>
                        @if (Route::has('register'))
                        <a class="menu-cta-signup" href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                        @endif
                        @else
                            <li>
                                <a href="#">{{ Auth::user()->first_name }} </a>
                            </li>
                            <li class="nav-avatar">
                                <a href="#">
                                    @if ( Auth::user()->image == '' )
                                        <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                                    @else
                                        <img src="/user/avatars/{{ Auth::user()->image }}" alt="avatar">
                                    @endif
                                    
                                    {{-- <span class="reportCounter">4</span> --}}
                                    <span>
                                        <svg width="10" height="5" viewBox="0 0 10 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M0 0L5 5L10 0H0Z" fill="white"/>
                                        </svg>                                       
                                    </span>
                                </a>
                                
                                <ul class="dropdown">
                                    <li>
                                        <a href="{{ route('profile', Auth::user()->id) }}">Profile</a>
                                    </li>
                                    {{-- <li>
                                        <span class="messageCounter">3</span>
                                        <a href="#">Messages</a>
                                    </li> --}}
                                    {{-- <li>
                                        <span class="notificationCounter">1</span>
                                        <a href="#">Notification</a>
                                    </li> --}}
                                    <li>
                                        {{-- <a href="#">Logout</a> --}}
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
        
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                              
                            </li>
                        @endguest
                    </ul>
                </div>
    
    
                <div class="hambuger">
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
            </div>
    
            <div id="menu" class="mobile-menu d-none">
    
                <div class="close">
                    <span class="bar-close"></span>
                    <span class="bar-close"></span>
                </div>
    
                <ul class="menu-mobile">
    
                    <li>
                        <a href="#">About Us</a>
                    </li>
                    <li>
                        <a href="#">Resources</a>
                    </li>
                    <li>
                        <a href="#">Daycare Providers</a>
                    </li>
                    
                    @guest
                        <li class="menu-cta-login">
                            <a href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                        @if (Route::has('register'))
                            <li class="menu-cta-signup">
                                <a href="{{ route('register') }}">{{ __('Sign Up') }}</a>
                            </li>
                        @endif
                        @else
                            <li>
                                <a href="#">{{ Auth::user()->first_name }} </a>
                            </li>
                            <li class="nav-avatar">
                                <a href="#">
                                    @if ( Auth::user()->image == '' )
                                        <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                                    @else
                                        <img src="/user/avatars/{{ Auth::user()->image }}" alt="avatar">
                                    @endif
                                    {{-- <span class="reportCounter">4</span> --}}
                                    <span>
                                        <svg width="10" height="5" viewBox="0 0 10 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M0 0L5 5L10 0H0Z" fill="white"/>
                                        </svg>                                       
                                    </span>
                                </a>
                                
                                <ul class="dropdown">
                                    <li>
                                        <a href="#">Profile</a>
                                    </li>
                                    {{-- <li>
                                        <span class="messageCounter">3</span>
                                        <a href="#">Messages</a>
                                    </li> --}}
                                    {{-- <li>
                                        <span class="notificationCounter">1</span>
                                        <a href="#">Notification</a>
                                    </li> --}}
                                    <li>
                                        {{-- <a href="#">Logout</a> --}}
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>
        
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                              
                            </li>
                            {{-- <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
    
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
    
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li> --}}
                        @endguest
                </ul>
            </div>
        </nav>
