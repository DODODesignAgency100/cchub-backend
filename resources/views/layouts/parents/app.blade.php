<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('layouts.parents.header')

    <main>
         @yield('content')
    </main>

    @include('layouts.parents.footer')

</body>
</html>
