<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Child care hub') }}</title>
    @yield('title')
    @yield('description')

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/favicon/apple-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/favicon/apple-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/favicon/apple-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/favicon/apple-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/favicon/apple-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/favicon/apple-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('asset/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{ asset('assets/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{ asset('assets/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    @yield('head')
</head>

<body>
    
    <nav class="nav-bar" aria-labelledby="header menu">

        <div class="main-nav">
            <div class="logo">
                @if( !Auth::user() )
                    <a href="{{ url('provider/home') }}">
                        <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                    </a>
                    @else
                    <a href="{{ url('daycare/dashboard') }}">
                        <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                    </a>
                @endif
            </div>

            <div class="menu">
                <ul class="menu-list-light">

                    <li>
                        <a href="{{ route('daycare/about') }}">About Us</a>
                    </li>

                    <li>
                        <a href="{{ route('daycare/resources') }}">Resources</a>
                    </li>

                    <li>
                        <a href="#">Daycare Providers</a>
                    </li>

                    {{-- @guest
                    <li class="menu-cta-login">
                        <a href="{{ route('daycare/login') }}"> {{ __('Login') }} </a>
                    </li>
                    @if (Route::has('register'))
                        <li class="menu-cta-signup">
                            <a href="{{ route('daycare/register') }}">{{ __('Sign Up') }}</a>
                        </li>
                    @endif --}}
                    @if(Auth::guard('daycare')->check())
                        {{-- <li>
                            <a href="#">{{ Auth::user() }} </a>
                        </li> --}}
                        <li class="nav-avatar">
                            <a href="#">
                                {{-- <img src="{{ asset('assets/img/header-avater.png') }}" alt="avatar"> --}}
                                @if( !Auth::user() )
                                    <img src="{{ asset('assets/img/avatar.png') }}" alt="avatar">
                                    @elseif ( Auth::user()->director_image == '' )
                                        <img src="{{ asset('assets/img/avatar.png') }}" alt="avatar">
                                    @else
                                        <img src="/daycare/{{ Auth::user()->director_image }}" alt="avatar">
                                @endif
                                <span>
                                    <svg width="10" height="5" viewBox="0 0 10 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0 0L5 5L10 0H0Z" fill="white"/>
                                    </svg>                                       
                                </span>
                            </a>
                            
                            <ul class="dropdown">
                                <li>
                                    <a href="{{ route('daycare/dashboard') }}">Dashboard</a>
                                </li>
                                <li>
                                    <a href="{{ route('stripe.login') }}">Balance</a>
                                </li>
                                <li>
                                    {{-- <a href="#">Logout</a> --}}
                                    <a href="{{ route('daycare.logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form0').submit();">
                                        {{ __('Logout') }}
                                    </a>
    
                                    <form id="logout-form0" action="{{ route('daycare.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                          
                        </li>
                        @else
                            <a class="menu-cta-login" href="{{ route('daycare/login') }}"> {{ __('Login') }} </a>
                            <a class="menu-cta-signup" href="{{ route('daycare/register') }}">{{ __('Sign Up') }}</a>
                    @endif
                    {{-- @endguest --}}
                </ul>
            </div>


            <div class="hambuger">
                <span class="bar"></span>
                <span class="bar"></span>
            </div>
        </div>

        <div id="menu" class="mobile-menu d-none">

            <div class="close">
                <span class="bar-close"></span>
                <span class="bar-close"></span>
            </div>

            <ul class="menu-mobile">

                <li>
                    <a href="{{ route('daycare/about') }}">About Us</a>
                </li>
                <li>
                    <a href="{{ route('daycare/resources') }}">Resources</a>
                </li>
                <li>
                    <a href="#">Daycare Providers</a>
                </li>
                @guest
                    <li class="menu-cta-login">
                        <a href="{{ route('daycare/login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="menu-cta-signup">
                            <a href="{{ route('daycare/register') }}">{{ __('Sign Up') }}</a>
                        </li>
                    @endif
                    @else
                        <li>
                            <a href="#">{{ Auth::user()->business_name }} </a>
                        </li>
                        <li class="nav-avatar">
                            <a href="#">
                                <img src="{{ asset('assets/img/header-avater.png') }}" alt="avatar">
                                <span class="reportCounter">4</span>
                                <span>
                                    <svg width="10" height="5" viewBox="0 0 10 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0 0L5 5L10 0H0Z" fill="white"/>
                                    </svg>                                       
                                </span>
                            </a>
                            
                            <ul class="dropdown">
                                <li>
                                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                                </li>
                                <li>
                                    {{-- <a href="#">Logout</a> --}}
                                    <a href="{{ route('daycare.logout') }}"
                                        onclick="event.preventDefault();
                                        document.getElementById('logout-form1').submit();">
                                        {{ __('Logout') }}
                                    </a>
    
                                    <form id="logout-form1" action="{{ route('daycare.logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li>
                            </ul>
                          
                        </li>
                    @endguest
            </ul>
        </div>
    </nav>