<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    @include('layouts.daycare.header')

    <main>
        @yield('content')
    </main>

    @include('layouts.daycare.footer')

</body>
</html>
