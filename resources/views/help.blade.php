<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title> {{ config('app.name', '| Child Care Hub') }} </title>

        <meta name="description" content="Find the right care for your child">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
    </head>

    <body>
        
        <nav class="nav-bar" aria-labelledby="header menu">

            <div class="main-nav">
                <div class="logo">
                    
                    @if(Auth::guard('daycare'))
                        <a href="{{ url('home') }}">
                            <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                        </a>
                    @elseif (Auth::guard('user')) 
    
                        <a href="{{ url('home') }}">
                            <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                        </a>
                    @else 
                        <a href="{{ url('parent-home') }}">
                            <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                        </a>
                    @endif
                    
                </div>

                <div class="menu">
                    <ul class="menu-list-light">

                        <li>
                            @if (!Auth::guard('daycare'))
                                <a href="#">About Us</a>
                            @else
                                <a href="{{ url('daycare/about') }}">About Us</a>
                            @endif
                            
                        </li>

                        <li>
                            <a href="#">Activities</a>
                        </li>

                        <li>
                            @if (!Auth::guard('daycare'))
                                <a href="#">Resources</a>
                            @else
                                <a href="{{ url('daycare/resources') }}">Resources</a>
                            @endif
                        </li>

                        <li>
                            <a href="#">Community</a>
                        </li>

                        <li>
                            <a href="#">Daycare Providers</a>
                        </li>

                        @guest
                            <li class="menu-cta-login">
                                <a href="{{ route('daycare/login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="menu-cta-signup">
                                    <a href="{{ route('daycare/register') }}">{{ __('Sign Up') }}</a>
                                </li>
                            @endif
                            @elseif( Auth::guard('daycare') )
                                <li>
                                    <a href="#">{{ Auth::user()->business_name }} </a>
                                </li>
                                <li class="nav-avatar">
                                    <a href="#">
                                        <img src="{{ asset('assets/img/header-avater.png') }}" alt="avatar">
                                        <span class="reportCounter">4</span>
                                        <span>
                                            <svg width="10" height="5" viewBox="0 0 10 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0 0L5 5L10 0H0Z" fill="white"/>
                                            </svg>                                       
                                        </span>
                                    </a>
                                    
                                    <ul class="dropdown">
                                        <li>
                                            <a href="{{ route('daycare/dashboard') }}">Profile</a>
                                        </li>
                                        <li>
                                            {{-- <a href="#">Logout</a> --}}
                                            <a href="{{ route('daycare.logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
            
                                            <form id="logout-form" action="{{ route('daycare.logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                  
                                </li>
                                @else
                                <li>
                                    <a href="#">{{ Auth::user()->first_name }} </a>
                                </li>
                                <li class="nav-avatar">
                                    <a href="#">
                                        <img src="{{ asset('assets/img/header-avater.png') }}" alt="avatar">
                                        <span class="reportCounter">4</span>
                                        <span>
                                            <svg width="10" height="5" viewBox="0 0 10 5" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0 0L5 5L10 0H0Z" fill="white"/>
                                            </svg>                                       
                                        </span>
                                    </a>
                                    
                                    <ul class="dropdown">
                                        <li>
                                            <a href="#">Profile</a>
                                        </li>
                                        <li>
                                            <span class="messageCounter">3</span>
                                            <a href="#">Messages</a>
                                        </li>
                                        <li>
                                            <span class="notificationCounter">1</span>
                                            <a href="#">Notification</a>
                                        </li>
                                        <li>
                                            {{-- <a href="#">Logout</a> --}}
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
            
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </li>
                                    </ul>
                                  
                                </li>
                        @endguest

                    </ul>
                </div>


                <div class="hambuger">
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
            </div>

            <div id="menu" class="mobile-menu d-none">

                <div class="close">
                    <span class="bar-close"></span>
                    <span class="bar-close"></span>
                </div>

                <ul class="menu-mobile">

                    <li>
                        <a href="#">About Us</a>
                    </li>
                    <li>
                        <a href="#">Activities</a>
                    </li>
                    <li>
                        <a href="#">Resources</a>
                    </li>
                    <li>
                        <a href="#">Community</a>
                    </li>
                    <li>
                        <a href="#">Daycare Providers</a>
                    </li>
                    
                </ul>
            </div>

        </nav>

        <main>

            <section class="hero-pr">
                <img src="{{ asset('assets/img/help-center-hero.jpg') }}" alt="Do not Sell Breadcrum">
            </section>

            <section class="contact-main">

                <div class="nav-breadcrum">
                    <div>
                        <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li>Help Center</li>
                        </ul>
                    </div>
                </div>

                <div class="help-center">

                    <figure class="help-center-res">

                        <img src="{{ asset('assets/img/helpcenter-parent.jpg') }}" alt="Help center">

                        <figcaption>
                            <a href="{{ route ('faq') }}">Parents</a>
                        </figcaption>
                    </figure>

                    <figure class="help-center-res">

                        <img src="{{ asset('assets/img/helpcenter-provider.jpg') }}" alt="Help center">

                        <figcaption>
                            <a href="{{ route('resources') }}">Providers</a>
                        </figcaption>
                    </figure>
                </div>

            </section>

        </main>
    </body>
</html>

