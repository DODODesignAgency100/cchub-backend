@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="active">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="">
                    <a href="#">Messages</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reviews') }}">Reviews</a>
                </li>
                <li class="">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
            </ul>
        </nav>
    </section>

    <section class="profile-main">
        <div class="schedule">
          <div class="tour">
           <div class="tour-main">
             <div class="tour-form">
               <form>
                 <select name="tour-confirmation" id="tour-confirmation" class="form-control tour-confirmation" onchange="link(this.value)">
                    <option value="{{ route('daycare/tour') }}">Waiting to Confirm</option>
                    <option value="{{ route('daycare/tour-approved') }}" selected>Approved</option>
                    <option value="{{ route('daycare/tour-rejected') }}">Rejected</option>
                 </select>
               </form>
             </div>
 
             <div class="card-display">

                @foreach ($tours as $tour)
                    
                    <div class="tour-card">
                        <img src="{{ asset('assets/img/tourjes.jpg')}}" alt="image placeholder">
                        <div class="tc">
                          <div class="tc-info">
                            Name: <span>{{ $tour->parent_name }}</span>
                          </div>
                          <div class="tc-info">
                            Kid Age: <span>{{ $tour->kid_age }}</span>
                          </div>
                          <div class="tc-info">
                            Booking Date: <span>9th May 2020 {{ date_format(new DateTime($tour->date  ),'l F j, Y') }}</span>
                          </div>
                          <div class="tc-info">
                            Time: <span>{{ date_format(new DateTime($tour->date  ), 'g:i A') }}</span>
                          </div>
                          <div class="tc-btn">
                                <form id="approve-form-{{ $tour->id }}" method="POST"
                                    action="{{ route('tour.approve', $tour->id) }}"
                                    style="display:none">
                                    @csrf
                                </form>
                                    <a class="accept" href=""
                                        onclick="
                                                if(confirm('Do you want to accept this reservation?'))
                                                {event.preventDefault(); document.getElementById('approve-form-{{ $tour->id }}').submit();}
                                                else{
                                                event.preventDefault();
                                            }"> Accept </a>
                                
                                <form id="reject-form-{{ $tour->id }}" method="POST"
                                        action="{{ route('tour.reject', $tour->id) }}"
                                        style="display:none">
                                        @csrf
                                    </form>
                                    <a class="reject" href=""
                                        onclick="
                                                if(confirm('Do you want to reject this tour?'))
                                                {event.preventDefault(); document.getElementById('reject-form-{{ $tour->id }}').submit();}
                                                else{
                                                event.preventDefault();
                                            }"> Reject </a>
                          </div>
                        </div>
                    </div>

                @endforeach
 
             </div>
             
           </div>
          </div>
         
        </div>
    </section>

@endsection
@section('footer')
<script> 
function link(src) {
    window.location= src
}
</script> 
@endsection