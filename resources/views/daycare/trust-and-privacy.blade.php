@extends('layouts.daycare.app')

@section('content')
<section class="hero-pr">
        <img src="{{ asset('assets/img/providers-resources banner.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="pro-resource">

        <h2>Providers</h2>

        <div class="pro-resources">

            <nav class="pro-resources_nav">
                <ul>
                    <li>
                        <a href="{{ route('daycare/resources') }}">Getting Started</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/get-started') }}">Managing Your Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/trust-and-safety') }}" class="p-active">Trust and Safety</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/referrals') }}">Getting Referrals</a>
                    </li>
                </ul>
            </nav>

            <div class="pro-resources_main">

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/vandalism.jpg') }}" alt="vandalism">
                    <figcaption>How Does Childcare Hub Prevent Vandalism and Fake Reviews?</figcaption>
                </figure>

                <figure class="pro-res">
                <img src="{{ asset('assets/img/hide-contact.jpg')}}" alt="Hide Contact">
                <figcaption>Can I Hide My Address or Contact Information?</figcaption>
                </figure>

                <figure class="pro-res">
                <img src="{{ asset('assets/img/mchanges.jpg')}}" alt="Advertising">
                <figcaption>Can Anyone Make Changes to My Page?</figcaption>
                </figure>

                <figure class="pro-res">
                <img src="{{ asset('assets/img/rm-page.jpg')}}" alt="Activity">
                <figcaption>Can I Remove My Page?</figcaption>
                </figure>

            </div>
        </div>

    </section>
@endsection