@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/providers-resources banner.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="pro-resource">

        <h2>Providers</h2>

        <div class="pro-resources">

            <nav class="pro-resources_nav">
                <ul>
                    <li>
                        <a href="{{ route('daycare/resources') }}" class="p-active">Getting Started</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/get-started') }}">Managing Your Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/trust-and-safety') }}">Trust and Safety</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/referrals') }}">Getting Referrals</a>
                    </li>
                </ul>
            </nav>

            <div class="pro-resources_main">

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/hand-on-laptop.jpg')}}" alt="Hand on laptop">
                    <figcaption>How Do I Claim My Page on Childcare Hub?</figcaption>
                </figure>

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/contact.jpg') }}" alt="Hand on laptop">
                    <figcaption>How Do I Contact Someone at Childcare Hub?</figcaption>
                </figure>

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/type-of-day-care-provider.jpg') }}" alt="Hand on laptop">
                    <figcaption>What Types of Child Care Providers Can Be Listed?</figcaption>
                </figure>

            </div>
        </div>

    </section>
@endsection