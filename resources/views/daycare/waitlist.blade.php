@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="active">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="">
                    <a href="#">Messages</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reviews</a>
                </li>
                <li class="">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
            </ul>
        </nav>
    </section>

    <section class="profile-main">
        <div class="waitlist">
            @isset($waitlists)
                
                @if(count($waitlists) != 0)

                    <div class="waitlist-col">

                        <h2>Name</h2>

                        @foreach ($waitlists ?? '' as $waitlist)

                            <div class="data">
                                <p>{{ $waitlist->user->first_name }}</p>
                            </div>

                        @endforeach

                    </div>
    
                    <div class="waitlist-col">
                        <h2>Email Address</h2>
                
                        @foreach ($waitlists as $waitlist)

                            <div class="data">
                                <p>{{ $waitlist->user->email }}</p>
                            </div>

                        @endforeach
                    </div>
    
                    <div class="waitlist-col">

                        <h2>Phone Number</h2>
                
                        @foreach ($waitlists as $waitlist)

                            <div class="data">
                                <p>{{ $waitlist->user->phone_number }}</p>
                            </div>

                        @endforeach
                    </div>
            
                    <div class="waitlist-col">
                        <h2>Action</h2>

                        @foreach ($waitlists as $waitlist)

                            <div class="data">
                                <form id="approve-form-{{ $waitlist->id }}" method="POST"
                                        action="{{ route('waitlist.approve', $waitlist->id) }}"
                                        style="display:none">
                                        @csrf
                                    </form>
                                    <a class="accept" href=""
                                        onclick="
                                                if(confirm('Do you want to accept this waitlist?'))
                                                {event.preventDefault(); document.getElementById('approve-form-{{ $waitlist->id }}').submit();}
                                                else{
                                                event.preventDefault();
                                            }"> Accept </a>
                                
                                <form id="reject-form-{{ $waitlist->id }}" method="POST"
                                        action="{{ route('waitlist.reject', $waitlist->id) }}"
                                        style="display:none">
                                        @csrf
                                    </form>
                                    <a class="reject" href=""
                                        onclick="
                                                if(confirm('Do you want to reject this waitlist?'))
                                                {event.preventDefault(); document.getElementById('reject-form-{{ $waitlist->id }}').submit();}
                                                else{
                                                event.preventDefault();
                                            }"> Reject </a>
            
                            </div>

                        @endforeach

            
                    </div>
                
                @endif
                
                @else 

                    <div class="waitlist-col">

                        <h2>No waitlist yet</h2>
                
                    </div>
            @endisset
 
        </div>
    </section>


@endsection