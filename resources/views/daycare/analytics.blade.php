@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="">
                    <a href="#">Messages</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reviews') }}">Reviews</a>
                </li>
                <li class="active">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
            </ul>
        </nav>
    </section>

    <section class="profile-main">

        <h1 id="insight">Insights</h1>

        <div class="schedule">

              <div class="analytics">
                    <h2 class="analytics-title">Number of Visits by Parents</h2>
                    <div class="analytics-graph">
                    <div class="graph-chart">
                        <div class="graph-chart--text">
                        <h3>{{$daycareviews_day->total_views ?? ''}}</h3>
                        <p>Your page views from {{ date_format(new DateTime($daycareviews_day->created_at ?? ''  ),'l F j, Y') }}  - {{ date_format(new DateTime($dateNow ?? ''  ),'l F j, Y') }}</p>
                        </div>
                        <canvas id="prChart" width="200" height="200"></canvas>
                    </div>
                    </div>
              </div>

        </div>

    </section>

    <section class="profile-main">

        <!--Accepted Reservations-->  
        <div class="schedule-reserve">
            <button class="accordion">List of Accepted Reservations</button>
            <div class="panel">
                <div class="analytics">
                    {{-- <h2 class="analytics-title">Most Loyal Customer</h2> --}}

                    <div class="analytics-row">

                        <div class="analytics-data-col">
                            <h2>Parent Name</h2>
                            @foreach ($reservations as $reservation)
            
                                <div class="data">
                                    <p>{{ $reservation->parent_name }}</p>
                                </div>
            
                            @endforeach
                        
                        </div>
            
                        <div class="analytics-data-col">
                            <h2>Kid's Name</h2>
            
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ $reservation->kid_name }}</p>
                                </div>
                            @endforeach
                
                        </div>
            
                        <div class="analytics-data-col">
                            <h2>Allergies</h2>
                
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ $reservation->allergies }}</p>
                                </div>
                            @endforeach
                        </div>
            
                        {{-- <div class="analytics-data-col">
                            <h2>Language</h2>
            
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ $reservation->language }}</p>
                                </div>
                            @endforeach
                        </div> --}}

                        <div class="analytics-data-col">
                            <h2>Parent Email</h2>
            
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ $reservation->user->email }}</p>
                                </div>
                            @endforeach
                        </div>

                        <div class="analytics-data-col">
                            <h2>Date Accepted</h2>
            
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ date_format(new DateTime($reservation->updated_at),'D F j, Y') }}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>    
            </div>
        </div>
            
        {{-- <div class="analytics">
            <h2 class="analytics-title">Most Loyal Customer</h2>

            <div class="analytics-row">
                <div class="analytics-data-col">
                    <h2>Name</h2>
                    <div class="data">
                        <img src="{{ asset('assets/img/loyal-image.jpg')}}" alt="most loyal image">
                        <span>Jane Benson</span>
                    </div>

                    <div class="data">
                        <img src="{{ asset('assets/img/loyal-image.jpg')}}" alt="most loyal image">
                        <span>Jane Benson</span>
                    </div>

                    <div class="data">
                        <img src="{{ asset('assets/img/loyal-image.jpg')}}" alt="most loyal image">
                        <span>Jane Benson</span>
                    </div>

                    <div class="data">
                        <img src="{{ asset('assets/img/loyal-image.jpg')}}" alt="most loyal image">
                        <span>Jane Benson</span>
                    </div>

                    <div class="data">
                        <img src="{{ asset('assets/img/loyal-image.jpg')}}" alt="most loyal image">
                        <span>Jane Benson</span>
                    </div>
                </div>

                <div class="analytics-data-col">
                    <h2>Email Address</h2>
                    <div class="data">
                        <p>Jane Benson@gmail.com</p>
                    </div>
        
                    <div class="data">
                        <p>Jane Benson@gmail.com</p>
                    </div>
        
                    <div class="data">
                        <p>Jane Benson@gmail.com</p>
                    </div>
        
                    <div class="data">
                        <p>Jane Benson@gmail.com</p>
                    </div>
        
                    <div class="data">
                        <p>Jane Benson@gmail.com</p>
                    </div>
                </div>

                <div class="analytics-data-col">
                    <h2>Phone Number</h2>
                    <div class="data">
                        <p>999-333-444-55</p>
                    </div>
        
                    <div class="data">
                        <p>999-333-444-55</p>
                    </div>
        
                    <div class="data">
                        <p>999-333-444-55</p>
                    </div>
        
                    <div class="data">
                        <p>999-333-444-55</p>
                    </div>
        
                    <div class="data">
                        <p>999-333-444-55</p>
                    </div>
                </div>

                <div class="analytics-data-col">

                    <h2>Date Joined</h2>
                    <div class="data">
                        <p>29/05/2020</p>
                    </div>
        
                    <div class="data">
                        <p>29/05/2020</p>
                    </div>
        
                    <div class="data">
                        <p>29/05/2020</p>
                    </div>
        
                    <div class="data">
                        <p>29/05/2020</p>
                    </div>
        
                    <div class="data">
                        <p>29/05/2020</p>
                    </div>
    
                </div>
            </div>
        </div> --}}
    </section>
     
    <section class="profile-main">

        {{--<div class="schedule">
        <div class="analytics">
            <h2 class="analytics-title">Least Loyal Customer</h2>
            <div class="analytics-row">
            <div class="analytics-data-col">

                <h2>Name</h2>

                <div class="data">
                    <img src="{{ asset('assets/img/loyal-image.jpg') }}" alt="most loyal image">
                    <span>Jane Benson</span>
                </div>

                
            </div>

            <div class="analytics-data-col">

                <h2>Email Address</h2>
                <div class="data">
                    <p>Jane Benson@gmail.com</p>
                </div>

            </div>

            <div class="analytics-data-col">

                <h2>Phone Number</h2>

                <div class="data">
                    <p>999-333-444-55</p>
                </div>
            </div>

            <div class="analytics-data-col">
                <h2>Date Joined</h2>
                <div class="data">
                    <p>29/05/2020</p>
                </div>
            </div>
            </div>
        </div>--}}
        
        
        <!--Rejected Reservations-->  
        <div class="schedule-reserve">
            <button class="accordion">List of Rejected Reservations</button>
            <div class="panel">
                <div class="analytics">
                    {{-- <h2 class="analytics-title">Most Loyal Customer</h2> --}}

                    <div class="analytics-row">

                        <div class="analytics-data-col">
                            <h2>Parent Name</h2>
                            @foreach ($rejectedReservations as $rReservation)
            
                                <div class="data">
                                    <p>{{ $rReservation->parent_name }}</p>
                                </div>
            
                            @endforeach
                        
                        </div>
            
                        <div class="analytics-data-col">
                            <h2>Kid's Name</h2>
            
                            @foreach ($rejectedReservations as $rReservation)
                                <div class="data">
                                    <p>{{ $rReservation->kid_name }}</p>
                                </div>
                            @endforeach
                
                        </div>
            
                        <div class="analytics-data-col">
                            <h2>Allergies</h2>
                
                            @foreach ($rejectedReservations as $rReservation)
                                <div class="data">
                                    <p>{{ $rReservation->allergies }}</p>
                                </div>
                            @endforeach
                        </div>
            
                        {{-- <div class="analytics-data-col">
                            <h2>Language</h2>
            
                            @foreach ($rejectedReservations as $rReservation)
                                <div class="data">
                                    <p>{{ $rReservation->language }}</p>
                                </div>
                            @endforeach
                        </div> --}}

                        <div class="analytics-data-col">
                            <h2>Parent Email</h2>
            
                            @foreach ($rejectedReservations as $rReservation)
                                <div class="data">
                                    <p>{{ $rReservation->user->email }}</p>
                                </div>
                            @endforeach
                        </div>

                        <div class="analytics-data-col">
                            <h2>Date Accepted</h2>
            
                            @foreach ($rejectedReservations as $rReservation)
                                <div class="data">
                                    <p>{{ date_format(new DateTime($rReservation->updated_at),'D F j, Y') }}</p>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>    
            </div>
        </div>
        
    </section>

@endsection

@section('footer')
<script src="{{ asset('vendor/jquery.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>

    ( function ( $ ) {

        var charts = {
            init: function () {
                // -- Set new default font family and font color to mimic Bootstrap's default styling
                Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
                Chart.defaults.global.defaultFontColor = '#292b2c';

                this.ajaxGetPostMonthlyData();

            },

            ajaxGetPostMonthlyData: function () {
                var urlPath =  'http://' + "{{ url('/') }}" + '/daycare/get-analytics';
                var urlPath = 'http://127.0.0.1:8000/daycare/get-analytics';
                var request = $.ajax( {
                    method: 'GET',
                    url: urlPath
            } );

                request.done( function ( response ) {
                    console.log( response );
                    charts.createCompletedJobsChart( response );
                });
            },

            /**
            * Created the Completed Jobs Chart
            */
            createCompletedJobsChart: function ( response ) {

                var ctx = document.getElementById("prChart");
                var myLineChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: response.months, // The response got from the ajax request containing all month names in the database
                        datasets: [{
                            label: '# of Page Views',
                            lineTension: 0.3,
                            backgroundColor: [
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF'
                            ],
                            borderColor: [
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF',
                                '#02A2AF'
                            ],
                            borderWidth: 1,
                            // pointRadius: 5,
                            // pointBackgroundColor: "rgba(2,117,216,1)",
                            // pointBorderColor: "rgba(255,255,255,0.8)",
                            // pointHoverRadius: 5,
                            // pointHoverBackgroundColor: "rgba(2,117,216,1)",
                            // pointHitRadius: 20,
                            // pointBorderWidth: 1,
                            data: response.post_count_data // The response got from the ajax request containing data for the completed jobs in the corresponding months
                        }],
                    },
                    options: {
                        scales: {
                            xAxes: [{
                                time: {
                                    unit: 'date'
                                },
                                gridLines: {
                                    display: false
                                },
                                ticks: {
                                    maxTicksLimit: 7
                                }
                            }],
                            yAxes: [{
                                ticks: {
                                    min: 0,
                                    max: response.max, // The response got from the ajax request containing max limit for y axis
                                    maxTicksLimit: 5
                                },
                                gridLines: {
                                    color: "rgba(0, 0, 0, .125)",
                                }
                            }],
                        },
                        legend: {
                            display: false
                        }
                    }
                });
            }
        };

        charts.init();

    } )( jQuery );


</script>
    
@show