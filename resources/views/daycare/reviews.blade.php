@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="">
                    <a href="#">Messages</a>
                </li>
                <li class="active">
                    <a href="{{ route('daycare/reviews') }}">Reviews</a>
                </li>
                <li class="">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
                {{-- <li class="">
                    <img src="{{ asset('assets/img/search.svg')}}" alt="search">
                </li> --}}
            </ul>
        </nav>
    </section>

    <section class="profile-main">
        <div class="waitlist">
            
            @if (!$reviews->isEmpty())
                <div class="heading">
                    <h3>Latest Reviews</h3>
        
                    <div class="reviewd-hld">
                        <div class="review-hld--main">
                        
                            @foreach ($reviews as $review)

                                <div class="review-holder">
                                    <div class="review-form">
                                    <div class="review-avatar">
                                        <img src="{{ asset('assets/img/avatar-pic.png')}}" alt="Avatar" class="rev-img">
                                    </div>
                            
                                    <div class="review-form--heading">
                                        <h4>{{ $review->user->first_name }} {{ $review->user->last_name }}</h4>
                                    </div>
                            
                                    <div class="review-m">
                                        <p>{{ $review->comments }}</p>
                                    </div>
                    
                                    {{-- <div class="review-cta">
                                        <div class="parent-review-hld">
                                        <div class="review-cta--left">
                                            <p class="rev-text">
                                            <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M24.7185 2.97538C24.7185 1.75035 23.6406 0.748047 22.3083 0.748047H2.93009C1.59784 0.748047 0.507812 1.75035 0.507812 2.97538V16.3394C0.507812 17.5644 1.59784 18.5667 2.93009 18.5667H19.886L24.7306 23.0214L24.7185 2.97538Z" fill="#333333"/>
                                            </svg>       
                                            <span class="comment-count">3 Comments</span>                 
                                            </p>
                        
                                            <p class="rev-text">
                                            <svg width="26" height="20" viewBox="0 0 26 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M15.4136 5.73073V0.607422L25.1641 9.57321L15.4136 18.539V13.2876C8.44893 13.2876 3.57369 15.3369 0.0913696 19.8198C1.4843 13.4157 5.66308 7.01156 15.4136 5.73073Z" fill="#333333"/>
                                            </svg>                               
                                            <span class="share-count">4 Shares</span>                 
                                            </p>
                        
                                            <p class="rev-text">
                                            <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M0.390625 19.1063H2.65923C3.2831 19.1063 3.79353 18.6369 3.79353 18.0632V8.67613C3.79353 8.10248 3.2831 7.63312 2.65923 7.63312H0.390625V19.1063ZM22.8839 11.68C23.0086 11.4193 23.0767 11.1376 23.0767 10.8456V9.71915C23.0767 8.57183 22.0558 7.63312 20.8081 7.63312H14.5694L15.613 2.78311C15.6697 2.55365 15.6357 2.30333 15.5222 2.09472C15.2613 1.62537 14.9324 1.19773 14.524 0.822247L14.0023 0.332031L6.73138 7.01774C6.30035 7.41409 6.06214 7.94602 6.06214 8.49882V16.676C6.06214 18.0111 7.25316 19.1063 8.71641 19.1063H17.9156C18.7096 19.1063 19.4583 18.7203 19.8666 18.0945L22.8839 11.68Z" fill="#727272"/>
                                            </svg>                                                       
                                            <span class="like-count">0 Likes</span>                 
                                            </p>
                        
                                        </div>
                        
                                        <div class="review-cta--right">
                                            <p class="rev-text">
                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M9.87266 2.01873L9.47266 0.179688H0.472656V15.8116H2.47266V9.37492H8.07266L8.47266 11.214H15.4727V2.01873H9.87266Z" fill="#333333"/>
                                            </svg>
                                            <span>Report</span>
                                            </p>
                                        </div>
                                        </div>
                                    </div> --}}
                                    </div>
                                </div>

                            @endforeach
            
                        </div>
                    
        
                        <div class="otherd">
                            <h3>Top Reviewed Daycares</h3>
                            <div class="other-day">
                                <div class="daycare-avatar">
                                <img src="{{ asset('assets/img/tp-daycare.png')}}" alt="Avatar">
                                </div>
                
                                <div class="other-dy--info">
                                <p class="lt-dy--name">The Little Tulip</p>
                                </div>
                            </div>
            
                        </div>

                    </div>
                </div>

                @else 
                <div class="heading">
                    <h3>No Reviews</h3>
                </div>
            @endif
        </div>
    </section>

@endsection