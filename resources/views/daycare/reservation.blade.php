@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="active">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="">
                    <a href="#">Messages</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reviews') }}">Reviews</a>
                </li>
                <li class="">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
            </ul>
        </nav>
    </section>

    <section class="profile-main">
        
        <div class="waitlist">

            {{-- @php
                dd($reservations);
            @endphp --}}

            {{--@isset( $reservations )
            
                @if ( count($reservations) != 0 )--}}

                    <div class="waitlist-col">
                        <h2>Parent Name</h2>
                        @if ( !is_null($reservations) )
                            @foreach ($reservations as $reservation)
            
                                <div class="data">
                                    <p>{{ $reservation->parent_name }}</p>
                                </div>
            
                            @endforeach
                        @endif
                    
                    </div>
         
                    <div class="waitlist-col">
                        <h2>Kid's Name</h2>
                        
                        @if ( !is_null($reservations) )
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ $reservation->kid_name }}</p>
                                </div>
                            @endforeach
                        @endif
            
                    </div>
         
                    <div class="waitlist-col">
                        <h2>Kid's Age</h2>
        
                        @if ( !is_null($reservations) )
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ $reservation->kid_age }}</p>
                                </div>
                            @endforeach
                        @endif
                    </div>
        
                    <div class="waitlist-col">
                        <h2>Allergies</h2>
            
                        @if ( !is_null($reservations) )
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ $reservation->allergies }}</p>
                                </div>
                            @endforeach
                        @endif
                    </div>
        
                    <div class="waitlist-col">
                        <h2>Language</h2>
        
                        @if ( !is_null($reservations) )
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <p>{{ $reservation->language }}</p>
                                </div>
                            @endforeach
                        @endif
                    </div>
         
                    <div class="waitlist-col">
                        <h2>Action</h2>
        
                        @if ( !is_null($reservations) )
                            @foreach ($reservations as $reservation)
                                <div class="data">
                                    <form id="approve-form-{{ $reservation->id }}" method="POST"
                                            action="{{ route('reservation.approve', $reservation->id) }}"
                                            style="display:none">
                                            @csrf
                                        </form>
                                        <a class="accept" href=""
                                            onclick="
                                                    if(confirm('Do you want to accept this reservation?'))
                                                    {event.preventDefault(); document.getElementById('approve-form-{{ $reservation->id }}').submit();}
                                                    else{
                                                    event.preventDefault();
                                                }"> Accept </a>
                                    
                                    <form id="reject-form-{{ $reservation->id }}" method="POST"
                                            action="{{ route('reservation.reject', $reservation->id) }}"
                                            style="display:none">
                                            @csrf
                                        </form>
                                        <a class="reject" href=""
                                            onclick="
                                                    if(confirm('Do you want to reject this reservation?'))
                                                    {event.preventDefault(); document.getElementById('reject-form-{{ $reservation->id }}').submit();}
                                                    else{
                                                    event.preventDefault();
                                                }"> Reject </a>
            
                                </div>
                            @endforeach
                        @endif
                    
                    </div>

                    {{--@else 

                        <div class="waitlist-col">

                            <h2> No reservations yet </h2>
                        
                        </div>
                    
                @endif
                
                @endisset--}}
 
        </div>
    </section>


@endsection