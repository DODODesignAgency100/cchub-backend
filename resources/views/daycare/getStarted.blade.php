@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/providers-resources banner.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="pro-resource">

        <h2>Providers</h2>

        <div class="pro-resources">

            <nav class="pro-resources_nav">
                <ul>
                    <li>
                        <a href="{{ route('daycare/resources') }}">Getting Started</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/get-started') }}" class="p-active">Managing Your Dashboard</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/trust-and-safety') }}">Trust and Safety</a>
                    </li>
                    <li>
                        <a href="{{ route('daycare/referrals') }}">Getting Referrals</a>
                    </li>
                </ul>
            </nav>

            <div class="pro-resources_main">

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/dashboard.jpg') }}" alt="Dashboard">
                    <figcaption>What Is a Provider Dashboard?</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/handonphone.jpg') }}" alt="Hand on laptop">
                    <figcaption>How Do I Access My Dashboard?</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/pageactivity.jpg')}}" alt="Advertising">
                    <figcaption>Page Activity</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/advertising.jpg') }}" alt="Activity">
                    <figcaption>Advertising</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/info.jpg')}}" alt="Information">
                    <figcaption>Edit Information</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/photo.jpg')}}" alt="Photo">
                    <figcaption>Add Photo</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/opening.jpg')}}" alt="Post Opening">
                    <figcaption>Post Openings</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/location.jpg') }}" alt="Update Location">
                    <figcaption>Update Location</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/Reviews.jpg') }}" alt="Post Opening">
                    <figcaption>Manage Reviews</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/page-management.jpg')}}" alt="Page management">
                    <figcaption>Page Management</figcaption>
                </figure>

            </div>
        </div>

    </section>
@endsection