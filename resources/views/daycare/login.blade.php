<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title> {{ config('app.name', '| Child Care Hub') }} </title>

        <meta name="description" content="Find the right care for your child">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/favicon/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/favicon/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicon/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/favicon/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/favicon/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('asset/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{ asset('assets/favicon/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('assets/favicon/ms-icon-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">

    </head>
 
    <body>

        <main>
            <nav class="nav-bar" aria-labelledby="header menu">

                <div class="main-nav">
                    <div class="logo-pr">
                        <a href="{{ route('provider/home') }}">
                            <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                        </a>
                    </div>

                    <div class="prreg_notifier">
                        <p>Don't have an account? 
                            <a href="{{ route('daycare/register') }}" class="notifier">Sign Up</a>
                        </p>
                    </div>
                </div>
            </nav> 
            
            <section class="prreg-main">

                <div class="prreg-main-img">
                    <img src="{{ asset('assets/img/signup-image.jpeg') }}" alt="Little girl smiling">
                </div>

                <div class="prreg-main-form pd">

                    <div class="form-center">

                        <h1>Log in</h1>

                        <form action="{{ route('daycare.login') }}" method="POST" class="regform">
                            @csrf
                            <div class="form-group si"> 
                                <label for="Email">Email</label>
                                <input type="email" name="email" id="pr-email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                                @error('email')
                                    <span class="err-text" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="form-group si">
                                <label for="Password">Password</label>
                                <input type="password" name="password" id="pr-pass" class="form-control">
                                @error('password')
                                    <span class="err-text" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="lg-action">
                                <div class="form-check form-group si">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
    
                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>

                                <div class="form-forgot">
                                    @if (Route::has('password.request'))
                                        <a href="{{ route('daycare.password.request') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                    @endif
                                </div>
                            </div>

                            <button type="submit" class="pr-submit lg">{{ __('Login') }}</button> 
                            <div class="prreg_notifier__mobile">
                                <p>Don't have an account? <a href="{{ route('daycare/register') }}" class="notifier">Sign up</a></p>
                            </div>

                            {{-- <div class="other-signup">
                                <span class="te">or with</span>
                                <a href="#">
                                    <img src="{{ asset('assets/img/google.svg') }}" alt="Google">
                                </a>
                                <a href="#">
                                    <img src="{{ asset('assets/img/facebookl.svg') }}" alt="Facebook">
                                </a>
                            </div> --}}
                        </form>

                    </div>

                </div>

            </section>

        </main>

    </body>

</html>