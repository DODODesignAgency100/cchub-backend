<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title> {{ config('app.name', '| Child Care Hub') }} </title>

        <meta name="description" content="Find the right care for your child">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/favicon/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/favicon/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicon/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/favicon/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/favicon/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('asset/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{ asset('assets/favicon/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('assets/favicon/ms-icon-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">
    </head>
 
    <body>

        <main>

            <nav class="nav-bar" aria-labelledby="header menu">

                <div class="main-nav">
                    <div class="logo-pr">
                        <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                    </div>

                    <div class="prreg_notifier">
                        <p>Already have an account? 
                            <a href="{{ route('daycare/login') }}" class="notifier">Log In</a>
                        </p>
                    </div>
                </div>
            </nav>       

            <section class="prreg-main">

                <div class="prreg-main-img">
                    <img src="{{ asset('assets/img/childplaying.jpeg')}}" alt=" Girl covering mouth">
                </div>

                <div class="prreg-main-form"> 

                    <h1>Sign Up</h1>

                    @include('includes.messages')

                    {{-- @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif --}}

                    <form action="{{ route('daycare.register') }}" method="POST" class="regform">
                        @csrf

                        <div class="form-group">
                            <label for=pr0fname">First Name <span class="req"> (Required)</span></label>
                            <input type="text" name="first_name" id="pr-fname" class="form-control" required value="{{ old('first_name') }}" autocomplete="">
                            @error('first_name')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Last Name">Last Name <span class="req"> (Required)</span></label>
                            <input type="text" name="last_name" id="pr-lname" class="form-control" required value="{{ old('last_name') }}" autocomplete="">
                            @error('last_name')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Address">Address <span class="req"> (Required)</span></label>
                            <input type="text" name="address" id="p-add" class="form-control" value="{{ old('address') }}" autocomplete="">
                            @error('address')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <label for="pr-add1" class="prlabel">Address Line 2</label>
                            <input type="text" name="address1" id="pr-add1" class="form-control" value="{{ old('address1') }}">
                        </div>

                        <div class="form-group">
                            <label for="pr-city">City <span class="req"> (Required)</span></label>
                            <input type="text" name="city" id="p-city" class="form-control" value="{{ old('city') }}" autocomplete="">
                            @error('city')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Province">Province <span class="req"> (Required)</span></label>
                            <input type="text" name="province" id="p-province" class="form-control" value="{{ old('province') }}" autocomplete="">
                            @error('province')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Province">Zip Code <span class="req"> (Required)</span></label>
                            <input type="text" name="zip" maxlength="6" id="p-zip" class="form-control" value="{{ old('zip') }}" autocomplete="">
                            @error('zip')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Phone Number">Phone Number <span class="req"> (Required)</span></label>
                                <input type="tel" name="phone_number" id="p-tel" class="form-control" value="{{ old('phone_number') }}" autocomplete="">
                            </label>
                            @error('phone_number')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Email">Email <span class="req"> (Required)</span></label>
                            <input type="email" name="email" id="p-email" class="form-control" value="{{ old('email') }}" autocomplete="email">
                            @error('email')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Password">Password <span class="req"> (Required)</span></label>
                            <input type="password" name="password" id="pr-pass" class="form-control">
                            @error('password')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="pr-pass1">Confirm Password <span class="req"> (Required)</span></label>
                            <input type="password" name="password_confirmation" id="pr-pass1" class="form-control">
                        </div>

                        <div class="border-top">
                            <h3>Business</h3>
                        </div>
                  
                        <div class="form-group-fr">
                            <label for="Bank" class="prlabel">Business Name <span class="req"> (Required)</span></label>
                            <input type="text" name="business_name" id="pr-bizname" class="form-control" value="{{ old('business_name') }}">
                            @error('business_name')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <label for="Bank" class="prlabel">Bank <span class="req"> (Required)</span></label>
                            <input type="text" name="bank" id="pr-bank" class="form-control" value="{{ old('bank') }}">
                            @error('bank')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <label for="Account Number" class="prlabel">Account Number <span class="req"> (Required)</span></label>
                                <input type="text" name="account_number" id="pr-no" class="form-control" value="{{ old('account_number') }}">
                            </label>
                            @error('aaount_number')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <h3 class="l-h">Type of Service</h3>
                            <input type="radio" name="service_type" id="ts" value="daycare">
                            <label for="ts" class="prlabel-radio">Daycare</label>
                            <input type="radio" name="service_type" id="sit" value="sitter">
                            <label for="sit" class="prlabel-radio">Sitter</label>
                        </div>
                  
                        {{-- <div class="form-group-pr">
                            <h3 class="l-h">How Frequently do you want your Payment</h3>
                            <input type="radio" name="payment_type" id="ts" value="daycare">
                            <label for="Type of Service" class="prlabel-radio">Daycare</label>
                            <input type="radio" name="payment_type" id="sit" value="sitter">
                            <label for="Type of Service" class="prlabel-radio">Sitter</label>
                        </div> --}}
                  
                        {{-- <div class="form-group-pr" id="adpr">
                            <h3 class="l-h">Address</h3>
                            <input type="radio" name="businessAddressIsPersonalAdrress" id="ts" value="yes">
                            <label for="Type of Service" class="prlabel-radio">Same address as personal profile</label>
                        </div> --}}
                  
                        <div class="border-top" style="margin-bottom: .5rem;">
                            <h3>Business Address</h3>
                            <input type="radio" name="businessAddressIsPersonalAdrress" id="ts" value="yes" onclick="reply_click(this);">
                            <label for="Type of Service" class="prlabel-radio">Same address as personal profile</label>
                        </div>
                  
                        <div class="form-group-fr">
                            <label for="pr-bizname-add" class="prlabel">Address <span class="req"> (Required)</span></label>
                            <input type="text" name="business_address" id="b-add" class="form-control" value="{{ old('business_address') }}">
                            @error('business_address')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <label for="pr-city1" class="prlabel">City <span class="req"> (Required)</span></label>
                            <input type="text" name="b_city" id="b-city" class="form-control" value="{{ old('b_city') }}">
                            @error('b_city')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <label for="pr-province1" class="prlabel">Province <span class="req"> (Required)</span></label>
                                <input type="text" name="b_province" id="b-province" class="form-control" value="{{ old('b_province') }}">
                            </label>
                            @error('b_province')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
{{--                   
                        <div class="form-group-pr">
                            <label for="State" class="prlabel">State <span class="req"> (Required)</span></label>
                            <input type="text" name="b_state" id="pr-state" class="form-control" value="{{ old('b_state') }}">
                            @error('b_state')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div> --}}
                  
                        <div class="form-group-pr">
                            <label for="Zip Code" class="prlabel">Zip Code <span class="req"> (Required)</span></label>
                            <input type="text" name="b_zip" id="b-zip" class="form-control" maxlength="6" value="{{ old('b_zip') }}">
                            @error('b_zip')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <label for="pr-tel1" class="prlabel">Phone Number <span class="req"> (Required)</span></label>
                            <input type="tel" name="b_phone" id="b-tel" class="form-control" value="{{ old('b_phone') }}">
                            @error('b_phone')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <label for="biz email" class="prlabel">Business Email <span class="req"> (Required)</span></label>
                            <input type="email" name="b_email" id="b-email" class="form-control" value="{{ old('b_email') }}">
                            @error('b_email')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-pr">
                            <label for="License" class="prlabel">License Number <span class="req"> (Required)</span></label>
                            <input type="number" name="license_number" id="pr-license" class="form-control" value="{{ old('license_number') }}">
                            @error('license_number')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                  
                        <div class="form-group-fr">
                            <label for="how you heard" class="prlabel">How did you hear about us?</label>
                            <textarea rows="4" cols="50" class="form-control" name="testomonial"></textarea>
                            @error('testimonial')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-label full">
                            <input type="checkbox" name="terms" id="checkbox" value="Agree">
                            <label for="checkbox" id="checktext">I have read and agree to the <a href="{{ route('terms') }}">terms of service</a> and <a href="{{ route('privacy') }}">privacy policy</a></label>
                        </div>

                        <button type="submit" class="pr-submit">Sign up</button> 

                        {{-- <div class="other-signup">
                            <span class="te">or with</span>
                            <a href="#">
                                <img src="{{ asset('assets/img/google.svg') }}" alt="Google">
                            </a>
                            <a href="#">
                                <img src="{{ asset('assets/img/facebookl.svg') }}" alt="Facebook">
                            </a>
                        </div> --}}

                    </form>

                </div>

            </section>

        </main>

    </body>

    <script type="text/javascript">
        function reply_click(ba) {
            var address = ba.checked ? document.getElementById("p-add").value : '';
            var city = ba.checked ? document.getElementById("p-city").value : '';
            var province = ba.checked ? document.getElementById("p-province").value : '';
            var zip = ba.checked ? document.getElementById("p-zip").value : '';
            var tel = ba.checked ? document.getElementById("p-tel").value : '';
            var email = ba.checked ? document.getElementById("p-email").value : '';

            document.getElementById("b-add").value = address;
            document.getElementById("b-city").value = city;
            document.getElementById("b-province").value = province;
            document.getElementById("b-zip").value = zip;
            document.getElementById("b-tel").value = tel;
            document.getElementById("b-email").value = email;
        }
    </script>

    <script>
        function space(el, after) {
            after = after || 4;
            var v = el.value.replace(/[^\dA-Z]/g, ''),
                reg = new RegExp(".{" + after + "}","g")
            el.value = v.replace(reg, function (a, b, c) {
                return a + ' ';
            });
        }
        
        var el = document.getElementById('p-zip');
        el.addEventListener('keyup', function () {
            space(this, 3);
        });
    </script>

</html>