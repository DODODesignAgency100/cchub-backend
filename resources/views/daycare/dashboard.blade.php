@extends('layouts.daycare.app')

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" />
@endsection

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Breadcrum Image">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="active">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="">
                    <a href="#">Messages</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reviews') }}">Reviews</a>
                </li>
                <li class="">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li>
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
                {{-- <li class="">
                    <img src="{{ asset('assets/img/search.svg')}}" alt="search">
                </li> --}}
            </ul>
        </nav>
    </section>

    <section class="profile-main">
        <div class="profile-accordion">

            <!--ACCOUNT DETAILS-->
            @include('includes.messages')
            
            <div class="acc-holder">
                <button class="accordion">Account Details</button>
                <div class="panel">
                    <form action="{{ route('user.update', Auth::user()->id) }}" method="POST" class="updateform">
                    @csrf
                    @method('PUT')
                    <div class="form-group-up">
                        <label for="First Name">First name</label>
                        <input type="text" name="first_name" id="up-fname" class="form-control" value="{{ $user->first_name }}">
                    </div>

                    <div class="form-group-up">
                        <label for="Last Name">Last name</label>
                        <input type="text" name="last_name" id="up-lname" class="form-control" value="{{ $user->last_name }}">
                    </div>

                    <div class="form-group-up">
                        <label for="email">Email Address</label>
                        <input type="email" name="email" id="up-email" class="form-control" value="{{ $user->email }}">
                    </div>

                    <div class="form-group-up">
                        <label for="phonenumber">Phone Number</label>
                        <input type="tel" name="phone_number" id="up-mobile" class="form-control" value="{{ $user->phone_number }}">
                    </div>

                    <span><small>By clicking "Save" you agree to our <a target="_blank" href="{{ route('terms') }}">Terms of Service</a> and the <a target="_blank" href="{{ route('privacy') }}">Privacy Policy</a>.</small></span>
                    <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>
                    </form>
                </div>
            </div>
        
            <!--Daycare info-->
            <div class="acc-holder">
                <button class="accordion">Daycare Info</button>
                <div class="panel">
                    <form action="{{ route('info.update', Auth::user()->id) }}" method="POST" class="updateform">
                        @csrf
                        @method('PUT')

                    <div class="form-group-up">
                        <label for="DaycareName">Daycare Name</label>
                        <input type="text" name="business_name" id="up-lname" class="form-control" value="{{ $user->business_name }}">
                    </div>

                    <div class="form-group-up">
                        <label for="address">Address</label>
                        <input type="text" name="business_address" id="up-address" class="form-control" value="{{ $user->business_address }}">
                    </div>

                    <div class="form-group-up">
                        <label for="daycarelicense">Daycare License Number</label>
                        <input type="text" name="license_number" id="up-license" class="form-control" value="{{ $user->license_number }}">
                    </div>

                    <div class="form-group-up">
                        <label for="phonenumber">Phone Number</label>
                        <input type="tel" name="b_phone" id="up-mobile" class="form-control" value="{{ $user->b_phone }}">
                    </div>

                    {{-- <p id="more-li">
                        <img src="{{ asset('assets/img/addmore.svg')}}" alt="addmore-button">
                        Add another license number
                    </p> --}}

                    <div class="form-label-up">
                        @if ($user->show_address === 'yes')
                            <input type="checkbox" name="show_address" id="checkbox" value="yes" checked>
                        @else
                            <input type="checkbox" name="show_address" id="checkbox" value="yes" checked>
                        @endif
                        <label for="label check" id="checktext">Show Address on the profile page and map</label>
                    </div>
                    
                    <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>
                    </form>
                </div> 
            </div>
     
            <!--Timeslots-->   
            <div class="acc-holder">
                <button class="accordion">Timeslots</button>

                <div class="panel">
                    @if ($timeslot)
                    <form action="{{ route('timeslot.update', $timeslot->id) }}" method="POST" class="updateform">
                        @csrf
                        @method('PUT')
                        <div class="form-group-up">
                            <label for="regularhours">Regular Hours</label>
                            <select id="reghro" name="rOpenhour" class="form-control">
                                @if ($timeslot->rOpenhour === '7am')
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->rOpenhour === '7:15am')
                                    <option value="7:15am" selected>7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am" >8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->rOpenhour === '7:30am')
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am" selected>7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->rOpenhour === '8:00am')
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am" selected>8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @else
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am" selected>8:15AM</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="regularhours" class="hide">Regular Hours</label>
                            <select id="reghrc" name="rClosehour" class="form-control">
                                <option value="Closing Hour" selected>Select Closing Hour</option>
                                @if ($timeslot->rClosehour === '9pm')
                                    <option value="9pm" >9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @elseif ($timeslot->rClosehour === '9:15pm')
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm" selected>9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>  
                                @elseif ($timeslot->rClosehour === '9:30pm')
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm" selected>9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @elseif ($timeslot->rClosehour === '10pm')
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm" selected>10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @else
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm" selected>10:15PM</option>                          
                                @endif
                            </select>
                        </div>

                        <div class="form-group-full">
                            <h3>Special Hours Care</h3>
                            <p><small>Check the box below if you offer weekend, evening, or overnight care upon request from parents.</small></p>
                        </div>

                        <div class="form-group-up">
                            <input type="checkbox" name="regularhours" id="regularhours" class="checkbox">
                            <label for="regularhours">Weeknight Care</label>
                            <select id="specialhro" name="wknOpenHour" class="form-control">
                                @if ($timeslot === 'NULL')
                                    <option value="Open" selected>Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->wknOpenHour === '7am')
                                    <option value="Open">Open</option>
                                    <option value="7am" selected>7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->wknOpenHour === '7:15am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am" selected>7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am" >8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->wknOpenHour === '7:30am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am" selected>7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->wknOpenHour === '8:00am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am" selected>8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @else
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am" selected>8:15AM</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="regularhours" class="hide">Regular Hours</label>
                            <select id="specialhrc" name="wknCloseHour" class="form-control">
                                @if ($timeslot->wknCloseHour === '9pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @elseif ($timeslot->wknCloseHour === '9:15pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm" selected>9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>  
                                @elseif ($timeslot->wknCloseHour === '9:30pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm" selected>9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @elseif ($timeslot->wknCloseHour === '10pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm" selected>10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @else
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm" selected>10:15PM</option>                          
                                @endif
                            </select>
                        </div>

                        <div class="form-group-up">
                            <input type="checkbox" name="regularhours" id="regularhours" class="checkbox">
                            <label for="regularhours">Weekend Care</label>
                            <select id="specialhro" name="wkcOpenHour" class="form-control">
                                @if ($timeslot === 'NULL')
                                    <option value="Open" selected>Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->wkcOpenHour === '7am')
                                    <option value="Open">Open</option>
                                    <option value="7am" selected>7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->wkcOpenHour === '7:15am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am" selected>7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am" >8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->wkcOpenHour === '7:30am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am" selected>7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->wkcOpenHour === '8:00am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am" selected>8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @else
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am" selected>8:15AM</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="" class="hide">Regular Hours</label>
                            <select id="specialhrc" name="wkcCloseHour" class="form-control">
                                @if ($timeslot->wkcCloseHour === '9pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @elseif ($timeslot->wkcCloseHour === '9:15pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm" selected>9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>  
                                @elseif ($timeslot->wkcCloseHour === '9:30pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm" selected>9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @elseif ($timeslot->wkcCloseHour === '10pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm" selected>10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @else
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm" selected>10:15PM</option>                          
                                @endif
                            </select>
                        </div>

                        <div class="form-group-up">
                            <input type="checkbox" name="regularhours" id="regularhours" class="checkbox">
                            <label for="regularhours">Overnight Care</label>
                            <select id="specialhro" name="ovrOpenHour" class="form-control">
                                @if ($timeslot === 'NULL')
                                    <option value="Open" selected>Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->ovrOpenHour === '7am')
                                    <option value="Open">Open</option>
                                    <option value="7am" selected>7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->ovrOpenHour === '7:15am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am" selected>7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am" >8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->ovrOpenHour === '7:30am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am" selected>7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @elseif ($timeslot->ovrOpenHour === '8:00am')
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am" selected>8:00AM</option>
                                    <option value="8:15am">8:15AM</option>
                                @else
                                    <option value="Open">Open</option>
                                    <option value="7am">7AM</option>
                                    <option value="7:15am">7:15AM</option>
                                    <option value="7:30am">7:30AM</option>
                                    <option value="8:00am">8:00AM</option>
                                    <option value="8:15am" selected>8:15AM</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="" class="hide">Regular Hours</label>
                            <select id="specialhrc" name="ovrCloseHour" class="form-control">
                                @if ($timeslot->ovrCloseHour === '9pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @elseif ($timeslot->ovrCloseHour === '9:15pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm" selected>9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>  
                                @elseif ($timeslot->ovrCloseHour === '9:30pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm" selected>9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @elseif ($timeslot->ovrCloseHour === '10pm')
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm" selected>10:00PM</option>
                                    <option value="10:15pm">10:15PM</option>   
                                @else
                                    <option value="Closing Hour" selected>Select Closing Hour</option>
                                    <option value="9pm">9:00PM</option>
                                    <option value="9:15pm">9:15PM</option>
                                    <option value="9:30pm">9:30PM</option>
                                    <option value="10pm">10:00PM</option>
                                    <option value="10:15pm" selected>10:15PM</option>                          
                                @endif
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="Socialmedia">Social Media Links</label>
                            <input type="url" name="sociallinkfacebook" id="up-sm" class="form-control" value="{{ $timeslot->sociallinkfacebook }}">
                        </div>

                        <div class="form-group-up">
                            <label for="daycareurl">Daycare Website URL</label>
                            <input type="url" name="website" id="up-web" class="form-control" value="{{ $timeslot->website }}">
                        </div>

                        <!--<p id="more-li">-->
                        <!--    <img src="{{ asset('assets/img/addmore.svg')}}" alt="addmore-button">-->
                        <!--    Add More Links-->
                        <!--</p>-->

                        <div class="form-label-up">
                            @if ($user->show_address === 'yes')
                                <input type="checkbox" name="show_address" id="checkbox" value="yes" checked>
                            @else
                                <input type="checkbox" name="show_address" id="checkbox" value="yes">
                            @endif
                            <label for="label check" id="checktext">Show Address on the profile page and map</label>
                        </div>

                        <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>
                    </form>

                    @else
                    <form action="{{ route('timeslot') }}" method="POST" class="updateform">
                        @csrf
                        <div class="form-group-up">
                            <label for="regularhours">Regular Hours</label>
                            <select id="reghro" name="rOpenhour" class="form-control">
                            <option value="Closing Hour" selected>Select Opening Hour</option>
                            <option value="7am">7:00AM</option>
                            <option value="7:15am">7:15AM</option>
                            <option value="7:30am">7:30AM</option>
                            <option value="8:00am">8:00AM</option>
                            <option value="8:15am">8:15AM</option>
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="regularhours" class="hide">Regular Hours</label>
                            <select id="reghrc" name="rClosehour" class="form-control">
                            <option value="Opening Hour" selected>Select Closing Hour</option>
                            <option value="9pm">9:00PM</option>
                            <option value="9:15pm">9:15PM</option>
                            <option value="9:30pm">9:30PM</option>
                            <option value="10pm">10:00PM</option>
                            <option value="10:15pm">10:15PM</option>
                            </select>
                        </div>

                        <div class="form-group-full">
                            <h3>Special Hours Care</h3>
                            <p><small>Check the box below if you offer weekend, evening, or overnight care upon request from parents.</small></p>
                        </div>

                        <div class="form-group-up">
                            <input type="checkbox" name="regularhours" id="regularhours" class="checkbox">
                            <label for="regularhours">Weeknight Care</label>
                            <select id="specialhro" name="wknOpenHour" class="form-control">
                            <option value="Opening Hour" selected>Select Opening Hour</option>
                            <option value="7am">7:00AM</option>
                            <option value="7:15am">7:15AM</option>
                            <option value="7:30am">7:30AM</option>
                            <option value="8:00am">8:00AM</option>
                            <option value="8:15am">8:15AM</option>
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="regularhours" class="hide">Regular Hours</label>
                            <select id="specialhrc" name="wknCloseHour" class="form-control">
                            <option value="Closing Hour" selected>Select Closing Hour</option>
                            <option value="9pm">9:00PM</option>
                            <option value="9:15pm">9:15PM</option>
                            <option value="9:30pm">9:30PM</option>
                            <option value="10pm">10:00PM</option>
                            <option value="10:15pm">10:15PM</option>
                            </select>
                        </div>

                        <div class="form-group-up">
                            <input type="checkbox" name="regularhours" id="regularhours" class="checkbox">
                            <label for="regularhours">Weekend Care</label>
                            <select id="specialhro" name="wkcOpenHour" class="form-control">
                            <option value="Opening Hour" selected>Select Opening Hour</option>
                            <option value="7am">7:00AM</option>
                            <option value="7:15am">7:15AM</option>
                            <option value="7:30am">7:30AM</option>
                            <option value="8:00am">8:00AM</option>
                            <option value="8:15am">8:15AM</option>
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="" class="hide">Regular Hours</label>
                            <select id="specialhrc" name="wkcCloseHour" class="form-control">
                            <option value="Closing Hour" selected>Select Closing Hour</option>
                            <option value="9pm">9:00PM</option>
                            <option value="9:15pm">9:15PM</option>
                            <option value="9:30pm">9:30PM</option>
                            <option value="10pm">10:00PM</option>
                            <option value="10:15pm">10:15PM</option>
                            </select>
                        </div>

                        <div class="form-group-up">
                            <input type="checkbox" name="regularhours" id="regularhours" class="checkbox">
                            <label for="regularhours">Overnight Care</label>
                            <select id="specialhro" name="ovrOpenHour" class="form-control">
                            <option value="Opening Hour" selected>Select Opening Hour</option>
                            <option value="7am">7:00AM</option>
                            <option value="7:15am">7:15AM</option>
                            <option value="7:30am">7:30AM</option>
                            <option value="8:00am">8:00AM</option>
                            <option value="8:15am">8:15AM</option>
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="" class="hide">Regular Hours</label>
                            <select id="specialhrc" name="ovrCloseHour" class="form-control">
                            <option value="Closing Hour" selected>Select Closing Hour</option>
                            <option value="9pm">9:00PM</option>
                            <option value="9:15pm">9:15PM</option>
                            <option value="9:30pm">9:30PM</option>
                            <option value="10pm">10:00PM</option>
                            <option value="10:15pm">10:15PM</option>
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="Socialmedia">Social Media Links</label>
                            <input type="url" name="sociallinkfacebook" id="up-sm" class="form-control">
                        </div>

                        <div class="form-group-up">
                            <label for="daycareurl">Daycare Website URL</label>
                            <input type="url" name="website" id="up-web" class="form-control">
                        </div>

                        <!--<p id="more-li">-->
                        <!--    <img src="{{ asset('assets/img/addmore.svg')}}" alt="addmore-button">-->
                        <!--    Add More Links-->
                        <!--</p>-->

                        {{-- <div class="form-label-up">
                            <input type="checkbox" name="check" id="checkbox">
                            <label for="label check" id="checktext">Show Address on the profile page and map</label>
                        </div> --}}

                        <div class="btn-holder">
                            <button class="pr-btn" id="save">Save</button>
                        </div>
                    </form>

                    @endif
                </div> 
            </div>

            <!--Services-->
            <div class="acc-holder">
                <button class="accordion">Services</button>
                <div class="panel">

                    @if ($service)
                        
                        <form action="{{ route('services.update', $service->id) }}" method="POST" class="updateform">
                            @csrf
                            @method('PUT')
                            <div class="form-group-up">
                                <label for="TotalKids">Total Number of Kids</label>
                                <input type="number" value="{{ $service->total_kids }}" name="total_kids" id="dailyhour" class="form-control" min="0">
                                
                            </div>
    
                            <div class="form-group-up">
                                <label for="Totalteachers">Total Number of Teachers</label>
                                <input type="number" value="{{ $service->total_teachers }}" name="total_teachers" id="dailyhour" class="form-control" min="0">
                            </div>
    
                            <div class="service-label">

                                @php
                                    $service_lange = json_decode($service->meals_offered);
                                    // dd($service_lange);
                                @endphp

                                <h3>Selected Meals Offered</h3>

                                @foreach ($service_lange as $sl)
                                    <div class="form-label-up">
                                        <input type="checkbox" name="meals_offered[]" id="checkbox" value="{{$sl ?? ''}}" checked>
                                        <label for="label check" id="snack">{{$sl ?? ''}}</label>
                                    </div>
                                @endforeach

                                <h3>Other Meals Offered</h3>
                                <div class="form-label-up">
                                <input type="checkbox" name="meals_offered[]" id="checkbox" value="Snacks">
                                <label for="label check" id="snack">Snacks</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="meals_offered[]" id="checkbox" value="Breakfast">
                                <label for="label check" id="bfast">Breakfast</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="meals_offered[]" id="checkbox" value="Lunch">
                                <label for="label check" id="lunch">Lunch</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="meals_offered[]" id="checkbox" value="Dinner">
                                <label for="label check" id="dinner">Dinner</label>
                                </div>

                            </div>
    
                            <div class="service-label">

                                @php
                                    $daycare_language = json_decode($service->daycare_languages);
                                @endphp

                                <h3>Selected Meals Offered</h3>

                                @foreach ($daycare_language as $dl)
                                    <div class="form-label-up">
                                        <input type="checkbox" name="daycare_languages[]" id="checkbox" value="{{$dl}}" checked>
                                        <label for="label check" id="snack">{{$dl}}</label>
                                    </div>
                                @endforeach

                                <h3>Other Languages Used at Daycare</h3>
        
                                <div class="form-label-up">
                                    <input type="checkbox" name="daycare_languages[]" id="checkbox" value="English">
                                    <label for="en" id="en">English</label>
                                </div>
    
                                <div class="form-label-up">
                                    <input type="checkbox" name="daycare_languages[]" id="checkbox" value="French">
                                    <label for="es" id="es">French</label>
                                </div>
    
                                <div class="form-label-up">
                                    <input type="checkbox" name="daycare_languages[]" id="checkbox" value="Chinese">
                                    <label for="Chinese" id="Chinese">Chinese</label>
                                </div>
    
                                <div class="form-label-up">
                                    <input type="checkbox" name="daycare_languages[]" id="checkbox" value="others">
                                    <label for="label check" id="Otherlang">Others</label>
                                </div>
                            </div>
                        
                            <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>
                        </form>

                    @else 

                        <form action="{{ route('services') }}" method="POST" class="updateform">
                            @csrf
                            <div class="form-group-up">
                                <label for="TotalKids">Total Number of Kids</label>
                                <input type="number" name="total_kids" id="dailyhour" class="form-control" min="0">
                                
                            </div>
    
                            <div class="form-group-up">
                                <label for="Totalteachers">Total Number of Teachers</label>
                                <input type="number" name="total_teachers" id="dailyhour" class="form-control" min="0">
                            </div>

                            <div class="service-label">
                                <h3>Meals Offered</h3>
                                <div class="form-label-up">
                                <input type="checkbox" name="meals_offered[]" id="checkbox" value="Snacks">
                                <label for="label check" id="snack">Snacks</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="meals_offered[]" id="checkbox" value="Breakfast">
                                <label for="label check" id="bfast">Breakfast</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="meals_offered[]" id="checkbox" value="Lunch">
                                <label for="label check" id="lunch">Lunch</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="meals_offered[]" id="checkbox" value="Dinner">
                                <label for="label check" id="dinner">Dinner</label>
                                </div>
                            </div>

                            <div class="service-label">
                                <h3>Languages Used at Daycare</h3>
                                <div class="form-label-up">
                                <input type="checkbox" name="daycare_languages[]" id="checkbox" value="English">
                                <label for="en" id="en">English</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="daycare_languages[]" id="checkbox" value="French">
                                <label for="es" id="es">French</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="daycare_languages[]" id="checkbox" value="Chinese">
                                <label for="Chinese" id="Chinese">Chinese</label>
                                </div>

                                <div class="form-label-up">
                                <input type="checkbox" name="daycare_languages[]" id="checkbox" value="others">
                                <label for="label check" id="Otherlang">Others</label>
                                </div>
                            </div>

                            <!--<div class="form-group-full">-->
                            <!--    <h3>Teachers’ English Level</h3>-->
                            <!--    <p><small>This will help parents know how much communication they can expect from the teachers</small></p>-->
                            <!--</div>-->

                            <!--<div class="form-group-up">-->
                            <!--    <select id="native" name="teacher_language_level" class="form-control">-->
                            <!--    <option value="Native">Native</option>-->
                            <!--    <option value="Chinese">Chinese</option>-->
                            <!--    <option value="African">African</option>-->
                            <!--    </select>-->
                            <!--</div>-->
                        
                            <div class="btn-holder">
                                <button class="pr-btn" id="save">Save</button>
                            </div>
                        </form>

                    @endif
                </div> 
            </div>

            <!--Rates-->
            <div class="acc-holder">

                <button class="accordion">Rates</button>
                
                <div class="panel">
                        
                    @if ($rates) 

                    
                    <form action="{{ route('rates.update', $rates->id) }}" method="POST" class="updateform">
                        @csrf
                        @method('PUT')
                        
                        <div class="form-group-up">
                            <label for="Infantage">Infant Age <span class="light">(2-year-old and under)</span></label>
                            <select id="Infantage" name="infant_age" class="form-control" value="{{ $rates->infant_age }}">
                                    <option value="min-age">Choose Minimum Age</option>
                                    <option value="0 - 6 Months">0 - 6 Months</option>
                                    <option value="7 - 12 Months">7 - 12 Months</option>
                                    <option value="1 Year">1 Year</option>
                                    <option value="2 Years">2 Years</option>
                            @if($rates->infant_age === 'min-age')
                                <option value="min-age" selected>Choose Minimum Age</option>
                            @endif
                            @if($rates->infant_age === '0 - 6 Months')
                                <option value="0 - 6 Months" selected>1 Month</option>
                            @endif
                            @if($rates->infant_age === '2 Month')
                                <option value="2 Month" selected>2 Month</option>
                            @endif
                            @if($rates->infant_age === '3 Month')
                                <option value="3 Month" selected>3 Month</option>
                            @endif
                            @if($rates->infant_age === '4 Month')
                                <option value="4 Month" selected>4 Month</option>
                            @endif
                            @if($rates->infant_age === '5 Month')
                                <option value="5 Month" selected>5 Month</option>
                            @endif
                            @if($rates->infant_age === '6 Month')
                                <option value="6 Month" selected>6 Month</option>
                            @endif
                            @if($rates->infant_age === '7 Month')
                                <option value="7 Month" selected>7 Month</option>
                            @endif
                            @if($rates->infant_age === '8 Month')
                                <option value="8 Month" selected>8 Month</option>
                            @endif
                            @if($rates->infant_age === '9 Month')
                                <option value="9 Month" selected>9 Month</option>
                            @endif
                            @if($rates->infant_age === '10 Month')
                                <option value="11 Month" selected>10 Month</option>
                            @endif
                            @if($rates->infant_age === '11 Month')
                                <option value="1 Year" selected>11 Year</option>
                            @endif
                            @if($rates->infant_age === '1 Year')
                                <option value="1 Year" selected> 1 Year</option>
                            @endif
                            @if($rates->infant_age === '1 year +')
                                <option value="1 Year +" selected> > Year</option>
                            @endif
                            @if($rates->infant_age === '2 Year')
                                <option value="2 Year" selected>2 Year</option>
                            @endif
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Daily </label>
                                
                            <input type="number" value="{{ $rates->infant_hours }}" name="infant_hours" id="dailyhour" class="form-control" min="0">
                            
                        </div>

                        <div class="form-group-up">
                            <label for="hourly"> Hourly </label>
                            <input type="number" value="{{ $rates->infant_daily_hours }}" name="infant_daily_hours" id="hourly" class="form-control" min="0">
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Monthly </label>
                            <input type="number" value="{{ $rates->infant_monthly_hours }}" name="infant_monthly_hours" id="monthly" class="form-control" min="0">
                        </div>

                        <div class="form-group-up">
                            <label for="weekly"> Weekly </label>
                            <input type="number" value="{{ $rates->infant_weekly_hours }}" name="infant_weekly_hours" id="weekly" class="form-control" min="0">
                        </div>

                        <div class="form-group-up">
                            <label for="Weekend/Overnight"> Weekend/Overnight </label>
                            <input type="number" value="{{ $rates->infant_weekend_overnights }}" name="infant_weekend_overnights" id="Weekend/Overnight" class="form-control" min="0">
                        </div>

                        <div class="form-group-up">
                            <label for="Weeknight"> Weeknight </label>
                            <input type="number" value="{{ $rates->infant_week_nights }}" name="infant_week_nights" id="Weeknight" class="form-control" min="0">
                        </div>

                        <div class="form-break"></div>
                        <div class="form-label-up">
                            <input type="checkbox" name="infant_no_offers" id="not-interested" value="{{ $rates->infants_no_offer ? 'checked' : '' }}">
                            <label for="label check" id="checktext">I don't offer care for this age group</label>
                        </div>
                    
                        <!--Rates-next-section-->
                        <div class="border-top-dark"></div>
                        <div class="form-group-up">
                            <label for="presschoolrate">Preschooler Rate <span class="light">(2 - 4 year old)</span></label>
                            <select id="preschoolrate" name="preschool_age" class="form-control" value="{{ $rates->preschool_age }}">
                            <option value="min-age">Select a Minimum Age</option>
                            <option value="1 Year Old">1 Year Old</option>
                            <option value="2 Year Old">2 Year Old</option>
                            <option value="3 Year Old">3 Year Old</option>
                            <option value="4 Year Old">4 Year Old</option>
                            
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Daily </label>
                            <input type="number" value="0" name="preschool_daily_hours" id="preschool-dailyhour" class="form-control" min="0" value="{{ $rates->preschool_daily_hours }}">
                        </div>

                        <div class="form-group-up">
                            <label for="hourly"> Hourly </label>
                            <input type="number" value="0" name="preschool_hours" id="preschool-hourly" class="form-control" min="0" value="{{ $rates->preschool_hours }}">
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Monthly </label>
                            <input type="number" value="0" name="preschool_monthly_hours" id="preschool-monthly" class="form-control" min="0" value="{{ $rates->preschool_monthly_hours }}">
                        </div>

                        <div class="form-group-up">
                            <label for="weekly"> Weekly </label>
                            <input type="number" value="0" name="preschool_weekly_hours" id="preschool-weekly" class="form-control" min="0" value="{{ $rates->preschool_weekly_hours }}">
                        </div>

                        <div class="form-group-up">
                            <label for="Weekend/Overnight"> Weekend/Overnight </label>
                            <input type="number" value="0" name="preschool_weekend_overnights" id="preschool-weekend-overnight" class="form-control" min="0" value="{{ $rates->preschool_weekend_overnights }}">
                        </div>

                        <div class="form-group-up">
                            <label for="Weeknight"> Weeknight </label>
                            <input type="number" value="0" name="preschool_week_nights" id="preschool-weeknight" class="form-control" min="0" value="{{ $rates->preschool_week_nights }}">
                        </div>

                        <div class="form-break"></div>
                        <div class="form-label-up form-break">
                            <input type="checkbox" name="preschool_no_offers" id="not-interested" value="{{ $rates->preschool_no_offers }}">
                            <label for="label check" id="checkt_not-interested">I don't offer care for this age group</label>
                        </div>

                        <!--Next rate-->
                        <div class="border-top-dark"></div>
                        <div class="form-group-up">
                            <label for="schoolagerate">School-age Rate <span class="light">(Kindergarten and older)</span></label>
                            <select id="schoolagerate" name="school_age" class="form-control" value="{{ $rates->school_age }}">
                            <option value="min-age">Select a Minimum Age</option>
                            <option value="5 Year Old">5 Year Old</option>
                            <option value="6 Year Old">6 Year Old</option>
                            <option value="7 Year Old">7 Year Old</option>
                            <option value="8 Year Old">8 Year Old</option>
                            <option value="9 Year Old">9 Year Old</option>
                            </select>
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Daily </label>
                            <input type="number" value="0" name="school_daily_hours" id="school-dailyhour" class="form-control" min="0" value="{{ $rates->school_daily_hours }}">
                        </div>

                        <div class="form-group-up">
                            <label for="hourly"> Hourly </label>
                            <input type="number" value="0" name="school_hours" id="school-hourly" class="form-control" min="0" value="{{ $rates->school_hours }}">
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Monthly </label>
                            <input type="number" value="0" name="school_monthly_hours" id="school-monthly" class="form-control" min="0" value="{{ $rates->school_monthly_hours }}">
                        </div>

                        <div class="form-group-up">
                            <label for="weekly"> Weekly </label>
                            <input type="number" value="0" name="school_weekly_hours" id="school-weekly" class="form-control" min="0" value="{{ $rates->school_weekly_hours }}">
                        </div>

                        <div class="form-group-up">
                            <label for="Weekend/Overnight"> Weekend/Overnight </label>
                            <input type="number" value="0" name="school_weekend_overnights" id="school-weekend-overnight" class="form-control" min="0" value="{{ $rates->school_weekend_overnights }}">
                        </div>

                        <div class="form-group-up">
                            <label for="Weeknight"> Weeknight </label>
                            <input type="number" value="0" name="school_week_nights" id="school-weeknight" class="form-control" min="0" value="{{ $rates->school_week_nights }}">
                        </div>

                        <div class="form-break"></div>
                        <div class="form-label-up form-break">
                            <input type="checkbox" name="school_no_offers" id="not-interested" value="{{ $rates->school_no_offers }}">
                            <label for="label check" id="checkt_not-interested">I don't offer care for this age group</label>
                        </div>

                        <div class="form-label-up form-break">
                            @if ($rates->show_rates === 'checked')
                            <input type="checkbox" name="show_rates" id="showrate" value="checked" checked>
                            @endif
                            <label for="label check" id="showrate-pg">Show Rate on Daycare Page</label>
                        </div>
                        
                        <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>
                        
                    </form>
                   
                    
                    @else
                    <form action="{{ route('rates') }}" method="POST" class="updateform">
                        @csrf
                        <div class="form-group-up">
                            <label for="Infantage">Baby <span class="light">(0 - 12 months)</span></label>
                            <select id="Infantage" name="infant_age" class="form-control" value=" {{ old('infant_age') ? 'selected' : '' }}">
                                <option value="min-age">Select a Minimum Age</option>
                                <option value="0 - 6 Months">0 - 6 Months</option>
                                <option value="7 - 12 Months">7 - 12 Months</option>
                                
                            </select>
                        </div>

                        

                        <div class="form-group-up">
                            <label for="hourly"> Hourly </label>
                            <input type="number" value="0" name="infant_daily_hours" id="hourly" class="form-control" min="0">
                        </div>
                        
                        <div class="form-group-up">
                            <label for="dailyhour"> Daily </label>
                                
                            <input type="number" value="0" name="infant_hours" id="dailyhour" class="form-control" min="0">
                            
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Monthly </label>
                            <input type="number" value="0" name="infant_monthly_hours" id="monthly" class="form-control" min="0">
                        </div>

                        <!--<div class="form-group-up">-->
                        <!--    <label for="weekly"> Weekly </label>-->
                        <!--    <input type="number" value="0" name="infant_weekly_hours" id="weekly" class="form-control" min="0">-->
                        <!--</div>-->

                        <!--<div class="form-group-up">-->
                        <!--    <label for="Weekend/Overnight"> Weekend/Overnight </label>-->
                        <!--    <input type="number" value="0" name="infant_weekend_overnights" id="Weekend/Overnight" class="form-control" min="0">-->
                        <!--</div>-->

                        <!--<div class="form-group-up">-->
                        <!--    <label for="Weeknight"> Weeknight </label>-->
                        <!--    <input type="number" value="0" name="infant_week_nights" id="Weeknight" class="form-control" min="0">-->
                        <!--</div>-->

                        <div class="form-break"></div>
                        <div class="form-label-up">
                            <input type="checkbox" name="infant_no_offers" id="not-interested" value="checked">
                            <label for="label check" id="checktext">I don't offer care for this age group</label>
                        </div>
                        
                        <!--New addition Rates-next-section-->
                        <div class="border-top-dark"></div>
                        <div class="form-group-up">
                            <label for="toodler">Toddler <span class="light">(1 - 3 years)</span></label>
                            <select id="toddler" name="toddler" class="form-control">
                                <option value="min-age">Select a Minimum Age</option>
                                <option value="1 Year">1 Year</option>
                                <option value="2 Years">2 Years</option>
                                <option value="3 Years">3 Years</option>
                            </select>
                        </div>


                        <div class="form-group-up">
                            <label for="toddler-hourly"> Hourly </label>
                            <input type="number" value="0" name="toddler-hourly" id="toddler-hourly" class="form-control" min="0">
                        </div>

                        <div class="form-group-up">
                            <label for="toddler_daily_hours"> Daily </label>
                            <input type="number" value="0" name="toddler_daily_hours" id="toddler_daily_hours" class="form-control" min="0">
                        </div>
                        
                        <div class="form-group-up">
                            <label for="toddler_monthly_hours"> Monthly </label>
                            <input type="number" value="0" name="toddler_monthly_hours" id="toddler_monthly_hours" class="form-control" min="0">
                        </div>


                        <div class="form-break"></div>
                        <div class="form-label-up form-break">
                            <input type="checkbox" name="preschool_no_offers" id="not-interested">
                            <label for="label check" id="checkt_not-interested">I don't offer care for this age group</label>
                        </div>
                    
                        <!--Rates-next-section-->
                        <div class="border-top-dark"></div>
                        <div class="form-group-up">
                            <label for="presschoolrate">Preschooler Rate <span class="light">(3 - 5 years)</span></label>
                            <select id="preschoolrate" name="preschool_age" class="form-control">
                                <option value="min-age">Select a Minimum Age</option>
                                <option value="3 Years">3 Years</option>
                                <option value="4 Years">4 Years </option>
                                <option value="5 Years">5 Years</option>
                            </select>
                        </div>
                        
                        <div class="form-group-up">
                            <label for="hourly"> Hourly </label>
                            <input type="number" value="0" name="preschool_hours" id="preschool-hourly" class="form-control" min="0">
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Daily </label>
                            <input type="number" value="0" name="preschool_daily_hours" id="preschool-dailyhour" class="form-control" min="0">
                        </div>

                        

                        <div class="form-group-up">
                            <label for="dailyhour"> Monthly </label>
                            <input type="number" value="0" name="preschool_monthly_hours" id="preschool-monthly" class="form-control" min="0">
                        </div>

                        <!--<div class="form-group-up">-->
                        <!--    <label for="weekly"> Weekly </label>-->
                        <!--    <input type="number" value="0" name="preschool_weekly_hours" id="preschool-weekly" class="form-control" min="0">-->
                        <!--</div>-->

                        <!--<div class="form-group-up">-->
                        <!--    <label for="Weekend/Overnight"> Weekend/Overnight </label>-->
                        <!--    <input type="number" value="0" name="preschool_weekend_overnights" id="preschool-weekend-overnight" class="form-control" min="0">-->
                        <!--</div>-->

                        <!--<div class="form-group-up">-->
                        <!--    <label for="Weeknight"> Weeknight </label>-->
                        <!--    <input type="number" value="0" name="preschool_week_nights" id="preschool-weeknight" class="form-control" min="0">-->
                        <!--</div>-->

                        <div class="form-break"></div>
                        <div class="form-label-up form-break">
                            <input type="checkbox" name="preschool_no_offers" id="not-interested">
                            <label for="label check" id="checkt_not-interested">I don't offer care for this age group</label>
                        </div>
                        
                        

                        <!--Next rate-->
                        <div class="border-top-dark"></div>
                        <div class="form-group-up">
                            <label for="schoolagerate">Grade School <span class="light">(5 - 12 years)</span></label>
                            <select id="schoolagerate" name="school_age" class="form-control">
                                <option value="min-age">Select a Minimum Age</option>
                                <option value="6 Years">5 Years Old</option>
                                <option value="6 Years">6 Years Old</option>
                                <option value="7 Years">7 Years Old</option>
                                <option value="8 Years">8 Years Old</option>
                                <option value="9 Years">9 Years Old</option>
                                <option value="10 Years">10 Years Old</option>
                                <option value="11 Years">11 Years Old</option>
                                <option value="12 Years">12 Years Old</option>
                                
                            </select>
                        </div>
                        
                        <div class="form-group-up">
                            <label for="hourly"> Hourly <span class="light"></span></label>
                            <input type="number" value="0" name="school_hours" id="school-hourly" class="form-control" min="0">
                        </div>

                        <div class="form-group-up">
                            <label for="dailyhour"> Daily <span class="light"></span></label>
                            <input type="number" value="0" name="school_daily_hours" id="school-dailyhour" class="form-control" min="0">
                        </div>


                        <div class="form-group-up">
                            <label for="dailyhour"> Monthly <span class="light"></span></label>
                            <input type="number" value="0" name="school_monthly_hours" id="school-monthly" class="form-control" min="0">
                        </div>

                        <!--<div class="form-group-up">-->
                        <!--    <label for="weekly"> Weekly <span class="light"></span></label>-->
                        <!--    <input type="number" value="0" name="school_weekly_hours" id="school-weekly" class="form-control" min="0">-->
                        <!--</div>-->

                        <!--<div class="form-group-up">-->
                        <!--    <label for="Weekend/Overnight"> Weekend/Overnight <span class="light"></span></label>-->
                        <!--    <input type="number" value="0" name="school_weekend_overnights" id="school-weekend-overnight" class="form-control" min="0">-->
                        <!--</div>-->

                        <!--<div class="form-group-up">-->
                        <!--    <label for="Weeknight"> Weeknight <span class="light"></span></label>-->
                        <!--    <input type="number" value="0" name="school_week_nights" id="school-weeknight" class="form-control" min="0">-->
                        <!--</div>-->

                        <div class="form-break"></div>
                        <div class="form-label-up form-break">
                            <input type="checkbox" name="school_no_offers" id="not-interested">
                            <label for="label check" id="checkt_not-interested">I don't offer care for this age group</label>
                        </div>

                        <div class="form-label-up form-break">
                            <input type="checkbox" name="show_rates" id="showrate" value="checked">
                            <label for="label check" id="showrate-pg">Show Rate on Daycare Page</label>
                        </div>
                        
                        <div class="btn-holder">
                            <button class="pr-btn" id="save">Save</button>
                        </div>
                    </form>

                    @endif
                    
                    
                </div> 
            </div>

            <!--Daycare Director Photo-->   
            <div class="acc-holder">

                <button class="accordion">Photos of Daycare Director</button>
                
                <div class="panel">
                    <form action="{{ route('image-upload', Auth::user()->id) }}" method="post" enctype="multipart/form-data" style="width: 40%;">
                        @csrf
                        @method('PUT')

                        <p>Image to be showed on home page</p>

                        <input type="file" name="director_image" id="edit-image" class="dropify" data-default-file="/daycare/{{$user->director_image ?? ''}}">

                        <div class="btn-holder">
                            <button type="submit" class="pr-btn" id="save">Save</button>
                        </div>

                    </form>
                    
                </div>
            </div>

            <!--Daycare Photo-->   
            <div class="acc-holder">
                <button class="accordion">Photos of Daycare</button>
                <div class="panel">
                    <form action="{{ route('photo_upload', Auth::user()->id) }}" method="post" enctype="multipart/form-data" style="width: 40%;">
                        @csrf
                        @method('PUT')

                        <p>Image to be showed on home page</p>
                        
                        <input type="file" name="daycare_photo[]" id="edit-image" class="dropify" data-allowed-file-extensions="png jpg webp" data-max-file-size="1M" data-default-file="/daycare/{{ $user->daycare_photo }}">
                        <input type="file" name="daycare_photo[]" id="edit-image" class="dropify" data-allowed-file-extensions="png jpg webp" data-max-file-size="1M" data-default-file="/daycare/{{ $user->daycare_photo }}">
                        <input type="file" name="daycare_photo[]" id="edit-image" class="dropify" data-allowed-file-extensions="png jpg webp" data-max-file-size="1M" data-default-file="/daycare/{{ $user->daycare_photo }}">
                        <input type="file" name="daycare_photo[]" id="edit-image" class="dropify" data-allowed-file-extensions="png jpg webp" data-max-file-size="1M" data-default-file="/daycare/{{ $user->daycare_photo }}">
                        <input type="file" name="daycare_photo[]" id="edit-image" class="dropify" data-allowed-file-extensions="png jpg webp" data-max-file-size="1M" data-default-file="/daycare/{{ $user->daycare_photo }}">

                        <div class="btn-holder">
                            <button type="submit" class="pr-btn" id="save">Save</button>
                        </div>
                    </form>
                </div>
            </div>

            <!--Description-->   
            <div class="acc-holder">

                <button class="accordion">Description</button>

                <div class="panel">

                    @if ($items) 

                        <form action="{{ route('description.update', $items->id) }}" method="POST" class="updateform" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <div class="form-group-fl">
                                <label for="Daycare overview">Daycare Overview <span class="light">(Your history, program overview, unique features of your care)</span></label>
                                <textarea cols="50" rows="20" name="overview" class="form-control"> {{ $items->overview }} </textarea>
                            </div>
                
                            <div class="form-group-fl">
                                <label for="About School Owner">About School Owner / Director <span class="light">(Your history, program overview, unique features of your care)</span></label>
                                <textarea cols="50" rows="20" name="director" class="form-control"> {{ $items->director }} </textarea>
                            </div>
                
                            <div class="border-top-dark">
                                <h3>Daily Schedule</h3>
                            </div>
                
                            <div class="form-group-up">
                                <label for="Time">Time</label>
                                <input type="time" name="dailyScheduleTime1" id="timeschedule" class="form-control" value="{{ $items->dailyScheduleTime1 }}">
                            </div>
                
                            <div class="form-group-up">
                                <label for="Activity">Activity</label>
                                <input type="text" name="dailyScheduleActivity1" id="activity" class="form-control" value="{{ $items->dailyScheduleActivity1 }}">
                            </div>

                            <div class="form-group-up">
                                <label for="Time">Time</label>
                                <input type="time" name="dailyScheduleTime2" id="timeschedule" class="form-control" value="{{ $items->dailyScheduleTime2 }}">
                            </div>
                
                            <div class="form-group-up">
                                <label for="Activity">Activity</label>
                                <input type="text" name="dailyScheduleActivity2" id="activity" class="form-control" value="{{ $items->dailyScheduleActivity2 }}">
                            </div>

                            <div class="form-group-up">
                                <label for="Time">Time</label>
                                <input type="time" name="dailyScheduleTime3" id="timeschedule" class="form-control" value="{{ $items->dailyScheduleTime3 }}">
                            </div>
                
                            <div class="form-group-up">
                                <label for="Activity">Activity</label>
                                <input type="text" name="dailyScheduleActivity3" id="activity" class="form-control" value="{{ $items->dailyScheduleActivity3 }}">
                            </div>
                            
                            {{-- <div class="btn-holder"><button class="pr-btn" id="save">Add</button></div> --}}

                            <!--<div class="border-top-dark">-->
                            <!--    <h3>Tour policy</h3>-->
                            <!--</div>-->

                            <!--<div class="form-label-up half">-->
                            <!--    <input type="radio" name="tour_policy" id="parent-tour">-->
                            <!--    <label for="label check" id="parent-tour_label" class="light">Parents are welcome to tour before making reservation</label>-->
                            <!--</div>-->

                            <!--<div class="form-label-up half">-->
                            <!--    <input type="radio" name="tour_policy" id="parent-tour_not">-->
                            <!--    <label for="label check" id="parent-tour_l-not" class="light">We do not provide tours for drop-in care families</label>-->
                            <!--</div>-->

                            <!--<div class="form-label-up half">-->
                            <!--    <input type="radio" name="tour_policy" id="parent-reserve">-->
                            <!--    <label for="label check" id="parent-reserve_label" class="light">Parent must make reservations before tour</label>-->
                            <!--</div>-->

                            <!--<div class="form-label-up half">-->
                            <!--    <input type="radio" name="tour_policy" id="parent-tour_not_n">-->
                            <!--    <label for="label check" id="parent-tour_not_nn" class="light">Parents can’t are not welcome to take tour</label>-->
                            <!--</div>-->

                            <!--<div class="form-label-up form-break">-->
                            <!--    <input type="checkbox" name="provide_tour" id="virtualtour-main" value="yes">-->
                            <!--    <label for="label check" id="virtualtour">Provide Virtual Tour</label>-->
                            <!--</div>-->

                            <!--<div id="v-tour">-->
                            <!--    <p class="form-break">Upload Images for 360 experience</p>-->

                            <!--    <div class="virtual-images">-->
                            <!--    <span></span>-->
                            <!--    <span></span>-->
                            <!--    <span></span>-->
                            <!--    <span></span>-->
                            <!--    </div>-->
                            <!--</div>-->

                            <div class="btn-holder">
                                <button class="pr-btn" id="save">Save</button>
                            </div>

                        </form>

                    @else
                        <form action="{{ route('dashboard.descriptions') }}" method="POST" class="updateform">
                            @csrf

                            <div class="form-group-fl">
                                <label for="Daycare overview">Daycare Overview <span class="light">(Your history, program overview, unique features of your care)</span></label>
                                <textarea cols="50" rows="20" name="overview" required class="form-control"></textarea>
                            </div>
                
                            <div class="form-group-fl">
                                <label for="About School Owner">About School Owner / Director <span class="light">(Your history, program overview, unique features of your care)</span></label>
                                <textarea cols="50" rows="20" name="director" required class="form-control"></textarea>
                            </div>
                
                            <div class="border-top-dark">
                                <h3>Daily Schedule</h3>
                            </div>
                
                            <div class="form-group-up">
                                <label for="Time">Time</label>
                                <input type="time" name="dailyScheduleTime1" id="timeschedule" class="form-control">
                            </div>
                
                            <div class="form-group-up">
                                <label for="Activity">Activity</label>
                                <input type="text" name="dailyScheduleActivity1" id="activity" class="form-control">
                            </div>

                            <div class="form-group-up">
                                <label for="Time">Time</label>
                                <input type="time" name="dailyScheduleTime2" id="timeschedule" class="form-control">
                            </div>
                
                            <div class="form-group-up">
                                <label for="Activity">Activity</label>
                                <input type="text" name="dailyScheduleActivity2" id="activity" class="form-control">
                            </div>

                            <div class="form-group-up">
                                <label for="Time">Time</label>
                                <input type="time" name="dailyScheduleTime3" id="timeschedule" class="form-control">
                            </div>
                
                            <div class="form-group-up">
                                <label for="Activity">Activity</label>
                                <input type="text" name="dailyScheduleActivity3" id="activity" class="form-control">
                            </div>
                            
                            {{-- <div class="btn-holder"><button class="pr-btn" id="save">Add</button></div> --}}

                            <div class="border-top-dark">
                                <h3>Tour policy</h3>
                            </div>

                            <div class="form-label-up half">
                                <input type="radio" name="tour_policy" id="parent-tour">
                                <label for="label check" id="parent-tour_label" class="light">Parents are welcome to tour before making reservation</label>
                            </div>

                            <div class="form-label-up half">
                                <input type="radio" name="tour_policy" id="parent-tour_not">
                                <label for="label check" id="parent-tour_l-not" class="light">We do not provide tours for drop-in care families</label>
                            </div>

                            <div class="form-label-up half">
                                <input type="radio" name="tour_policy" id="parent-reserve">
                                <label for="label check" id="parent-reserve_label" class="light">Parent must make reservations before tour</label>
                            </div>

                            <div class="form-label-up half">
                                <input type="radio" name="tour_policy" id="parent-tour_not_n">
                                <label for="label check" id="parent-tour_not_nn" class="light">Parents are not welcome to take tour</label>
                            </div>

                            <div class="form-label-up form-break">
                                <input type="checkbox" name="provide_tour" id="virtualtour-main" value="yes">
                                <label for="label check" id="virtualtour">Provide Virtual Tour</label>
                            </div>

                            <!--<div id="v-tour">-->
                            <!--    <p class="form-break">Upload Images for 360 experience</p>-->

                            <!--    <div class="virtual-images">-->
                            <!--    <span></span>-->
                            <!--    <span></span>-->
                            <!--    <span></span>-->
                            <!--    <span></span>-->
                            <!--    </div>-->
                            <!--</div>-->

                            <!--<div class="btn-holder">-->
                            <!--    <button class="pr-btn" id="save">Save</button>-->
                            <!--</div>-->

                        </form>
                    @endif
                </div>
            </div>

            <!--Features of Your Daycare-->  
            <div class="acc-holder">
                <button class="accordion">Features of Your Daycare</button>
                <div class="panel">

                    @if ($feature) 

                    <h3>These are your Daycare Features</h3>

                        @php
                            $feat = json_decode($feature->features);
                        @endphp
                        
                        <form class="updateform" style="margin-bottom:2rem;">
                            @foreach ($feat as $ft)
                                <div class="form-feat-label">
                                    <input type="checkbox" name="features[]" id="firstaid" value="{{ $ft }}" readonly checked>
                                    <label for="label check">{{ $ft }}</label>
                                </div>
                            @endforeach
                        </form>

                    <h3>All Features</h3>

                    <form action="{{ route('features.update', $feature->id) }}" method="POST" class="updateform">
                        @csrf
                        @method('PUT')
                        
                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="firstaid" value="Teachers are CPR and First Aid certified">
                            <label for="label check">Teachers are CPR and First Aid certified</label>
                        </div>

                        
                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="arts" value="Arts & crafts">
                            <label for="label check">Arts & crafts</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="outdoor" value="Outdoor play area available">
                            <label for="label check">Outdoor play area available</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="based" value="play-based">
                            <label for="label check">Play-based</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="sci" value="Science, cooking project">
                            <label for="label check">Science, cooking project</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="potty" value="Children must be potty-trained">
                            <label for="label check">Children must be potty-trained</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="music" value="Music and dance">
                            <label for="label check">Music and dance</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="Montessori" value="Montessori inspired">
                            <label for="label check">Montessori inspired</label>
                        </div>
                        
                        <div class="form-feat-label"></div>
                        
                        <div class="form-feat-label">
                            <input type="text" name="features[]" id="Others1" class="form-control" maxlength="18">
                        </div>
                        
                        <div class="form-feat-label">
                            <input type="text" name="features[]" id="Others2" class="form-control" maxlength="18">
                        </div>

                        <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>

                    </form>

                    @else 

                    <form action="{{ route('features') }}" method="POST" class="updateform">
                        @csrf

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="firstaid" value="Teachers are CPR and First Aid certified">
                            <label for="label check">Teachers are CPR and First Aid certified</label>
                        </div>

                       

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="arts" value="Arts & crafts">
                            <label for="label check">Arts & crafts</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="outdoor" value="Outdoor play area available">
                            <label for="label check">Outdoor play area available</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="based" value="play-based">
                            <label for="label check">Play-based</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="sci" value="Science, cooking project">
                            <label for="label check">Science, cooking project</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="potty" value="Children must be potty-trained">
                            <label for="label check">Children must be potty-trained</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="music" value="Music and dance">
                            <label for="label check">Music and dance</label>
                        </div>
                        

                        <div class="form-feat-label">
                            <input type="checkbox" name="features[]" id="Montessori" value="Montessori inspired">
                            <label for="label check">Montessori inspired</label>
                        </div>
                        
                        <div class="form-feat-label"></div>
                        
                        <div class="form-feat-label">
                            <input type="text" name="features[]" id="Others1" class="form-control" maxlength="18">
                        </div>
                        
                        <div class="form-feat-label">
                            <input type="text" name="features[]" id="Others2" class="form-control" maxlength="18">
                        </div>

                        <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>

                    </form>

                    @endif
                    
                </div>
            </div>

         
            <!--What to Bring for Drop-off-->  
            <div class="acc-holder">

                <button class="accordion">What to Bring for Drop-off</button>

                <div class="panel">

                    @if ($drop) 

                    <h3>Selected Lists</h3>

                        @php
                            $dp = json_decode($drop->drops);
                        @endphp
                        
                        <form class="updateform" style="margin-bottom:2rem;">
                            @foreach ($dp as $drop_list)
                                <div class="form-feat-label">
                                    <input type="checkbox" id="firstaid" value="{{ $drop_list }}" readonly checked>
                                    <label for="label check">{{ $drop_list }}</label>
                                </div>
                            @endforeach
                        </form>

                    <h3>All Lists</h3>

                    <form action="{{ route('drops.update', $drop->id)  }}" method="POST" class="updateform">
                        @csrf
                        @method('PUT')
                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Diapers" value="Diapers">
                            <label for="label check">Diapers</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Wipes" value="Wipes">
                            <label for="label check">Wipes</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="clothes" value="Change of clothes">
                            <label for="label check">Change of clothes</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Formula" value="Formula / Breastmilk (for infant)">
                            <label for="label check">Formula / Breastmilk (for infant)</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Lunch" value="Lunch">
                            <label for="label check">Lunch</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Snack" value="Snack">
                            <label for="label check">Snack</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Blanket" value="Blanket">
                            <label for="label check">Blanket</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Nap sheet" value="Nap sheet">
                            <label for="label check">Nap sheet</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="sippy" value="Sippy cup / water bottle">
                            <label for="label check">Sippy cup / water bottle</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Bib" value="Bib">
                            <label for="label check">Bib</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Toys" value="Toys">
                            <label for="label check">Toys</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Swimming trucks" value="Swimming trucks">
                            <label for="label check">Swimming trucks</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Other" value="Others">
                            <label for="label check">Other</label>
                        </div>

                        <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>

                    </form>

                    @else

                    <form action="{{ route('drops') }}" method="POST" class="updateform">
                        @csrf
                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Diapers" value="Diapers">
                            <label for="label check">Diapers</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Wipes" value="Wipes">
                            <label for="label check">Wipes</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="clothes" value="Change of clothes">
                            <label for="label check">Change of clothes</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Formula" value="Formula / Breastmilk (for infant)">
                            <label for="label check">Formula / Breastmilk (for infant)</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Lunch" value="Lunch">
                            <label for="label check">Lunch</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Snack" value="Snack">
                            <label for="label check">Snack</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Blanket" value="Blanket">
                            <label for="label check">Blanket</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Nap sheet" value="Nap sheet">
                            <label for="label check">Nap sheet</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="sippy" value="Sippy cup / water bottle">
                            <label for="label check">Sippy cup / water bottle</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Bib" value="Bib">
                            <label for="label check">Bib</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Toys" value="Toys">
                            <label for="label check">Toys</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Swimming trucks" value="Swimming trucks">
                            <label for="label check">Swimming trucks</label>
                        </div>

                        <div class="form-feat-label">
                            <input type="checkbox" name="drops[]" id="Other" value="Others">
                            <label for="label check">Other</label>
                        </div>

                        <div class="btn-holder"><button class="pr-btn" id="save">Save</button></div>

                    </form>
                    @endif

                </div>
            </div>

            <!--Manage Subscription-->
            <div class="acc-holder">
                <button class="accordion">Manage your Subscription</button>
                <div class="panel">
                    <ul style="margin: 1rem;">
                        <li>Subscription automatically renews unless you cancel them.</li>
                        <li>If you cancel, you can not keep using the subscription as subscription ends instantly.</li>
                    </ul>

                    <div class="btn-holder">
                        <a href="{{ route('daycare/cancel') }}" class="renew-btn">Click to Cancel</a>
                    </div>
                    
                </div>
            </div>

       </div>
    </section>

@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
    <script>
        $('.dropify').dropify();
    </script>
@endsection
