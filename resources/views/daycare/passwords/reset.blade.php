<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title> {{ config('app.name', '| Child Care Hub') }} </title>

        <meta name="description" content="Find the right care for your child">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
    </head>


{{-- <div class="container">
    <div class="row justify-content-center"> 
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @include('includes.messages')
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

    <body>

        <main>
    
            <nav class="nav-bar" aria-labelledby="header menu">
                <div class="main-nav">

                    <div class="logo-pr">
                        <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                    </div>
            
                    <div class="prreg_notifier">
                        <p>Already have an account? <a href="{{ route('login') }}" class="notifier">Log In</a></p>
                    </div>

                </div>
            </nav>       
    
            <section class="prreg-main">

                <div class="prreg-main-img">
                    <img src="{{ asset('assets/img/covering-mouth.jpg') }}" alt=" Girl covering mouth">
                </div>
        
                <div class="prreg-main-form p-reset">

                    <h1>{{ __('Reset Password') }}</h1>

                   @include('includes.messages')

                    <form action="{{ route('daycare.password.update') }}" method="POST" class="regform">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group" id="si">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" readonly>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group" id="si">
                            <label for="Password"> {{ __('New Password') }} </label>
                            <input type="password" name="password" id="pr-pass" class="form-control  @error('password') is-invalid @enderror" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
            
                        <div class="form-group" id="si">
                            <label for="Password"> {{ __('Confirm Password') }}</label>
                            <input type="password" name="password_confirmation" required autocomplete="new-password" id="pr-pass" class="form-control">
                        </div>
            
                        <button type="submit" class="pr-submit">{{ __('Reset Password') }}</button> 

                    </form>
                    
                </div>
            </section>
    
        </main>
    
    </body>
</html>

