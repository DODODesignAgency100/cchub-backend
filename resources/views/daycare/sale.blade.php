@extends('layouts.daycare.app')

@section('content')
        
    <section class="hero-pr">
        <img src="{{ asset ('assets/img/DNS.jpg') }}" alt="Do not Sell Breadcrum">
    </section>

    <section class="privacy-main">

        <div class="nav-breadcrum">
            <div>
                <ul class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li>Do not Sell my Information</li>
                </ul>
            </div>
        </div>

        <div class="privacy-main--hero">
            <img src="{{ asset('assets/img/donotsell-bread.jpg') }}" alt="Do not sell Hero Image">
        </div>

        <div class="privacy-content">
            <div class="privacy-content--nav">
                <nav class="privacy-content_nav">
                    <ul>
                        <li><a href="privacy.html">Privacy Policy</a></li>
                        <li><a href="{{ route('daycare/terms-of-use') }}">Terms of Use</a></li>
                        <li><a href="{{ route('daycare/do-not-sell') }}" class="p-active">Do not Sell my Information</a></li>
                    </ul>
                </nav>

                <div class="privacy-content--main">

                    <div class="privacy-content--inner">
                        <h3>Do Not Sell My Personal Information</h3>
                        <p>At CCH, we honor the importance of personal information. That’s why:</p>
        
                        <ul>
                        <li>We’re not selling any personal information.</li>
                        <li>We don’t have plans to sell personal information.</li>
                        </ul>

                        <p class="gap-top">For more information, read our <a href="privacy.html">Privacy Policy</a> or <a href="terms-of-use.html">Term of use</a> about personal data. We respect and understand that you may wish to ensure your personal information isn’t sold in the future. If so, please update your status below.</p>
                    </div>

                </div>
            </div>                
        </div>
    </section>

@endsection