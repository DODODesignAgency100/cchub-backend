@extends('layouts.daycare.app')

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" />
@endsection

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Breadcrum Image">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="active">
                    <a href="{{ route('daycare/messages') }}">Messages</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reviews') }}">Reviews</a>
                </li>
                <li class="">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li>
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
            </ul>
        </nav>
    </section>

    <section class="profile-main">
       <div class="messages">
            
            <div class="heading">
                <h2>Direct</h2>
            </div>
         
            <a class="provider-message" href="providers-chat.html">
                <div class="provider-message--col">
                    <div class="message-data">
                        <img src="{{ asset('assets/img/parent.png') }}" alt="Messenger-Image">
                        <h3>Jane Benson</h3>
                    </div>
                </div>

                <div class="provider-message--col">
                    <div class="message-data">
                        <p>Thank you so much for making james a better person each and every day......</p>
                    </div>
                </div>

                <div class="provider-message--col">
                    <div class="message-data">
                        <p>3 mins</p>
                    </div>
                </div>
            </a>

            <!--The next message-->
            <a class="provider-message" href="providers-chat.html">
                <div class="provider-message--col">
                    <div class="message-data">
                        <img src="{{ asset('assets/img/parent.png') }}" alt="Messenger-Image">
                        <h3>Jane Benson</h3>
                    </div>
                </div>

                <div class="provider-message--col">
                    <div class="message-data">
                        <p>Thank you so much for making james a better person each and every day......</p>
                    </div>
                </div>

                <div class="provider-message--col">
                    <div class="message-data">
                        <p>3 mins</p>
                    </div>
                </div>
            </a>

       </div>
    </section>

@endsection