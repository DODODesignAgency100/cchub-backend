@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li class="active">
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="">
                    <a href="#">Messages</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reviews') }}">Reviews</a>
                </li>
                <li class="">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
                {{-- <li class="">
                    <img src="{{ asset('assets/img/search.svg')}}" alt="search">
                </li> --}}
            </ul>
        </nav>
    </section>

    <section class="profile-main">
            @include('includes.messages')
        <div class="schedule">
            <div class="schedule-main">

                <h2>Edit Schedule</h2>

                <div class="schedule-main_ctrl">
                    <h3>Infant</h3>

                    @if ($schedule)
                        
                        <form action="{{ route('schedule.update', $schedule->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="schedule_">
                                <div class="schedule_ctrl">
                                    <label class="switch">
                                    @if ($schedule->weekdayOff === 'yes')
                                    <input type="checkbox" id="weekdays" name="weekdayOff" value="yes" checked>
                                    @endif
                                    
                                    <span class="slider round"></span>
                                    </label>
                                    <h4 id="wkd">Turn off All Weekdays</h4>
                                </div>

                                <div class="schedule_ctrl">
                                    <label class="switch">
                                         @if ($schedule->weekendOff === 'yes')
                                        <input type="checkbox" id="weekends" name="weekendOff" value="yes" checked>
                                        @endif
                                        {{-- <input type="checkbox" id="weekends" name="weekendOff" value="yes"> --}}
                                        <span class="slider round"></span>
                                    </label>
                                    <h4 id="wke">Turn on All Weekends</h4>
                                </div>

                                <div class="schedule_ctrl">
                                    <label class="switch">
                                         @if ($schedule->holidayOff === 'yes')
                                        <input type="checkbox" id="weekends" name="holidayOff" value="yes" checked>
                                        @endif
                                    {{-- <input type="checkbox" id="holidays" name="holidayOff" value="yes"> --}}
                                    <span class="slider round"></span>
                                    </label>
                                    <h4 id="hol">Turn on All Holidays</h4>
                                </div>
                            </div>
                
                            <div class="gap"></div>

                            <div class="btn-holder">
                                <button class="pr-btn" id="save">Save</button>
                            </div>
                        </form>
                    
                        @else
                        <form action="{{ route('schedules') }}" method="POST">
                            @csrf
                            <div class="schedule_">
                            <div class="schedule_ctrl">
                                <label class="switch">
                                <input type="checkbox" id="weekdays" name="weekdayOff" value="yes">
                                <span class="slider round"></span>
                                </label>
                                <h4 id="wkd">Turn off All Weekdays</h4>
                            </div>

                            <div class="schedule_ctrl">
                                <label class="switch">
                                    <input type="checkbox" id="weekends" name="weekendOff" value="yes">
                                    <span class="slider round"></span>
                                </label>
                                <h4 id="wke">Turn on All Weekends</h4>
                            </div>

                            <div class="schedule_ctrl">
                                <label class="switch">
                                <input type="checkbox" id="holidays" name="holidayOff" value="yes">
                                <span class="slider round"></span>
                                </label>
                                <h4 id="hol">Turn on All Holidays</h4>
                            </div>
                        </div>
                
                        <div class="gap"></div>

                        <div class="btn-holder">
                            <button class="pr-btn" id="save">Save</button>
                        </div>
                    </form>

                @endif
                
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
    <script>
        document.querySelector('#weekdays').addEventListener('change', weekdays, false);
        document.querySelector('#weekends').addEventListener('change', weekends, false);
        document.querySelector('#holidays').addEventListener('change', holidays, false);

        function weekdays(e) {
            if (e.target.checked) {
            document.getElementById('wkd').innerHTML = "Turn off All Weekdays";
            } else {
            document.getElementById('wkd').innerHTML = "Turn on All Weekdays";
            }
        }

        function weekends(e) {
            if (e.target.checked) {
            document.getElementById('wke').innerHTML = "Turn off All Weekends";
            } else {
            document.getElementById('wke').innerHTML = "Turn on All Weekends";
            }
        }

        function holidays(e) {
            if (e.target.checked) {
            document.getElementById('hol').innerHTML = "Turn off All Holidays";
            } else {
            document.getElementById('hol').innerHTML = "Turn on All Holidays";
            }
        }
    </script>
@endsection