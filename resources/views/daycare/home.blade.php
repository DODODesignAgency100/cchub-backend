@extends('layouts.daycare.app')

{{-- @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif --}}

@section('content')
    <section class="hero">

        <img src="{{ asset('assets/img/hero-banner-provider.jpg')}}" alt="Happy Little girl">

        <div class="hero-desc-provider">

            <h1>Showcase your Daycare</h1>

            <form action="#" method="GET" class="search-form" role="form">

                <span class="formwrap">
                    <label for="zipcode">
                      <input type="text" name="zipcode" id="zipcode" class="hmzip" pattern="[0-9]{5}" placeholder="Type Zip code or city" required>
                    </label>
                    <label for="zipcode">
                      <input type="text" name="business-name" id="bizname" class="bizname" placeholder="Type Business Name" required>
                    </label>
                </span>

                <button type="submit" class="homesubmit">Register Daycare</button>

            </form>
        </div>
        
        <div class="anchor">
          <img src="{{ asset('assets/img/down-arrow-cchub.png')}}">
        </div>

    </section>

    <section class="features">

        <h2>Your partner in Child care</h2>

        <div class="partner-logo">

            <figure>
                <img src="{{ asset('assets/img/audience.svg')}}" alt="Wider reach to Your Audience">
                <figcaption>Wider reach to Your Audience</figcaption>
            </figure>

            <figure>
                <img src="{{ asset('assets/img/showcase.svg')}}" alt="Showcase your daycare">
                <figcaption>Showcase your daycare</figcaption>
            </figure>

          <figure>
            <img src="{{ asset('assets/img/Quick.svg')}}" alt="Quick and easy sign ons">
            <figcaption>Quick and easy sign ons</figcaption>
          </figure>
          
        </div>

    </section>
    
    <section class="howitworks">

        <h2>How it Works</h2>
        
        <div class="howitworks-main">
          
            <div class="howitworks-main_">
                <h3>1</h3>
                <h3>Register Daycare</h3>
                <p>Create a free account to get started on Childcare Hub.</p>
            </div>

            <div class="howitworks-main_">
                <h3>2</h3>
                <h3>Get Reviews</h3>
                <p>Get quality review and trade insights with an authentic community of parents.</p>
            </div>

            <div class="howitworks-main_">
                <h3>3</h3>
                <h3>Payment</h3>
                <p>Get paid using Childcare Hub.</p>
            </div>

        </div>

    </section>

    <section class="testi">

          <h2>What Parents are Saying</h2>

          <div class="testi-main">

              <div class="testi-main-card">
                  <img src="{{ asset('assets/img/testi1.jpg') }}" alt="testimonial">

                  <h3 class="test-card-name">Jessica Lane</h3>
                  <p class="testi-card-content">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do”</p>
                  <p class="testifier">Crescengo Daycare, Ontario</p>
              </div>

              <div class="testi-main-card">
                  <img src="{{ asset('assets/img/test2.jpg') }}" alt="testimonial">
                  <h3 class="test-card-name">Fikayo Obi</h3>
                  <p class="testi-card-content">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do”</p>
                  <p class="testifier">Comfort Daycare, Quebec</p>
              </div>

              <div class="testi-main-card">
                  <img src="{{ asset('assets/img/test3.jpg') }}" alt="testimonial">
                  <h3 class="test-card-name">Ladipo Seun</h3>
                  <p class="testi-card-content">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do”</p>
                  <p class="testifier">Bluecell Daycare, Ontario</p>
              </div>
            
          </div>
    </section>

    <section class="about">

        <div class="about-img">
            <img src="{{ asset('assets/img/about.jpg') }}" alt="About Child Care Hub">
        </div>
        <div class="about-content">
          
            <h2>about the Childcare Hub</h2>
            <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
            <br><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <a href="#" class="cc-btn">Find out more</a>
        </div>

    </section>
@endsection
