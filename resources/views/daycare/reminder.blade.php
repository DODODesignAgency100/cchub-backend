@extends('layouts.daycare.app')

@section('content')
    <section class="hero-pr">
        <img src="{{ asset('assets/img/breadcrum-providers-profile.jpg')}}" alt="Happy Little girl">
    </section>

    <section class="profile-nav">
        <nav class="profile-nav-main">
            <ul class="profile-menu">
                <li class="">
                    <a href="{{ route('daycare/dashboard') }}">Profile</a>
                </li>
                <li>
                    <a href="{{ route('daycare/schedule') }}">Schedule</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/tour') }}">Tour Bookings</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/waitlist') }}">Waitlist</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reservations') }}">Reservations</a>
                </li>
                <li class="">
                    <a href="#">Messages</a>
                </li>
                <li class="">
                    <a href="{{ route('daycare/reviews') }}">Reviews</a>
                </li>
                <li class="">
                    <a href="{{ route('analytics') }}">Analytics</a>
                </li>
                <li class="active">
                    <a href="{{ route('daycare/reminder') }}">Reminder</a>
                </li>
            </ul>
        </nav>
    </section>

    <section class="profile-main">
        @include('includes.messages')

        @isset($reservations)
            <div class="reminder">
                <div class="reminder-col" style="display:none;">
                    <h2>ID</h2>
                    @foreach ($reservations as $reservation)
                        <div class="data">
                            <p> {{ $reservation->id }} </p>
                        </div>
                    @endforeach
                </div>

                <div class="reminder-col">
                    <h2>Name</h2>
                    @foreach ($reservations as $reservation)
                        <div class="data">
                            <p> {{ $reservation->parent_name }} </p>
                        </div>
                    @endforeach
                </div>
        
                <div class="reminder-col">
                    <h2>Email Address</h2>
                    @foreach ($reservations as $reservation)
                        <div class="data">
                            <p> {{ $reservation->user->email }} </p>
                        </div>
                    @endforeach
                </div>
        
                <div class="reminder-col">
                    <h2>Phone Number</h2>
                    @foreach ($reservations as $reservation)
                        <div class="data">
                            <p>{{ $reservation->user->phone_number }}</p>
                        </div>
                    @endforeach
                </div>
        
                <div class="reminder-col">
                    <h2>Date Joined</h2>
                    @foreach ($reservations as $reservation)
                        <div class="data">
                            <p>{{ date_format(new DateTime($reservation->updated_at),'D F j, Y') }}</p>
                        </div>
                    @endforeach
                </div>
        
                {{-- <div class="reminder-col">
                    <h2>Due Date</h2>
                    @foreach ($reservations as $reservation)
                        <div class="data">
                            <p>{{ date_format(new DateTime($reservation->updated_at->addDays(30)),'D F j, Y') }}</p>
                        </div>
                    @endforeach
                </div> --}}
        
                <div class="reminder-col">
                    <h2>Action</h2>
                    @foreach ($reservations as $reservation)
                        <div class="data">
                            <button type="submit" data-target="button-{{ $reservation->id ?? '' }}" class="accept re-modal">Send Reminder</button>
                        </div>
                    @endforeach
                    
                </div>
        
            </div>
            @else
            <div class="reminder-col">
                <h2>No reminder list</h2>
            </div>
        @endisset

    </section>

    <div class="reminder-modal" id="button-{{ $reservation->id ?? '' }}">
        <div class="reminder-form">
            <div class="close">
                <img src="{{ asset('assets/img/close.svg') }}" alt="close" id="close">
            </div>

            <form action="{{ route('reminder', $reservation->id ?? '') }}" method="POST">
                @csrf
                <input type="hidden" name="user_id" value="{{ $reservation->user->id  ?? ''}}">
                {{-- <input type="hidden" name="daycare_id" value="{{ auth()->id }}"> --}}
                <div class="form-group">
                    <label for="parent_name">Parent's Name</label>
                    <input type="text" name="parent_name" id="parentname" class="form-control" value="{{ $reservation->parent_name ?? '' }}">
                </div>
    
                <div class="form-group">
                    <label for="reminder-email">Parent's Email</label>
                    <input type="email" name="parent_email" id="reminder-email" class="form-control" value="{{ $reservation->user->email ?? '' }}" required>
                </div>

                <div class="form-group">
                    <label for="reminder-amount">Amount due</label>
                    <input type="number" name="price" id="reminder-amount" class="form-control" value="" required>
                </div>
    
                <div class="form-group-ref">
                    <label for="message">Message</label>
                    
                    <textarea name="message" cols="0" rows="10" id="reminder-message" class="form-control reminder-message">Hi [Parent Name], this is top remind you that your payment is due on 31 of August, 2020.</textarea>
                </div>
    
                <div class="btn-holder">
                    <button class="pr-btn" id="save">Send</button>
                </div>
            </form>
        </div>
    </div>

@endsection
@section('footer')
    <script>
        const buttons = document.querySelectorAll('.re-modal');
        const modal = document.querySelector('#button-{{ $reservation->id ?? '' }}');
        const close = document.querySelector('#close');

        buttons.forEach(button => {
            button.addEventListener('click', function(){
                modal.style.display = "block";
            });

            close.addEventListener('click', function(){
                modal.style.display = "none";
            });

            document.addEventListener('click', function(e){
                if (e.target === modal) {
                    modal.style.display = "none";
                }
            
            });
        });
    </script>
@endsection