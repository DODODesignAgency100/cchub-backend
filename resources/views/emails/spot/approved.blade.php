@component('mail::message')
Hello {{ $userName }}

<h4>We are glad to inform you that 
    your spot reservation has been approved at {{ $daycareName }}.
    Therefore your kid has been registered with us</h4>

Kindly contact us if you're no longer interested <br>

Thanks,<br>
Childcare Hub
@endcomponent
