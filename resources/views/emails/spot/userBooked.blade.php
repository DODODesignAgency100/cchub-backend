@component('mail::message')
Hello, 

You have booked a spot at {{ $daycareName }}. <br> 
You'll receive an email from the daycare provider if your spot was accepted. <br>

Kindly contact us if you're no longer interested <br>

Thanks,<br>
Childcare Hub
@endcomponent
