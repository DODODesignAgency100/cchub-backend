@component('mail::message')
Hello, 

{{ $userName }} just booked a spot at your daycare center. <br> 
Follow the link below to accept or reject
<br>
<br>
<a href="http://127.0.0.1:8000/daycare/reservation">http://127.0.0.1:8000/daycare/reservation</a> <br>
<br>
Thanks,<br>
Childcare Hub
@endcomponent