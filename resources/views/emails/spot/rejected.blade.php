@component('mail::message')
Hello {{ $userName }}

Your reservation was rejected by {{ $daycareName }}

Thanks,<br>
{{ config('Childcare Hub') }}
@endcomponent
