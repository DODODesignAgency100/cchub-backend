@component('mail::message')
Hello, 

You have scheduled a tour with {{ $daycareName }}. <br> 
You'll receive an email from the daycare provider if your tour was accepted. <br>

Kindly contact us if you're no longer interested <br>

Thanks,<br>
Childcare Hub
@endcomponent
