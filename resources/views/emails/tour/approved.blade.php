@component('mail::message')
Hello {{ $userName }}

<h4>We are glad to inform you that 
    your tour has been approved {{ $daycareName }}</h4>

Thanks,<br>
{{ config('Childcare Hub') }}
@endcomponent
