@component('mail::message')
Hello, 

{{ $userName }} just scheduled a tour at your daycare center. <br> 
Follow the link below to accept or reject
<br>
<br>
<a href="http://127.0.0.1:8000/daycare/tour">http://127.0.0.1:8000/daycare/tour</a> <br>
<br>
Thanks,<br>
Childcare Hub
@endcomponent