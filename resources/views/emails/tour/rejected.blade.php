@component('mail::message')
Hello {{ $userName }}

{{ $daycareName }} has rejected your tour. <br>
Please reschedule for a later date as {{ $daycareName }} <br>
will not be available.

Thanks,<br>
{{ config('Childcare Hub') }}
@endcomponent
