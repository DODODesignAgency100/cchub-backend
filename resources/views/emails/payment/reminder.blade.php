@component('mail::message')
Hello {{ $userName }}
<h4>This is a reminder from {{ $daycareName }}.</h4>
{{ $message }} <br>
<h4> Amount Due: {{ $amountDue }}<h4>

@php
    $url = route('parent-reminder', $remindId) 
@endphp

@component('mail::button', ['url' => $url])
View to Make Payment
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
