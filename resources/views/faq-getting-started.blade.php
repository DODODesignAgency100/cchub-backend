@extends('layouts.parents.app')

@section('content')

    <section class="hero-pr">
        <img src="{{ asset('assets/img/getting-started-parents.jpg') }}" alt="Getting Started">
      </section>

    <section class="pro-resource">

        <h2>Parents</h2>

        <div class="pro-resources">
            <nav class="pro-resources_nav">
                <ul>
                    <li><a href="{{ route('resources') }}" class="p-active">Getting Started</a></li>
                    <li><a href="{{ route('daycare-search') }}">Searching for daycare</a></li>
                    <li><a href="{{ route('daycare-connect') }}">Connecting with daycare</a></li>
                </ul>
            </nav>

            <div class="pro-resources_main gap">
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/types-of-hub.jpg') }}" alt="Type of Hub">
                    <figcaption>What Types of Child Care Services Are Listed on Childcare Hub?</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/daycare-licensed.jpg') }}" alt="daycare licensed">
                    <figcaption>How Can I Tell If a Daycare or Is Licensed?</figcaption>
                </figure>

            </div>
        </div>
        
    </section>
@endsection