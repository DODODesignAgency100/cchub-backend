@extends('layouts.parents.app')

@section('content')

    <section class="hero-pr">
        <img src="{{ asset('assets/img/getting-started-parents.jpg') }}" alt="Getting Started">
    </section>

    <section class="pro-resource">
        <h2>Parents</h2>
        

        <div class="pro-resources">
            <nav class="pro-resources_nav">
                <ul>
                    <li><a href="{{ route('resources') }}">Getting Started</a></li>
                    <li><a href="{{ route('daycare-search') }}" class="p-active">Searching for daycare</a></li>
                    <li><a href="{{ route('daycare-connect') }}">Connecting with daycare</a></li>
                </ul>
            </nav>

            <div class="pro-resources_main gap">
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/preschool-parent.jpg')}}" alt="Find Preschool">
                    <figcaption>How Do I Find a Preschool?</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/find-daycare.jpg') }}" alt="findcare daycare">
                    <figcaption>How Do I Find a Daycare?</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/open-space-parent.jpg') }}" alt="open space">
                    <figcaption>How Do I See Programs With Open Spaces?</figcaption>
                </figure>
    
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/page-management.jpg') }}" alt="open space">
                    <figcaption>How Do I See Programs With Open Spaces?</figcaption>
                </figure>

            </div>
        </div>

    </section>

@endsection