@extends('layouts.parents.app')

@section('content')

    <section class="hero-pr">
        <img src="{{ asset('assets/img/getting-started-parents.jpg') }}" alt="Getting Started">
    </section>

    <section class="pro-resource">
        
        <h2>Parents</h2>
    
        <div class="pro-resources">
            <nav class="pro-resources_nav">
                <ul>
                    <li><a href="{{ route('resources') }}">Getting Started</a></li>
                    <li><a href="{{ route('daycare-search') }}">Searching for daycare</a></li>
                    <li><a href="{{ route('daycare-connect') }}" class="p-active">Connecting with daycare</a></li>
                </ul>
            </nav>

            <div class="pro-resources_main gap">
                <figure class="pro-res">
                    <img src="{{ asset('assets/img/what-to-do.jpg') }}" alt="What to do">
                    <figcaption>What Do I Do After I’ve Found a Child Care Provider I’m Interested In?</figcaption>
                </figure>

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/trouble-contacting.jpg') }}" alt="trouble contacting">
                    <figcaption>I’m Having Trouble Contacting a Provider. Help!</figcaption>
                </figure>

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/recommedation.jpg') }}" alt="recommedations">
                    <figcaption>How Do I Write a Recommendation for a Provider?</figcaption>
                </figure>

                <figure class="pro-res">
                    <img src="{{ asset('assets/img/update-info.jpg') }}" alt="Update info">
                    <figcaption>How Do I Report Inaccurate or Out-Of-Date Information About a Provider?</figcaption>
                </figure>

            </div>
        </div>

    </section>

@endsection