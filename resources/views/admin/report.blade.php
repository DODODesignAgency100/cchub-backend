@extends('layouts.admin.app')

@section('content')

    {{-- <div id="report-alert">
        <p>You have 2 new Reports</p>
    </div> --}}
      
    <section id="report">
  
        <div class="report-table">

            @if(!$reports->isEmpty())
  
                <h2 id="report-heading">
                    Reports
                </h2>
                
                <table id="tabular-report">
                    <thead>
                        <tr class="sep">
                            <th class="tb-s">Name</th>
                            <th class="tb-m">Feedback</th>
                            <th class="tb-s">Date</th>
                            <th class="tb-s">Time</th>
                        </tr>
                    </thead>
        
                    <tbody class="sep">

                        @foreach ($reports as $item)
                            <tr class="recent-report">
                                <td>{{ $item->user->first_name }}</td>
                                <td>{{ $item->comments }}</td>
                                <td>{{ date_format(new DateTime($item->created_at),'l F j, Y') }}</td>
                                <td>{{ date_format(new DateTime($item->created_at),'g:ia') }}</td>
                            </tr>
                        @endforeach

                    </tbody>
                </table>

                @else 

                     <h2 id="report-heading">
                        No Reports Yet
                    </h2>
            
            @endif
  
        </div>
  
    </section>
  
    <div class="gap"></div>
    
@endsection