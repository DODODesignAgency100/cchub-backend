<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title> {{ config('app.name', '| Child Care Hub') }} </title>

        <meta name="description" content="Find the right care for your child">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
    </head>
 
    <body>

        <main>
            <nav class="nav-bar" aria-labelledby="header menu">

                <div class="main-nav">
                    <div class="logo-pr">
                        <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                    </div>

                    <div class="prreg_notifier">
                        <p>Don't have an account? 
                            <a href="{{ route('admin-register') }}" class="notifier">Sign Up</a>
                        </p>
                    </div>
                </div>
            </nav> 
            
            <section class="prreg-main">

                <div class="prreg-main-img">
                    <img src="{{ asset('assets/img/covering-mouth.jpg') }}" alt="Girl covering mouth">
                </div>

                <div class="prreg-main-form">

                    <h1>Log in</h1>

                    @include('includes.messages')

                    <form action="{{ route('admin.login') }}" method="POST" class="regform">
                        @csrf
                        <div class="form-group" id="si"> 
                            <label for="Email">Email</label>
                            <input type="email" name="email" id="Email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group" id="si">
                            <label for="Password">Password</label>
                            <input type="password" name="password" id="Password" class="form-control">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        
                        <div class="lg-action">
                            <div class="form-check form-group" id="si">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
    
                                <label class="form-check-label" for="remember">
                                    {{ __('Remember Me') }}
                                </label>
                            </div>
    
                            <div class="form-forgot">
                                @if (Route::has('password.request'))
                                    <a href="{{ route('daycare.password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>

                        <button type="submit" class="pr-submit">{{ __('Login') }}</button> 

                    </form>

                </div>

            </section>

        </main>

    </body>

</html>