@extends('layouts.admin.app')

@section('content')

    <div class="admin-intro">

        <div class="admin-intro--hd">
            <h3>Various Daycare Centers</h3>
        </div>
  
        <div class="daycare-country">
            <div class="country active">
                <p>
                    <img src="{{ asset('admin/asset/img/canada.png') }}" alt="Canada">Canada
                </p>
            </div>
        </div>
  
    </div>
      
    <section class="parent">

        <div class="provider-quick">
            <div id="pr-heading">
                <h3>Number of daycare centers per province on the platform</h3>      
            </div>

            <span class="sort">
                Sort by
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7 10L12 15L17 10H7Z" fill="#333333"/>
                </svg>            
            </span>
        </div>
      
        <div class="pr-analytics">

            <div class="chart">
                <canvas id="prChart" width="900" height="500"></canvas>
            </div>
      
            <div class="sort-list none">
        
                <div class="month">
                    <h4>Month</h4>
                    <a href="#">January</a>
                    <a href="#">February</a>
                    <a href="#">March</a>
                    <a href="#">April</a>
                    <a href="#">May</a>
                    <a href="#">June</a>
                    <a href="#">July</a>
                    <a href="#">August</a>
                    <a href="#">September</a>
                    <a href="#">October</a>
                    <a href="#">November</a>
                    <a href="#">December</a>
                </div>
        
                <div class="year">
                    <h4>Year</h4>
                    <a href="#">2020</a>
                </div>
        
                <div class="quarterly">
                    <h4>Querterly</h4>
                    <a href="#">(Q1) January, February, and March</a>
                    <a href="#">(Q2) April, May, and June</a>
                    <a href="#">(Q3) July, August, and September</a>
                    <a href="#">(Q4) October, November, and December</a>
                </div>
            </div>
        </div>
      
        <div class="gap"></div>
                       
    </section>
      
    <section class="parent">
      
        <div class="provider-quick">
            <div id="pr-select" class="form-control">

                <span class="arr">Longest-serving daycare center</span>
    
                <ul class="pr-dropdown none">
                    <li>Most patronized daycare center</li>
                    <li>Least patronized daycare center</li>
                    <li>Attrition rate per period</li>
                    <li>Number of of visits per daycare</li>
                    <li>Number of successful daycare</li>
                    <li>Number of unsuccessful daycare</li>
                    <li>Number of recurring reservations</li>
                </ul> 
            </div>
            <span class="sort">
                Sort by
        
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M7 10L12 15L17 10H7Z" fill="#333333"/>
                </svg>            
            </span>
        </div>
      
        <div class="pr-analytics">
            <div class="chart">
                <canvas id="histo" width="900" height="500"></canvas>
            </div>
        </div>
      
        <div class="sort-list none">
    
            <div class="month">
                <h4>Month</h4>
                <a href="#">January</a>
                <a href="#">February</a>
                <a href="#">March</a>
                <a href="#">April</a>
                <a href="#">May</a>
                <a href="#">June</a>
                <a href="#">July</a>
                <a href="#">August</a>
                <a href="#">September</a>
                <a href="#">October</a>
                <a href="#">November</a>
                <a href="#">December</a>
            </div>
    
            <div class="year">
                <h4>Year</h4>
                <a href="#">2020</a>
            </div>
    
            <div class="quarterly">
                <h4>Querterly</h4>
                <a href="#">(Q1) January, February, and March</a>
                <a href="#">(Q2) April, May, and June</a>
                <a href="#">(Q3) July, August, and September</a>
                <a href="#">(Q4) October, November, and December</a>
            </div>
        </div>
    
        <div class="gap"></div>      
    </section>

@endsection
@section('footer')
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script>
        var ctx = document.getElementById('prChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['British Columbia', 'Alberta', 'Saskatchewan', 'Manitoba', 'Ontario', 'Quebec', 'New Brunswick', 'Nova Scotia', 'Prince Edward Island', 'Newfoundland and Labrador', 'Yukon', 'Northwest Territories', 'Nunavut'],
                datasets: [{
                    label: '# of Visits',
                    data: [12, 19, 3, 5, 2, 3, 5, 3, 5, 2, 3, 10, 5],
                    backgroundColor: [
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF'
                    ],
                    borderColor: [
                    '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF',
                        '#02A2AF'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
              legend: {
                display: false
              }
            }
        });
      </script>
    
    <script>
      var histo = document.getElementById('histo').getContext('2d');
      var myChart = new Chart(histo, {
          type: 'horizontalBar',
          data: {
              labels: ['British Columbia', 'Alberta', 'Saskatchewan', 'Manitoba', 'Ontario', 'Quebec', 'New Brunswick', 'Nova Scotia', 'Prince Edward Island', 'Newfoundland and Labrador', 'Yukon', 'Northwest Territories', 'Nunavut'],
              datasets: [{
                  label: '# of Visits',
                  data: [12, 19, 3, 5, 2, 3, 5, 3, 5, 2, 3, 10, 5],
                  backgroundColor: [
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF'
                  ],
                  borderColor: [
                  '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF',
                      '#02A2AF'
                  ],
                  borderWidth: 1
              }]
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              },
            legend: {
              display: false
            }
          }
      });
    </script>
@endsection