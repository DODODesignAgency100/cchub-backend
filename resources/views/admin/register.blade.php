<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title> {{ config('app.name', '| Child Care Hub') }} </title>

        <meta name="description" content="Find the right care for your child">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
    </head>
 
    <body>

        <main>

            <nav class="nav-bar" aria-labelledby="header menu">

                <div class="main-nav">
                    <div class="logo-pr">
                        <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                    </div>

                    <div class="prreg_notifier">
                        <p>Already have an account? 
                            <a href="{{ route('admin-login') }}" class="notifier">Log In</a>
                        </p>
                    </div>
                </div>
            </nav>       

            <section class="prreg-main">

                <div class="prreg-main-img">
                    <img src="{{ asset('assets/img/covering-mouth.jpg')}}" alt=" Girl covering mouth">
                </div>

                <div class="prreg-main-form">

                    <h1>Sign Up</h1>

                    @include('includes.messages')

                    <form action="{{ route('admin.register') }}" method="POST" class="regform">
                        @csrf

                        <div class="form-group">
                            <label for="pr0fname">First name</label>
                            <input type="text" name="first_name" id="pr-fname" class="form-control" required value="{{ old('first_name') }}" autocomplete="">
                            @error('first_name')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Last Name">Last name</label>
                            <input type="text" name="last_name" id="pr-lname" class="form-control" required value="{{ old('last_name') }}" autocomplete="">
                            @error('last_name')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Phone Number">Phone Number</label>
                                <input type="tel" name="phone_number" id="pr-tel" class="form-control" value="{{ old('phone_number') }}" autocomplete="">
                            </label>
                            @error('phone_number')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Email">Email</label>
                            <input type="email" name="email" id="pr-email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" autocomplete="email">
                            @error('email')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Password">Password</label>
                            <input type="password" name="password" id="pr-pass" class="form-control">
                            @error('password')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Confirm Password">Confirm Password</label>
                            <input type="password" name="password_confirmation" id="pr-pass" class="form-control">
                        </div>

                        <button type="submit" class="pr-submit">Sign up</button> 

                    </form>

                </div>

            </section>

        </main>

    </body>

</html>