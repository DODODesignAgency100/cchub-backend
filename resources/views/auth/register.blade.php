<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">

        <title> {{ config('app.name', '| Child Care Hub') }} </title>

        <meta name="description" content="Find the right care for your child">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700;800&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/res.css') }}">
        <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('assets/favicon/apple-icon-57x57.png') }}">
        <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('assets/favicon/apple-icon-60x60.png') }}">
        <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('assets/favicon/apple-icon-72x72.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/favicon/apple-icon-76x76.png') }}">
        <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('assets/favicon/apple-icon-114x114.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('assets/favicon/apple-icon-120x120.png')}}">
        <link rel="apple-touch-icon" sizes="144x144" href="{{ asset('assets/favicon/apple-icon-144x144.png')}}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('assets/favicon/apple-icon-152x152.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/favicon/apple-icon-180x180.png')}}">
        <link rel="icon" type="image/png" sizes="192x192"  href="{{ asset('assets/favicon/android-icon-192x192.png')}}">
        <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/favicon/favicon-32x32.png')}}">
        <link rel="icon" type="image/png" sizes="96x96" href="{{ asset('assets/favicon/favicon-96x96.png')}}">
        <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('asset/favicon/favicon-16x16.png')}}">
        <link rel="manifest" href="{{ asset('assets/favicon/manifest.json')}}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="{{ asset('assets/favicon/ms-icon-144x144.png')}}">
        <meta name="theme-color" content="#ffffff">
    </head>
 
    <body>

        <main>

            <nav class="nav-bar" aria-labelledby="header menu">

                <div class="main-nav">
                    <div class="logo-pr">
                        <a href="{{ route('parent-home') }}">
                            <img src="{{ asset('assets/img/cchublogo.png') }}" alt="Child Care Hub Logo">
                        </a>
                    </div>

                    <div class="prreg_notifier">
                        <p>Already have an account? 
                            <a href="{{ route('login') }}" class="notifier">Log In</a>
                        </p>
                    </div>
                </div>
            </nav>       

            <section class="prreg-main">

                <div class="prreg-main-img">
                    <img src="{{ asset('assets/img/providers-signin.jpeg')}}" alt="Signin banner for login page CCHUB">
                </div>

                <div class="prreg-main-form">

                    <h1>Sign Up</h1>

                    {{-- @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif --}}

                    <form action="{{ route('register') }}" method="POST" class="regform">
                        @csrf

                        <div class="form-group">
                            <label for=pr0fname">First name <span class="req"> (Required)</span></label>
                            <input type="text" name="first_name" id="pr-fname" class="form-control" required value="{{ old('first_name') }}" autocomplete="">
                            @error('first_name')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Last Name">Last name <span class="req"> (Required)</span></label>
                            <input type="text" name="last_name" id="pr-lname" class="form-control" required value="{{ old('last_name') }}" autocomplete="">
                            @error('last_name')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Address">Address <span class="req"> (Required)</span></label>
                            <input type="text" name="address" id="pr-add" class="form-control" value="{{ old('address1') }}" autocomplete="">
                            @error('address')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="City">City <span class="req"> (Required)</span></label>
                            <input type="text" name="city" id="pr-city" class="form-control" value="{{ old('city') }}" autocomplete="">
                            @error('city')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Province">Province <span class="req"> (Required)</span></label>
                            <input type="text" name="province" id="pr-province" class="form-control" value="{{ old('province') }}" autocomplete="">
                            @error('province')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Province">Zip Code <span class="req"> (Required)</span></label>
                            <input type="text" name="zip" id="pr-zip" maxlength="6" class="form-control" value="{{ old('zip') }}" autocomplete="">
                            @error('zip')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Phone Number">Phone Number <span class="req"> (Required)</span></label>
                                <input type="tel" name="phone_number" id="pr-tel" class="form-control" value="{{ old('phone_number') }}" autocomplete="">
                            </label>
                            @error('phone_number')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Email">Email <span class="req"> (Required)</span></label>
                            <input type="email" name="email" id="pr-email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" autocomplete="email">
                            @error('email')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Password">Password <span class="req"> (Required)</span></label>
                            <input type="password" name="password" id="pr-pass" class="form-control">
                            @error('password')
                                <span class="err-text" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="Confirm Password">Confirm Password <span class="req"> (Required)</span></label>
                            <input type="password" name="password_confirmation" id="pr-pass" class="form-control">
                        </div>

                        <div class="form-label full">
                            <input type="checkbox" name="terms" id="checkbox" value="Agree">
                            <label for="checkbox" id="checktext">I have read and agree to the <a href="{{ route('terms') }}">terms of service</a> and <a href="{{ route('privacy')}}">privacy policy</a></label>
                        </div>

                        <button type="submit" class="pr-submit">Sign up</button> 

                        {{-- <div class="other-signup">
                            <span class="te">or with</span>
                            <a href="#">
                                <img src="{{ asset('assets/img/google.svg') }}" alt="Google">
                            </a>
                            <a href="#">
                                <img src="{{ asset('assets/img/facebookl.svg') }}" alt="Facebook">
                            </a>
                        </div> --}}

                    </form>

                </div>

            </section>

        </main>

    </body>

</html>















{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}
