@extends('layouts.parents.app')

@section('content')
<section class="hero">
    <img src="{{ asset('assets/img/CCHUBbanner.png') }}" alt="Happy Little girl">   
    <div class="hero-desc">

        <h1>Find the right care for your child</h1>

        <form action="/search" method="GET" class="search-form" role="form">

            <label for="search form">
                <input type="search" required name="search" id="search" class="homesearch" placeholder="Type Zip code or city">
            </label>

            <button type="submit" class="homesubmit">Find Daycare</button>

        </form>
    </div>

</section>

<section class="features">

    <h2>The Features you Really care about</h2>

    <div class="feature-logo">

        <figure>
            <img src="{{ asset('assets/img/govt.svg') }}" alt="Government certified">
            <figcaption>Government certified</figcaption>
        </figure>

        <figure>
            <img src="{{ asset('assets/img/bg.svg') }}" alt="Background checks">
            <figcaption>Background checks</figcaption>
        </figure>

        <figure>
            <img src="{{ asset('assets/img/caregiver.svg') }}" alt="Caregiver to Child ratio">
            <figcaption>Caregiver to Child ratio</figcaption>
        </figure>
          
        <figure>
            <img src="{{ asset('assets/img/idverification.svg') }}" alt="Identity Verification">
            <figcaption>Identity Verification</figcaption>
        </figure>
          
          <figure>
            <img src="{{ asset('assets/img/clean.svg') }}" alt="Clean and Hygenic">
            <figcaption>Clean and Hygenic</figcaption>
          </figure>
          
          <figure>
            <img src="{{ asset('assets/img/safety.svg') }}" alt="Safety Assurance">
            <figcaption>Safety Assurance</figcaption>
          </figure>
        </div>
</section>
        
<section class="howitworks">

    <h2>How it Works</h2>
        
    <div class="howitworks-main">
        <div class="howitworks-main_">
            <h3>1</h3>
            <h3>Search for Daycare</h3>
            <p>Childcare Hub is a website aimed at making daycare search more efficient for parents.</p>
        </div>

        <div class="howitworks-main_">
            <h3>2</h3>
            <h3>Select Daycare</h3>
            <p>Childcare Hub presents you with most important factors Parent consider while finilizing a day care for their kids such as Staffs, Facilities and Program structure.</p>
        </div>

        <div class="howitworks-main_">
            <h3>3</h3>
            <h3>Enlist in Daycare Center</h3>
            <p>Browse through the list of Daycares in your center and entlist your child.</p>
        </div>
    </div>

</section>

<section class="daycares">

    <h2>Featured Daycares</h2>

    <div class="daycare-display">

    @if($daycares != '')

      @foreach ($daycares as $item)
        <div class="daycares-card">

            <img src="{{ asset('assets/img/comfort.jpg')}}" alt="Daycare" class="daycare-centre">

            <div class="daycare-detail">
                <h3 class="daycare-name">{{ $item->business_name ?? ''}}</h3>
                <h3 class="daycare-city">{{ $item->city ?? ''}}</h3>
                <p>{{ $item->description()->overview ?? ''}}</p>
                {{-- <div class="ratings">Reviews
                    <img src="{{ asset('assets/img/ratings.svg') }}" alt="ratings">
                </div> --}}
            </div>
        </div>
      @endforeach
    @endif

    </div>
    
</section>

<section class="testi">

        <h2>What Parents are Saying</h2>

        <div class="testi-main">
          <div class="testi-main-card">
            <img src="assets/img/testi1.jpg" alt="testimonial">
            <h3 class="test-card-name">Jessica Lane</h3>
            <p class="testi-card-content">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do”</p>
            <p class="testifier">Crescengo Daycare, Ontario</p>
          </div>

          <div class="testi-main-card">
            <img src="assets/img/test2.jpg" alt="testimonial">
            <h3 class="test-card-name">Fikayo Obi</h3>
            <p class="testi-card-content">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do”</p>
            <p class="testifier">Comfort Daycare, Quebec</p>
          </div>

          <div class="testi-main-card">
            <img src="assets/img/test3.jpg" alt="testimonial">
            <h3 class="test-card-name">Ladipo Seun</h3>
            <p class="testi-card-content">“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do”</p>
            <p class="testifier">Bluecell Daycare, Ontario</p>
          </div>
        </div>
      </section>

<section class="about">

    <div class="about-img">
        <img src="{{ asset('assets/img/about-us.jpeg') }}" alt="About Child Care Hub">
    </div>

    <div class="about-content">
        <h2>About Childcare Hub</h2>
        <p>Childcare Hub Inc. is a Canadian online daycare listing company based in Saskatchewan, Canada. We provide a dynamic online platform that advertises daycare centers and their childcare availabilities. Parents can search, compare, and reserve spots based on their preferences.</p>
        <a href="{{ route('about') }}" class="cc-btn">Find out more</a>
    </div>
</section>
@endsection
