@extends('layouts.parents.app')

@section('head')
    <!-- Begin Mailchimp Signup Form -->
    <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
    <style type="text/css">
        #mc_embed_signup{}
        .cform {
            display: flex;
            width: 90%;
            margin: auto;
            flex-wrap: wrap;
            padding: 50px 0;
        }
        .cf-message {
            width: 95%;
            padding: 14px 15px;
            margin: 10px 0 20px;
            background-color: rgba(244, 244, 244, 0.35);
            border: 1px solid rgba(196, 196, 196, 0.2);
        }
        #mc_embed_signup_scroll {
            display: flex;
            width: 100%;
            margin: auto;
            flex-wrap: wrap;
            padding: 50px 0;
        }
        #mc_embed_signup input {
            border: 1px solid rgba(196, 196, 196, 0.2);
            border-radius: 0;
        }
    </style>
@endsection

@section('content')
    <section class="hero-pr">
        <img src="assets/img/contact-breadcrum.jpg" alt="Do not Sell Breadcrum">
    </section>

    <section class="contact-main">
        <div class="nav-breadcrum">
            <div>
                <ul class="breadcrumb">
                    <li>
                        <a href="{{ route('parent-home') }}">Home</a>
                    </li>
                    <li>Contact Us</li>
                </ul>
            </div>
        </div>

        <!-- Begin Mailchimp Signup Form -->
        <div id="mc_embed_signup" class="contact-form">
            <form action="https://childcarehub.us10.list-manage.com/subscribe/post?u=dfbe0cd3f2b6f4647a4c0d509&amp;id=584af0942c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate cform" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
                    {{-- <h2>Subscribe</h2> --}}
                    {{-- <div class="indicates-required"><span class="asterisk">*</span> indicates required</div> --}}
                    <div class="contact-control">
                        <label for="mce-EMAIL">Email Address *
                        </label>
                        <input type="email" value="" name="EMAIL" class="required email form-control" id="mce-EMAIL">
                    </div>
                    <div class="contact-control">
                        <label for="mce-NAME">Name *
                        </label>
                        <input type="text" value="" name="NAME" class="required form-control" id="mce-NAME">
                    </div>
                    <div id="contact-control-message">
                        <label for="mce-MESSAGE">Message </label>
                        {{-- <input type="text" value="" name="MESSAGE" class="" id="mce-MESSAGE"> --}}
                        <textarea name="MESSAGE" class="cf-message" id="mce-MESSAGE" cols="30" rows="10"></textarea>
                    </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true">
                        <input type="text" name="b_dfbe0cd3f2b6f4647a4c0d509_584af0942c" tabindex="-1" value="">
                    </div>
                    {{-- 
                    <div class="clear">
                        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                    </div> --}}
                    <div id="cf-btn">
                        <button type="submit" class="pr-submit" id="form-submit">Send</button> 
                    </div>
                </div>
            </form>
        </div>
        <!--End mc_embed_signup-->
{{-- 
        <div class="contact-form">
            <form action="" id="cform">
                <div class="contact-control">
                    <label for="name">Name</label>
                    <input type="text" id="cf-name" name="cf-name" class="form-control">
                </div>

                <div class="contact-control">
                    <label for="email">Email</label>
                    <input type="email" id="cf-email" name="cf-email" class="form-control">
                </div>

                <div id="contact-control-message">
                    <label for="message">Message</label>
                    <textarea name="contactform-message" class="cf-message" cols="30" rows="10"></textarea>
                </div>

                <div id="cf-btn">
                    <button type="submit" class="pr-submit" id="form-submit">Send</button> 
                </div>
            </form>
        </div> --}}
    </section>
@endsection
@section('footer')
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='NAME';ftypes[1]='text';fnames[2]='MESSAGE';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->
@endsection