@extends('layouts.parents.app')


@section('content')

    <section class="waitlist-hero">
        <img src="{{ asset('assets/img/waitlist-breadcrum.jpg') }}" alt="breadcrumb">
    </section>

    <section class="waitlist-breadcrum">

        <div>
            <ul class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="{{ route('search') }}">Search Page</a>
                </li>
                <li>Daycare View Page</li>
            </ul>
        </div>

        @include('includes.messages')

    </section>

    <section class="daycare">

        <div class="daycare-pictoral">

            @php
                $images = json_decode($post->daycare_photo);
                // dd($images);
            @endphp

            @if ($post->daycare_photo == '')

                <picture>
                    <source src="{{ asset('daycare/no-image.webp') }} 1x, {{ asset('daycare/no-image.webp') }} 2x" type="image/webp">
                    <img src="{{ asset('daycare/default-image.jpg') }}" alt="daycare no image" style="width:100%;">
                </picture>

                @else 
                    
                <img src="{{ asset('daycare/'. $post->daycare_photo) }}" alt="Daycare center">
                
            @endif

            <div class="dayowner">
                @if ($post->director_image == '')

                    <picture>
                        <source src="{{ asset('daycare/no-image.webp') }} 1x, {{ asset('daycare/no-image.webp') }} 2x" type="image/webp">
                        <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                    </picture>

                    @else 

                    <img src="{{ asset('daycare/'.$post->director_image) }}" alt="Daycare center owner">
                    
                @endif
                <h3>{{ $post->business_name }}</h3>
            </div>

            {{-- <div class="daycare-virtual">
                <h4>virtual tour</h4>
                <img src="{{ asset('assets/img/daycare-virtual-tour.jpg')}}" alt="Virtual tour">
                <a href="daycare-virtual.html">
                <h3>
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12 4.5C7 4.5 2.73 7.61 1 12C2.73 16.39 7 19.5 12 19.5C17 19.5 21.27 16.39 23 12C21.27 7.61 17 4.5 12 4.5ZM12 17C9.24 17 7 14.76 7 12C7 9.24 9.24 7 12 7C14.76 7 17 9.24 17 12C17 14.76 14.76 17 12 17ZM12 9C10.34 9 9 10.34 9 12C9 13.66 10.34 15 12 15C13.66 15 15 13.66 15 12C15 10.34 13.66 9 12 9Z" fill="#333333"/>
                    </svg>
                    Take Virtual Tour
                </h3>
                </a>
            </div> --}}
            </div>

            <div class="daycare-info">

                <h2>{{ $post->business_name ?? ''}}</h2>
                <p>
                    {{ $post->description()->overview ?? '' }}
                </p>

                <div class="dy-cta">
                    <div class="dy-cta-col">
                        <p>
                            <svg width="21" height="21" viewBox="0 0 31 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.3995 0.666016C6.92931 0.666016 0.0703125 6.63935 0.0703125 13.9993C0.0703125 21.3593 6.92931 27.3327 15.3995 27.3327C23.885 27.3327 30.7593 21.3593 30.7593 13.9993C30.7593 6.63935 23.885 0.666016 15.3995 0.666016ZM26.0332 8.66602H21.5066C21.0156 6.99935 20.3097 5.39935 19.389 3.91935C22.2124 4.75935 24.5601 6.46602 26.0332 8.66602ZM15.4148 3.38602C16.6884 4.98602 17.6858 6.75935 18.3456 8.66602H12.484C13.1438 6.75935 14.1412 4.98602 15.4148 3.38602ZM3.53817 16.666C3.29266 15.8127 3.13921 14.9193 3.13921 13.9993C3.13921 13.0793 3.29266 12.186 3.53817 11.3327H8.72462C8.60186 12.2127 8.50979 13.0927 8.50979 13.9993C8.50979 14.906 8.60186 15.786 8.72462 16.666H3.53817ZM4.79642 19.3327H9.32305C9.81407 20.9994 10.5199 22.5993 11.4406 24.0793C8.6172 23.2393 6.26949 21.546 4.79642 19.3327V19.3327ZM9.32305 8.66602H4.79642C6.26949 6.45268 8.6172 4.75935 11.4406 3.91935C10.5199 5.39935 9.81407 6.99935 9.32305 8.66602V8.66602ZM15.4148 24.6127C14.1412 23.0127 13.1438 21.2394 12.484 19.3327H18.3456C17.6858 21.2394 16.6884 23.0127 15.4148 24.6127ZM19.0054 16.666H11.8242C11.6861 15.786 11.5787 14.906 11.5787 13.9993C11.5787 13.0927 11.6861 12.1993 11.8242 11.3327H19.0054C19.1435 12.1993 19.2509 13.0927 19.2509 13.9993C19.2509 14.906 19.1435 15.786 19.0054 16.666ZM19.389 24.0793C20.3097 22.5993 21.0156 20.9994 21.5066 19.3327H26.0332C24.5601 21.5327 22.2124 23.2393 19.389 24.0793V24.0793ZM22.105 16.666C22.2278 15.786 22.3198 14.906 22.3198 13.9993C22.3198 13.0927 22.2278 12.2127 22.105 11.3327H27.2915C27.537 12.186 27.6904 13.0793 27.6904 13.9993C27.6904 14.9193 27.537 15.8127 27.2915 16.666H22.105Z" fill="#0087DB"/>
                            </svg>  
                            @if ($post->timeslots() == '')

                            @else
                                <a @if(!$post->timeslots()->website->isEmpty()) href="{{ $post->timeslots()->website }}"  target="_blank" @else href='' @endif> {{ $post->timeslots()->website ?? 'null'}} </a>
                            @endif
                            
                        </p>

                        <p id="resbtn">
                            <svg width="22" height="22" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M15.988 2.66602C8.62797 2.66602 2.66797 8.63935 2.66797 15.9993C2.66797 23.3594 8.62797 29.3327 15.988 29.3327C23.3613 29.3327 29.3346 23.3594 29.3346 15.9993C29.3346 8.63935 23.3613 2.66602 15.988 2.66602ZM16.0013 26.666C10.108 26.666 5.33464 21.8927 5.33464 15.9993C5.33464 10.106 10.108 5.33268 16.0013 5.33268C21.8946 5.33268 26.668 10.106 26.668 15.9993C26.668 21.8927 21.8946 26.666 16.0013 26.666Z" fill="#FEAE2C"/>
                            <path d="M16.668 9.33398H14.668V17.334L21.668 21.534L22.668 19.894L16.668 16.334V9.33398Z" fill="#FEAE2C"/>
                            </svg>                    
                            Schedule an on site Tour
                        </p>
                    </div>

                    <div class="dy-cta-col">

                        <p id="tourbtn">
                            <svg width="22" height="22" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M25.3333 4.00065H24V1.33398H21.3333V4.00065H10.6667V1.33398H8V4.00065H6.66667C5.18667 4.00065 4 5.20065 4 6.66732V25.334C4 26.8006 5.18667 28.0006 6.66667 28.0006H25.3333C26.8 28.0006 28 26.8006 28 25.334V6.66732C28 5.20065 26.8 4.00065 25.3333 4.00065ZM25.3333 25.334H6.66667V12.0007H25.3333V25.334ZM25.3333 9.33398H6.66667V6.66732H25.3333V9.33398ZM9.33333 14.6673H16V21.334H9.33333V14.6673Z" fill="#31B7D2"/>
                            </svg>
                            Reserve a Spot
                        </p>

                        <p id="reportbtn">
                            <svg width="21" height="20" viewBox="0 0 21 23" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M12.668 3.00065L11.3346 0.333984H0.667969V23.0007H3.33464V13.6673H10.0013L11.3346 16.334H20.668V3.00065H12.668ZM18.0013 13.6673H12.668L11.3346 11.0007H3.33464V3.00065H10.0013L11.3346 5.66732H18.0013V13.6673Z" fill="#FD5E62"/>
                            </svg>                  
                            Report
                        </p>
                    </div>

                </div>

                <div class="dya-more">

                    <div class="dya-more-col">
                        <h3>License Number</h3>
                        <p>{{ $post->license_number }}</p>
                    </div>

                </div>

                <div class="dya-more border">
                    @php
                        $ph  = $post->phone_number;
                        $ph1 = substr($ph, 0 , 3);
                        $ph2 = substr($ph, 3 ,strlen($ph));
                        $p = $post->service();
                    @endphp

                    <div class="dya-more-col">
                        <h3>Price</h3>
                        
                        <p>{{ $post->infantHours()->infant_hours ?? '' }} per hour</p>

                        <h3>Capacity</h3>
                            @if ($p != '')
                                <span>{{ $post->service()->total_kids }}</span>
                                @else
                                <span>NILL</span>
                            @endif
                            
                        {{-- @if (!$totalSpace->isEmpty()) {
                            <p>{{ $totalSpace[0]->total_kids}}</p>
                        @else
                            <p>Nill</p>
                            @php
                                $totalSpace[0];
                            @endphp
                        @endif --}}

                        <h3>Age</h3>
                        <p>{{ $post->infantHours()->infant_age ?? '' }}</p>

                        <h3>Place</h3>
                        <p>{{ $post->business_address ?? '' }}</p>
                    </div>

                    <div class="dya-more-col">
                        
                        <h3>
                            Space left
                        </h3>

                        @if ($p != '')
                            @php
                                $totalSpaceLeft = $post->service()->total_kids - $post->spot();
                            @endphp
                            <p class="space">Spaces Left: 
                                @if ($totalSpaceLeft == 0)
                                    <span class="full">Full</span>
                                    @else 
                                    <span>{{$totalSpaceLeft}}</span>
                                @endif
                            </p>
                        @endif

                        <h3>Phone</h3>
                        
                        <p>{{ $ph1 ?? '' }}{{ preg_replace("/[0-9]/", "X", '-'.$ph2) ?? '' }}</p>

                        <h3>Languages</h3>

                        @if ($post->languages() != '')
                            @php
                                $language = implode(', ', json_decode($post->languages()->daycare_languages));
                            @endphp
                            <p>
                                {{ $language ?? '' }}
                            </p>
                        @endif
                        
                    </div>

                </div>

                <div class="dya-more-fl">
                    <h3>Program Details</h3>
                    <p>{{ $post->description()->overview ?? '' }}</p>
                </div>

                <div class="dya-more-fl">
                    <h3>Enrollment Instructions</h3>
                    <p>Contact by phone or email to schedule a tour.</p>
                </div>

                <div class="dya-more-fl daycare-rating">
                    <h3>Rating</h3>
                    @php
                        $stars = $post->ratings();
                        $stars = round($stars, 0);
                    @endphp
                    <div class="rate" style="display: flex;padding: 0;">
                        @for ($i = 0; $i < 5; $i++)
                            <label for="star5" title="5 stars" class="@if($i < $stars) checked-star @endif"></label>
                        @endfor
                    </div>
                    <div>
                        <p>{{ $post->totalRatings() }} total ratings</p>
                    </div>
                    
                </div>

            </div>
    </section>

    <section class="reviewd">

        <div class="review-heading">
            <h3>Parents Reviews</h3>
        </div>
    
        <div class="reviewd-hld">

            <div class="review-hld--main">
                <div class="review-holder rvh">
                    <div class="review-form">
                    <div class="review-avatar">
                        @if ( Auth::user()->image == '' )
                            <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                        @else
                            <img src="/user/avatars/{{ Auth::user()->image }}" alt="avatar">
                        @endif
                    </div>
            
                    <div class="review-form--heading">
                        <h3>Share your Review</h3>
                    </div>
            
                    <div class="review-form--form">
                        <form action="{{ route('review') }}" id="formreview" method="POST">
                            @csrf
                            <div class="rate">
                                <input type="radio" id="star5" name="rate" value="5" />
                                <label for="star5" title="5 stars">5 stars</label>
                                <input type="radio" id="star4" name="rate" value="4" />
                                <label for="star4" title="4 stars">4 stars</label>
                                <input type="radio" id="star3" name="rate" value="3" />
                                <label for="star3" title="3 stars">3 stars</label>
                                <input type="radio" id="star2" name="rate" value="2" />
                                <label for="star2" title="2 stars">2 stars</label>
                                <input type="radio" id="star1" name="rate" value="1" />
                                <label for="star1" title="1 star">1 star</label>
                            </div>
                            <input type="hidden" name="daycare_id" value="{{ $post->id }}">
                            <textarea name="comments" id="reviewinput" cols="100" rows="5"></textarea>
                            <input type="submit" value="Post" id="review-btn">
                        </form>
                    </div>
                    </div>
                </div>

                @foreach ($reviews as $review)

                    <div class="review-holder">

                        <div class="review-form">
                            <div class="review-avatar">
                                @if ($review->user->image == '')

                                    <picture>
                                        <source src="{{ asset('daycare/no-image.webp') }} 1x, {{ asset('daycare/no-image.webp') }} 2x" type="image/webp">
                                        <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                                    </picture>
                
                                    @else 
                
                                    <img src="/user/avatars/{{ $review->user->image }}" alt="Avatar" class="rev-img">
                                    
                                @endif
                                
                            </div>
                
                            <div class="review-form--heading">
                                <h4>{{ $review->user->first_name }} {{ $review->user->last_name }}</h4>
                            </div>
                
                            <div class="review-m">
                                <p>{{ $review->comments }}</p>
                            </div>
    
                            {{-- <div class="review-cta">
                                <div class="parent-review-hld">
                                    <div class="review-cta--left">
                                        <p>
                                        <svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M24.7185 2.97538C24.7185 1.75035 23.6406 0.748047 22.3083 0.748047H2.93009C1.59784 0.748047 0.507812 1.75035 0.507812 2.97538V16.3394C0.507812 17.5644 1.59784 18.5667 2.93009 18.5667H19.886L24.7306 23.0214L24.7185 2.97538Z" fill="#333333"/>
                                        </svg>       
                                        <span class="comment-count">3 Comments</span>                 
                                        </p>
    
                                        <p>
                                        <svg width="26" height="20" viewBox="0 0 26 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.4136 5.73073V0.607422L25.1641 9.57321L15.4136 18.539V13.2876C8.44893 13.2876 3.57369 15.3369 0.0913696 19.8198C1.4843 13.4157 5.66308 7.01156 15.4136 5.73073Z" fill="#333333"/>
                                        </svg>                               
                                        <span class="share-count">4 Shares</span>                 
                                        </p>
    
                                        <p>
                                        <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M0.390625 19.1063H2.65923C3.2831 19.1063 3.79353 18.6369 3.79353 18.0632V8.67613C3.79353 8.10248 3.2831 7.63312 2.65923 7.63312H0.390625V19.1063ZM22.8839 11.68C23.0086 11.4193 23.0767 11.1376 23.0767 10.8456V9.71915C23.0767 8.57183 22.0558 7.63312 20.8081 7.63312H14.5694L15.613 2.78311C15.6697 2.55365 15.6357 2.30333 15.5222 2.09472C15.2613 1.62537 14.9324 1.19773 14.524 0.822247L14.0023 0.332031L6.73138 7.01774C6.30035 7.41409 6.06214 7.94602 6.06214 8.49882V16.676C6.06214 18.0111 7.25316 19.1063 8.71641 19.1063H17.9156C18.7096 19.1063 19.4583 18.7203 19.8666 18.0945L22.8839 11.68Z" fill="#727272"/>
                                        </svg>                                                       
                                        <span class="like-count">0 Likes</span>                 
                                        </p>
    
                                    </div>
    
                                    <div class="review-cta--right">
                                        <p>
                                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M9.87266 2.01873L9.47266 0.179688H0.472656V15.8116H2.47266V9.37492H8.07266L8.47266 11.214H15.4727V2.01873H9.87266Z" fill="#333333"/>
                                        </svg>
                                        <span>Report</span>
                                        </p>
                                    </div>
    
                                </div>
                            </div> --}}
                        </div>
                    </div>
                @endforeach

                @if (is_null($post->show_address))

                @elseif ($post->show_address === 'yes')

                    <div class="review-holder">
                        <div class="review-map">

                            @php
                                $api_key = 'AIzaSyAAfE-uI-vq_hDh1RPXAru17R5dYSxNDVs';
                                $id = $post->place_id;

                                $place_id = 'https://www.google.com/maps/embed/v1/place?q=place_id:'.$id.'&key='.$api_key;
                            @endphp
                            
                            <iframe width="600" height="450" frameborder="0" style="border:0"
                            src='<?php echo $place_id ?>' allowfullscreen></iframe>
                                
                        </div>
                    </div>

                @endif

            </div>
            
            <div class="otherd">

                <h3>Daycare Nearby</h3>

                @if (!$nearby_daycare->isEmpty())

                    @foreach ($nearby_daycare as $n_daycare)

                            <div class="other-day">
                                <div class="daycare-avatar">
                                    @if ($post->director_image == '')

                                        <picture>
                                            <source src="{{ asset('daycare/no-image.webp') }} 1x, {{ asset('daycare/no-image.webp') }} 2x" type="image/webp">
                                            <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                                        </picture>
                    
                                        @else 
                    
                                        <img src="{{ asset('daycare/'.$post->director_image) }}" alt="Daycare center avatar">
                                        
                                    @endif
                                </div>
            
                                <div class="other-dy--info">
                                    <p class="other-dy--name">{{ $n_daycare->business_name }}</p>
                                    <p class="other-dy-loc">Daycare in {{ $n_daycare->b_city }}</p>
                                </div>
                            </div>
        
                    @endforeach

                    @else 

                        <p class="other-dy--name">There are no daycare's around you</p>
                        
                @endif                
                
            </div>
        </div>
    </section>
    
    <section id="tour">

        <div class="reserve">

            <form action="{{ route('reserve-spot') }}" method="POST" class="reform updateform">
                @csrf
                <input type="hidden" name="daycare_id" value="{{ $post->id }}">
                <div class="form-group-up">
                    <label for="ParentName">Parent Name</label>
                    <input type="text" name="parent_name" id="ParentName" class="form-control">
                </div>

                <div class="form-group-up">
                    <label for="Address">Address</label>
                    <input type="text" name="address" id="Address" class="form-control">
                </div>

                <div class="form-group-up">
                    <label for="kidName">Kids Name</label>
                    <input type="text" name="kid_name" id="kidName" class="form-control">
                </div>

                <div class="form-group-up">
                    <label for="kidAge">Kids Age</label>
                    <input type="text" name="kid_age" id="kidAge" class="form-control">
                </div>

                <div class="form-group-up">
                    <label for="allergies">Allergies</label>
                    <input type="text" name="allergies" id="allergies" class="form-control">
                </div>

                <div class="form-group-up">
                    <label for="language">Language</label>
                    <input type="text" name="language" id="language" class="form-control">
                </div>

                {{-- <div class="form-group-up">
                    <label for="add more">
                    <input type="checkbox" name="" id="">
                    Add another Kid
                    </label>
                </div> --}}

                <div class="btn-holder">
                    <button class="pr-btn" id="save">Save</button>
                </div>
            </form>
        </div>
    </section>
    
    <section id="reserve">
        <div class="reserve">
            <form action="{{ route('tour') }}" class="reform updateform" method="POST">
                @csrf
                <input type="hidden" name="daycare_id" value="{{ $post->id }}">
                <div class="form-group-up">
                    <label for="Parent Name">Parent Name</label>
                    <input type="text" name="parent_name" id="tour-pname" class="form-control">
                </div>

                <div class="form-group-up">
                    <label for="Kid age">Kids Age</label>
                    <input type="tel" name="kid_age" id="tour-age" class="form-control">
                </div>

                <div class="form-group-up">
                    <label for="Date">Date</label>
                    <input type="date" name="date" id="up-mobile" class="form-control">
                </div>

                <div class="form-group-up">
                    <label for="Time">Time</label>
                    <input type="time" name="time" id="up-mobile" class="form-control" value="03-">
                </div>

                <div class="btn-holder">
                    <button class="pr-btn" id="save">Save</button>
                </div>
            </form>
        </div>
    </section>
    
    <section id="report">
        <div class="reserve">

            <div class="report-main">

                <h3>Report a Problem</h3>
                <p>Thanks for the heads up. What was wrong about this place?</p>
                <form action="{{ route('report') }}" class="rep updateform" method="POST">
                    @csrf
                    <input type="hidden" name="daycare_id" value="{{ $post->id }}">
                    <textarea name="report" id="reviewinput" cols="100" rows="5" placeholder="Example: closed, not a real place, bad photos, incorrect data etc."></textarea>
                    <div class="btn-holder">
                    <input type="submit" value="Post" id="review-btn">
                    </div>
                </form>
            </div>
        </div>
    </section>
    
    <section class="parent-search">
        <div class="parent-search--main">
            <div class="search-icon">
                <svg width="150" height="150" viewBox="0 0 180 180" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="90" cy="90" r="90" fill="#02A2AF"/>
                    <path d="M102.882 97.7358H100.172L99.211 96.8096C102.573 92.8988 104.597 87.8216 104.597 82.2985C104.597 69.9828 94.6141 60 82.2985 60C69.9828 60 60 69.9828 60 82.2985C60 94.6141 69.9828 104.597 82.2985 104.597C87.8216 104.597 92.8988 102.573 96.8096 99.211L97.7358 100.172V102.882L114.889 120L120 114.889L102.882 97.7358ZM82.2985 97.7358C73.7564 97.7358 66.8611 90.8405 66.8611 82.2985C66.8611 73.7564 73.7564 66.8611 82.2985 66.8611C90.8405 66.8611 97.7358 73.7564 97.7358 82.2985C97.7358 90.8405 90.8405 97.7358 82.2985 97.7358Z" fill="white"/>
                </svg>            
            </div>

            <div class="search-daycare">

                <form action="/search" method="GET" class="search-form" role="form">

                    <label for="search form">Search for Daycares in other locations</label>
                    <input type="search" name="search" id="search" class="homesearch" placeholder="Type Zip code or city" required>
        
                    <button type="submit" class="homesubmit">Find Daycare</button>
        
                </form>
            </div>
        </div>
    </section>

@endsection