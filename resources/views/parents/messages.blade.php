@extends('layouts.parents.app')

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" />
@endsection

@section('content')
    <section class="waitlist-hero">
        <img src="{{ asset('assets/img/waitlist-breadcrum.jpg') }}" alt="breadcrumb">
    </section>
  
    <section class="profile-main">
  
        <section class="waitlist-breadcrum">
            <div>
                <ul class="breadcrumb">
                    <li><a href="{{ route('parent-home') }}">Home</a></li>
                    <li><a href="{{ route('profile', Auth::user()->id) }}">Profile</a></li>
                    <li>Messages</li>
                </ul>
            </div>
        </section>
  
         <div class="messages">
            <div class="heading">
                <h2>Direct</h2>
            </div>
           @php
               $result = array();
               foreach ($users as  $value) {
                   $result[$value['id']][] = $value;
               }
           @endphp
            @foreach ($result[$value['id']] as $user)
            <a class="provider-message" href="{{ route('chat',$user->user_id) }}">
                <div class="provider-message--col">
                    
                    <div class="message-data">
                        @if ( $user->image == '' )
                            <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                        @else
                            <img src="/user/avatars/{{ Auth::user()->image }}" alt="avatar">
                        @endif
                        <h3>{{$user->user->first_name}} {{$user->last_name}}</h3>
                    </div>
                    
                </div>

                <div class="provider-message--col">
                    <div class="message-data">
                        <p>{{ Str::limit($user->message, 100) }}</p>
                    </div>
                </div>

                <div class="provider-message--col">
                    <div class="message-data">
                        @php
                            $date = $user->created_at;
                            $created = Carbon\Carbon::parse($date);
                        @endphp
                        <p>{{ Carbon\Carbon::parse($user->created_at)->diffForHumans() }}</p>
                    </div>
                </div>
            </a>
            @endforeach
         </div>

    </section>
@endsection