@extends('layouts.parents.app')

@section('head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/css/dropify.min.css" integrity="sha512-EZSUkJWTjzDlspOoPSpUFR0o0Xy7jdzW//6qhUkoZ9c4StFkVsp9fbbd0O06p9ELS3H486m4wmrCELjza4JEog==" crossorigin="anonymous" />
@endsection

@section('content')
        
    <section class="hero-pr">
        <img src="{{ asset('assets/img/waitlist-breadcrum.jpg') }}" alt="Happy Little girl">
    </section>

    <section class="user-profile">

        @include('includes.messages')

        <div class="profile-accordion">

            <!--ACCOUNT DETAILS-->
            <div class="acc-holder">
                <button class="accordion user">Profile Information</button>

                <div class="panel user">

                    <form action="{{ route('profile.update', Auth::id() ) }}" method="POST" class="updateform" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="proile-user">
                            <div class="user">
    
                                {{-- <img id="output" src="{{ asset('assets/img/Ellipse 233.png') }}"> --}}
    
                                {{-- <input type="file" id="upload" name="image" class="dropify"> --}}
                                <input type="file" name="image" id="edit-image" class="dropify" data-default-file="/user/avatars/{{ $user->image }}">

                                {{-- <label for="upload">
                                    <span>
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M12.0008 15.2008C13.7681 15.2008 15.2008 13.7681 15.2008 12.0008C15.2008 10.2335 13.7681 8.80078 12.0008 8.80078C10.2335 8.80078 8.80078 10.2335 8.80078 12.0008C8.80078 13.7681 10.2335 15.2008 12.0008 15.2008Z" fill="#02A2AF"/>
                                            <path d="M9 2L7.17 4H4C2.9 4 2 4.9 2 6V18C2 19.1 2.9 20 4 20H20C21.1 20 22 19.1 22 18V6C22 4.9 21.1 4 20 4H16.83L15 2H9ZM12 17C9.24 17 7 14.76 7 12C7 9.24 9.24 7 12 7C14.76 7 17 9.24 17 12C17 14.76 14.76 17 12 17Z" fill="#02A2AF"/>
                                        </svg>                    
                                    </span>
                                </label> --}}
    
                            </div>
                        </div>

                        <div class="form-group-up p-user">
                            <label for="First Name">First name</label>
                            <input type="text" name="first_name" id="up-fname" class="form-control users" value="{{ $user->first_name }}">
                        </div>

                        <div class="form-group-up p-user">
                            <label for="Last Name">Last name</label>
                            <input type="text" name="last_name" id="up-lname" class="form-control users" value="{{ $user->last_name }}">
                        </div>

                        <div class="form-group-up p-user">
                            <label for="up-address">Address line</label>
                            <input type="text" name="address" id="up-address" class="form-control users" value="{{ $user->address }}">
                        </div>

                        <div class="form-group-up p-user">
                            <label for="up-city">City</label>
                            <input type="text" name="city" id="up-city" class="form-control users" value="{{ $user->city }}">
                        </div>

                        <div class="form-group-up p-user">
                            <label for="province">Province</label>
                            <input type="text" name="province" id="up-province" class="form-control users" value="{{ $user->province }}">
                        </div>

                        <div class="form-group-up p-user">
                            <label for="up-zip">Zip Code</label>
                            <input type="text" name="zip" id="up-zip" class="form-control users" value="{{ $user->zip }}">
                        </div>

                        <div class="form-group-up p-user">
                            <label for="phonenumber">Phone Number</label>
                            <input type="tel" name="phone_number" id="up-mobile" class="form-control users" value="{{ $user->phone_number }}">
                        </div>

                        <div class="form-group-up p-user">
                            <label for="email">Email Address</label>
                            <input type="email" name="email" id="up-email" class="form-control users" value="{{ $user->email }}">
                        </div>

                        <div class="btn-holder">
                            <button class="pr-btn" id="saveBtn">Save Changes</button>
                        </div>
                    </form>
                </div>
            </div>

            <!-- Change Password -->
            <div class="acc-holder">

                <button class="accordion user">Change Password</button>

                <div class="panel user">
        
                    <form action="{{ route('password.update', Auth::user()->id) }}" method="POST" class="updateform">
                        @csrf
                        @method('PUT')

                        <div class="form-group-up p-user">
                            <label for="password">Change Password</label>
                            <input type="password" name="password" id="password" class="form-control users">
                            
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group-up p-user">
                            <label for="passConfirm">Confirm password</label>
                            <input type="password" name="password_confirmation" id="passConfirm" class="form-control users">
                        </div>
        
                        <div class="btn-holder"><button class="pr-btn" id="save">Save Changes</button></div>

                    </form>

                </div>
            </div>
        </div>

    </section>

@endsection

@section('footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous"></script>
<script>
    $('.dropify').dropify();
</script>
@endsection