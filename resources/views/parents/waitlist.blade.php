@extends('layouts.parents.app')


@section('content')

    <section class="waitlist-hero">
        <img src="{{ asset('assets/img/waitlist-breadcrum.jpg') }}" alt="">
    </section>

    <section class="waitlist-breadcrum">
        <div>
            <ul class="breadcrumb">
                <li><a href="{{ route('parent-home') }}">Home</a></li>
                <li>{{ $post->business_name }}</li>
            </ul>
        </div>
    </section>

    <section class="waitlist-parent">
        <header class="waitlist-parent--hero">
            <div class="avatar">
                @if ($post->daycare_photo == '')
                    <img src="{{ asset('assets/img/avatar.png') }}" alt="Profile Picture">
                    @else 
                    <img src="{{ asset('daycare/'. $post->daycare_photo) }}" alt="Daycare image">   
                @endif
            </div>

            <div class="parent-info">
                <h2 id="daycare-center">{{ $post->business_name }}</h2>
                <p id="parent-location">{{ $post->b_city }}</p>
            </div>
        </header>

        <div class="parent-row">
            <div class="waitlist-col">
                <h2>Name</h2>
                @foreach ($waitlists as $key => $waitlist)
                    <div class="data">
                        <p>{{ $key - 1 }}. {{ $waitlist->parent_name }}</p>
                    </div>
                @endforeach
            </div>
    
            <div class="waitlist-col">
                <h2>Email Address</h2>
                @foreach ($waitlists as $waitlist)
                    @php
                        $em = $waitlist->user->email;
                        $em1 = substr($em, 0 , 4);
                        $em2 = substr($em, 4 ,strlen($em));
                    @endphp
                    <div class="data">
                        <p>{{ $em1 ?? '' }}{{ preg_replace("/[a-z,A-Z]/", "X", ''.$em2) ?? '' }}</p>
                    </div>
                @endforeach
            </div>
    
            <div class="waitlist-col">
                <h2>Phone Number</h2>
                @foreach ($waitlists as $waitlist)
                    @php
                        $ph  = $waitlist->user->phone_number;
                        $ph1 = substr($ph, 0 , 4);
                        $ph2 = substr($ph, 4 ,strlen($ph));
                    @endphp
                    <div class="data">
                            <p>{{ $ph1 ?? '' }}{{ preg_replace("/[0-9]/", "X", '-'.$ph2) ?? '' }}</p>
                    </div>
                @endforeach
            </div>
    
            <div class="waitlist-col">
                <h2>Action</h2>
                @foreach ($waitlists as $waitlist)
                    <div class="data">
                        <a href="{{ route('chat', $waitlist->user_id) }}" class="message">Message</a>
                    </div>  
                @endforeach
                
           </div>
        </div>
    </section>
    
    <section class="parent-search">
        <div class="parent-search--main">
            <div class="search-icon">
                <svg width="150" height="150" viewBox="0 0 180 180" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="90" cy="90" r="90" fill="#02A2AF"/>
                    <path d="M102.882 97.7358H100.172L99.211 96.8096C102.573 92.8988 104.597 87.8216 104.597 82.2985C104.597 69.9828 94.6141 60 82.2985 60C69.9828 60 60 69.9828 60 82.2985C60 94.6141 69.9828 104.597 82.2985 104.597C87.8216 104.597 92.8988 102.573 96.8096 99.211L97.7358 100.172V102.882L114.889 120L120 114.889L102.882 97.7358ZM82.2985 97.7358C73.7564 97.7358 66.8611 90.8405 66.8611 82.2985C66.8611 73.7564 73.7564 66.8611 82.2985 66.8611C90.8405 66.8611 97.7358 73.7564 97.7358 82.2985C97.7358 90.8405 90.8405 97.7358 82.2985 97.7358Z" fill="white"/>
                </svg>            
            </div>

            <div class="search-daycare">

                <form action="/search" method="GET" class="search-form" role="form">

                    <label for="search form">Search for Daycares in other locations</label>
                    <input type="search" name="search" id="search" class="homesearch" placeholder="Type Zip code or city" required>
        
                    <button type="submit" class="homesubmit">Find Daycare</button>
        
                </form>
            </div>
        </div>
    </section>

@endsection