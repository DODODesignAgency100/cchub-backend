@extends('layouts.parents.app')


@section('content')

    <section class="waitlist-hero">
        <img src="{{ asset('assets/img/waitlist-breadcrum.jpg')}}" alt="">
    </section>

{{-- @if(count($result)) --}}

   @isset($daycares)

        @foreach ($daycares as $post)

            <section class="search-filter" style="margin-top: 5rem;">

                <div class="search-img">

                    <a href="{{ route('post', $post->slug) }}" style="color:inherit;">

                        @if ($post->daycare_photo == '')

                            <picture>
                                <source src="{{ asset('daycare/no-image.webp') }} 1x, {{ asset('daycare/no-image.webp') }} 2x" type="image/webp">
                                <img src="{{ asset('daycare/default-image.jpg') }}" alt="daycare no image">
                            </picture>

                            @else 

                            <img src="{{ asset('daycare/'. $post->daycare_photo) }}" alt="Daycare image">
                            
                        @endif
                    </a>

                </div>
        
                <div class="filter-content">
        
                    <div class="filter-content--">
        
                        <div class="daycare-avatar">

                            @if ($post->daycare_photo == '')

                                <picture>
                                    <source src="{{ asset('daycare/no-image.webp') }} 1x, {{ asset('daycare/no-image.webp') }} 2x" type="image/webp">
                                    <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                                </picture>
    
                                @else 
    
                                <img src="{{ asset('daycare/'.$post->director_image) }}" alt="Daycare Logo">
                                
                            @endif

                        </div>
        
                        <div class="av-heading">
                            <h3>{{ $post->business_name }}</h3>
                            <p>{{ $post->address }}</p>
                        </div>
        
                    </div>
        
                    <div class="av-content">
                        <p>{{ $post->description()->overview ?? '' }}</p>
            
                        {{-- <p class="av-ft">Government Approved . Multiple languages</p> --}}
            
                        <div class="av-info">
                            <p class="cap">Capacity: 
                                @php
                                    $p = $post->service();
                                    $ph  = $post->phone_number;
                                    $em = $post->email;
                                    $b_em = $post->b_email;
                                    $em1 = substr($em, 0 , 4);
                                    $em2 = substr($em, 4 ,strlen($em));
                                    $b_em1 = substr($b_em, 0 , 4);
                                    $b_em2 = substr($b_em, 4 ,strlen($b_em));
                                    $ph1 = substr($ph, 0 , 4);
                                    $ph2 = substr($ph, 4 ,strlen($ph));
                                @endphp
                                @if ($p === 'null')
                                    <span>{{ $post->service()->total_kids }}</span>
                                    @else
                                    <span>NILL</span>
                                @endif
                            </p>
                            @if ($p != '')
                                @php
                                    $totalSpaceLeft = $post->service()->total_kids - $post->spot();
                                    $language = implode(', ', json_decode($post->languages()->daycare_languages));
                                @endphp
                                <p class="space">Spaces Left: 
                                    @if ($totalSpaceLeft == 0)
                                        <span class="full">Full</span>
                                        @else 
                                        <span>{{$totalSpaceLeft}}</span>
                                    @endif
                                </p>
                            @endif
                        </div>
            
                        <div class="av-more-info">
            
                            <div class="av-col">
                
                                <h4>
                                <svg width="20" height="20" viewBox="0 0 25 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="12.957" cy="12.8242" r="12" fill="#0C7084" fill-opacity="0.24"/>
                                    <path d="M17.3068 15.2874C16.6166 15.2874 15.9489 15.1701 15.326 14.959C15.1296 14.8886 14.9107 14.9414 14.7592 15.0997L13.8782 16.255C12.2902 15.4633 10.8031 13.9679 10.0119 12.2497L11.1062 11.2762C11.2577 11.112 11.3026 10.8833 11.2408 10.678C11.0332 10.0271 10.9266 9.32922 10.9266 8.60791C10.9266 8.29124 10.6741 8.02734 10.3711 8.02734H8.42948C8.12646 8.02734 7.76172 8.16809 7.76172 8.60791C7.76172 14.0559 12.0994 18.5831 17.3068 18.5831C17.7052 18.5831 17.8624 18.2137 17.8624 17.8911V15.8679C17.8624 15.5513 17.6098 15.2874 17.3068 15.2874Z" fill="#02A2AF"/>
                                </svg>                    
                                Phone Number
                                </h4>
                
                                <p>{{ $ph1 ?? '' }}{{ preg_replace("/[0-9]/", "X", '-'.$ph2) ?? '' }}</p>
                
                            </div>
            
                            <div class="av-col">
                
                                <h4>
                                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="12" cy="12" r="12" fill="#0C7084" fill-opacity="0.24"/>
                                    <path d="M16.3452 7.49414H7.65294C7.05535 7.49414 6.57184 8.00512 6.57184 8.62965L6.56641 15.4427C6.56641 16.0672 7.05535 16.5782 7.65294 16.5782H16.3452C16.9428 16.5782 17.4318 16.0672 17.4318 15.4427V8.62965C17.4318 8.00512 16.9428 7.49414 16.3452 7.49414ZM16.3452 9.76515L11.9991 12.6039L7.65294 9.76515V8.62965L11.9991 11.4684L16.3452 8.62965V9.76515Z" fill="#02A2AF"/>
                                </svg>                                       
                                Email address
                                </h4>
                
                                <p>{{ $em1 ?? '' }}{{ preg_replace("/[a-z]/", "X", ''.$em2) ?? '' }}, {{ $b_em1 ?? '' }}{{ preg_replace("/[a-z]/", "X", ''.$b_em2) ?? '' }}</p>
                
                            </div>
            
                            <div class="av-col">
                
                                <h4>
                                <svg width="20" height="20" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="12" cy="12" r="12" fill="#0C7084" fill-opacity="0.24"/>
                                    <path d="M12.5027 13.8978L11.0293 12.3762L11.0467 12.358C12.056 11.182 12.7753 9.83013 13.1988 8.39947H14.8984V7.18704H10.8379V5.97461H9.67772V7.18704H5.61719V8.3934H12.0966C11.708 9.5634 11.0931 10.6728 10.2578 11.6427C9.71832 11.0183 9.27166 10.3333 8.91782 9.6119H7.75767C8.18112 10.6 8.7612 11.5336 9.48629 12.3762L6.53371 15.4194L7.35741 16.2803L10.2578 13.2492L12.0618 15.1345L12.5027 13.8978ZM15.7685 10.8243H14.6084L11.998 18.0989H13.1582L13.8079 16.2803H16.5632L17.2187 18.0989H18.3788L15.7685 10.8243ZM14.2487 15.0678L15.1884 12.4429L16.1282 15.0678H14.2487Z" fill="#02A2AF"/>
                                </svg>                                                         
                                Languages
                                </h4>
                
                                <p>{{ $language ?? '' }}</p>
                
                            </div>
            
                        </div>
                    </div>
        
                </div>
                
        
            </section>

            <div class="gap"></div>

        @endforeach


    @endisset

@endsection