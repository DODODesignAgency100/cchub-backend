@extends('layouts.parents.app')

@section('head')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('content')

    <section class="waitlist-hero">
        <img src="{{ asset('assets/img/waitlist-breadcrum.jpg') }}" alt="breadcrumb">
    </section>
  
    <section class="profile-main" id="app">
  
        <section class="waitlist-breadcrum">
            <div>
                <ul class="breadcrumb">
                    <li><a href="{{ route('parent-home') }}">Home</a></li>
                    <li><a href="{{ route('profile', Auth::user()->id) }}">Profile</a></li>
                    <li>Messages</li>
                </ul>
            </div>
        </section>

        <chat :user="{{ auth()->user() }}" v-bind:parent_id="'{{ $id }}'" :emessages="{{ $messages }}"></chat>
  
         {{-- <div class="messages" id="messaging">

            <div id="prev-btn">
                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M11.67 3.86961L9.9 2.09961L0 11.9996L9.9 21.8996L11.67 20.1296L3.54 11.9996L11.67 3.86961Z" fill="#333333"/>
                </svg> 
            </div>

  
            @foreach ($messages as $message)
            <div class="message-holder">
                
                <div class="message-chat">
                    
                    <!--Sender-->
                    @if ($message->user_id != Auth::id())
                        <div class="sender">
                            <div class="sender-avatar">
                                <img src="{{ asset('assets/img/parent.png') }}" alt="avatar">
                                <div class="message-l" id="message">
                                    <p>{{$message->message}}</p>
                                </div>
                                <div class="more-l">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 8C13.1 8 14 7.1 14 6C14 4.9 13.1 4 12 4C10.9 4 10 4.9 10 6C10 7.1 10.9 8 12 8ZM12 10C10.9 10 10 10.9 10 12C10 13.1 10.9 14 12 14C13.1 14 14 13.1 14 12C14 10.9 13.1 10 12 10ZM12 16C10.9 16 10 16.9 10 18C10 19.1 10.9 20 12 20C13.1 20 14 19.1 14 18C14 16.9 13.1 16 12 16Z" fill="#333333"/>
                                    </svg>
                                </div>
                            </div>
    
                            <div class="message-time">
                                <p><small>{{ $message->updated_at }}</small></p>
                            </div>
                        </div>
                        @else 

                        <!--Receiver-->  
                        <div class="receiver">
                            <div class="message-right">
                                <div class="more-r">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M12 8C13.1 8 14 7.1 14 6C14 4.9 13.1 4 12 4C10.9 4 10 4.9 10 6C10 7.1 10.9 8 12 8ZM12 10C10.9 10 10 10.9 10 12C10 13.1 10.9 14 12 14C13.1 14 14 13.1 14 12C14 10.9 13.1 10 12 10ZM12 16C10.9 16 10 16.9 10 18C10 19.1 10.9 20 12 20C13.1 20 14 19.1 14 18C14 16.9 13.1 16 12 16Z" fill="#333333"/>
                                    </svg>
                                </div>
            
                                <div class="message-r" id="message">
                                    <p>{{$message->message}}</p>
                                </div>
                            </div>
            
                            <div class="message-time">
                                <p><small>{{ date('h:i a', strtotime($message->created_at)) }}</small></p>
                            </div>
                        </div>
                    @endif

                </div>

            </div>
            @endforeach
  
            <!---->
            <div class="typing-bar">
                <div id="message-avatar">
                    @if ( Auth::user()->image == '' )
                        <img src="{{ asset('user/avatars/avatar.png') }}" alt="avatar">
                    @else
                        <img src="/user/avatars/{{ Auth::user()->image }}" alt="avatar">
                    @endif
                </div>

                <div class="message-input">

                    <div class="emoji">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M16.1984 10.7992C17.1926 10.7992 17.9984 9.99333 17.9984 8.99922C17.9984 8.00511 17.1926 7.19922 16.1984 7.19922C15.2043 7.19922 14.3984 8.00511 14.3984 8.99922C14.3984 9.99333 15.2043 10.7992 16.1984 10.7992Z" fill="#999999"/>
                            <path d="M7.8 10.7992C8.79411 10.7992 9.6 9.99333 9.6 8.99922C9.6 8.00511 8.79411 7.19922 7.8 7.19922C6.80589 7.19922 6 8.00511 6 8.99922C6 9.99333 6.80589 10.7992 7.8 10.7992Z" fill="#999999"/>
                            <path d="M12 19.1984C14.736 19.1984 17.064 17.2064 18 14.3984H6C6.936 17.2064 9.264 19.1984 12 19.1984Z" fill="#999999"/>
                            <path d="M11.988 0C5.364 0 0 5.376 0 12C0 18.624 5.364 24 11.988 24C18.624 24 24 18.624 24 12C24 5.376 18.624 0 11.988 0ZM12 21.6C6.696 21.6 2.4 17.304 2.4 12C2.4 6.696 6.696 2.4 12 2.4C17.304 2.4 21.6 6.696 21.6 12C21.6 17.304 17.304 21.6 12 21.6Z" fill="#999999"/>
                        </svg>                  
                    </div>

                    <form action="/message" method="post">
                        @csrf 
                        <input type="text" name="message" id="chat-msg" placeholder="Type your message..." class="form-control">
                        <input type="hidden" name="receiver_id" value="{{$id}}">
                        <button type="submit">Submit</button>
                    </form>

                    <div class="send-button">
                        <button>
                            <svg width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M40 20C40 31.0457 31.0457 40 20 40C8.9543 40 0 31.0457 0 20C0 8.9543 8.9543 0 20 0C31.0457 0 40 8.9543 40 20Z" fill="#02A2AF"/>
                                <path d="M13.3346 15.025L19.5846 17.7083L13.3346 16.875V15.025ZM19.5846 22.2917L13.3346 24.975V23.125L19.5846 22.2917ZM11.668 12.5V18.3333L24.168 20L11.668 21.6667V27.5L29.168 20L11.668 12.5Z" fill="white"/>
                            </svg>  
                        </button>   
                    </div>
                </div>
            </div>
        </div> --}}
 
    </section>

@endsection

@section('footer')
{{-- <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    var receiver_id = '';
    var my_id = "{{ Auth::id() }}";

    $(document).ready(function () {
        // ajax setup form csrf token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Enable pusher logging - don't include this in production
        Pusher.logToConsole = true;

        var pusher = new Pusher('adf4551a5e98c93b26d5', {
            cluster: 'eu'
        });

        var channel = pusher.subscribe('my-channel');
            channel.bind('my-event', function(data) {
            alert(JSON.stringify(data));
        });

        $(document).on('keyup', '#chat-msg', function (e) {
            var message = $(this).val();
            // check if enter key is pressed and message is not null also receiver is selected
            if (e.keyCode == 13 && message != '') {
                $(this).val(''); // while pressed enter text box will be empty
                var datastr = "message=" + message;
                $.ajax({
                    type: "post",
                    url: "/sendMessage", // need to create this post route
                    data: datastr,
                    cache: false,
                    success: function (data) {
                    },
                    error: function (jqXHR, status, err) {
                    },
                    complete: function () {
                        // scrollToBottomFunc();
                    }
                })
            }
        });
    });
</script>
{{-- <script>
    var receiver_id = '';
    var my_id = "{{ Auth::id() }}";

    $(document).ready(function () {
        // ajax setup form csrf token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Enable pusher logging - don't include this in production
        // Pusher.logToConsole = true;

        // var pusher = new Pusher('adf4551a5e98c93b26d5', {
        //     cluster: 'eu'
        // });

        // var channel = pusher.subscribe('my-channel');
        //     channel.bind('my-event', function(data) {
        //     alert(JSON.stringify(data));
        // });

        $(document).on('keyup', '#chat-msg', function (e) {
            var message = $(this).val();
            // check if enter key is pressed and message is not null also receiver is selected
            if (e.keyCode == 13 && message != '' ) {
                
                $(this).val(''); // while pressed enter text box will be empty
                var datastr = message;
                console.log(datastr);
                $.ajax({
                    type: "post",
                    url: "/sendMessage", 
                    data: datastr,
                    cache: false,
                    success: function (data) {
                        alert('success');
                    },
                    error: function (jqXHR, status, err) {

                    },
                    complete: function () {
                        
                    }
                })
            }
        });
    });

    // make a function to scroll down auto
    function scrollToBottomFunc() {
        $('.message-holder').animate({
            scrollTop: $('.message-holder').get(0).scrollHeight
        }, 50);
    }
</script> --}}
<script src="{{ asset('js/app.js') }}"></script>
@endsection